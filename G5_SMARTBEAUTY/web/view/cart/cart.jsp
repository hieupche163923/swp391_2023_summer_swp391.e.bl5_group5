<%-- 
    Document   : cart
    Created on : Aug 23, 2023, 1:36:39 AM
    Author     : binhp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cart</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
        <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
        <link rel="stylesheet" id="flatsome-googlefonts-css" href="//fonts.googleapis.com/css?family=Playfair+Display%3Aregular%2C700%7CRoboto%3Aregular%2Cregular%7CVollkorn%3Aregular%2C700%7CDancing+Script%3Aregular%2C400&amp;display=swap&amp;ver=3.9" type="text/css" media="all">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.10.5/font/bootstrap-icons.min.css" integrity="sha512-ZnR2wlLbSbr8/c9AgLg3jQPAattCUImNsae6NHYnS9KrIwRdcY9DxFotXhNAKIKbAXlRnujIqUWoXXwqyFOeIQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/G5_SMARTBEAUTY/view/home/index.css"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
        <script>
            function confirmDelete(id) {
                var message = confirm("Xóa khỏi giỏ hàng?");
                if (message === true) {
                    window.location.href = '/G5_SMARTBEAUTY/cart/delete?id=' + id;
                } else {
                    return false;
                }
            }
            function toggleDropup() {
                var dropup = document.getElementById("myDropup");

                if (dropup.style.display === "block") {
                    dropup.style.display = "none";
                } else {
                    dropup.style.display = "block";
                }
            }
            function logoutSubmit() {
                document.getElementById("logoutForm").submit();
            }
            function calculate(id) {

                var amount = Number(document.getElementById("amount" + id).value);
                var price = parseFloat(document.getElementById("price" + id).textContent);
                var multiple = amount * price;
                document.getElementById("singleprice" + id).textContent = multiple;
                calTotalMoney();
            }
            function calTotalMoney() {
                var total = 0;
                for (var i = 1, max = ${requestScope.count}; i <= max; i++) {
                    total += parseFloat(document.getElementById("singleprice" + i).textContent);
                }
                document.getElementById("total").textContent = total;
            }
            function checkValue(sender) {
                let min = sender.min;
                let max = sender.max;
                let value = int(sender.value);
                if (value > max) {
                    sender.value = min;
                } else if (value < min) {
                    sender.value = max;
                }
            }
        </script>
    </head>
    <style>
        body{
        }
        .search{
        }
        .searchbox {
            width: 30%;
            margin-left: auto;
            margin-right: 0;
        }
        .inner-item{
            border: 1px solid white;
            border-bottom: 4px solid white;
            box-shadow: 0px 0px 17px rgba(0,0,0,0.14);
            padding: 15px;
            border-radius: 7px;
        }
        .body-content{
            margin-left: 10%;
            margin-right: 10%;
            margin-bottom: 30px;
            margin-top: 30px;
        }
    </style>
    <body>
        <div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v17.0" nonce="tdHZraaT"></script>
        <!--header-->
        <header class="header-wrapper" style="font-family: 'Vollkorn', 'sans-serif';">
            <div class="navbar navbar-expand-lg">
                <div class="flex-row container" style="max-width: 1234px;">
                    <div class="flex-col flex-left">
                        <a class="empty-a" href="tel:0846555566">
                            <i class="bi bi-telephone-fill"></i> &nbsp;		      
                            <span>0846555566</span>
                        </a>
                    </div>
                    <div class="flex-col flex-center"></div>
                    <div class="flex-col">
                        <a class="empty-a" href="https://www.facebook.com/spahapbeauty/">
                            <i class="fab fa-facebook-f" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://www.instagram.com/" >
                            <i class="fab fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://twitter.com/" >
                            <i class="fab fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="mailto:flearning2077@gmail.com" >
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-row container" style="max-width: 1234px;">
                <div class="flex-col hide-for-medium flex-left">
                    <form class="nosubmit" hidden="true">
                        <input type="search" class="search-box nosubmit" placeholder="Tìm kiếm sản phẩm" value="" name="search">
                    </form>
                </div>
                <div class="flex-col logo">
                    <a href="/G5_SMARTBEAUTY/home" title="hapbeauty" rel="home">
                        <img style="max-height: 100%"
                             src="/G5_SMARTBEAUTY/img/logo-tet.jpg" 
                             class="header_logo header-logo" alt="hapbeauty">
                    </a>
                </div>
                <div class="flex-col hide-for-medium flex-right">
                    <ul class="cart">
                        <li>
                            <c:if test="${sessionScope.user == null}">
                                <a class="header-nav" href="/G5_SMARTBEAUTY/view/login/Login.jsp">
                                    ĐĂNG NHẬP
                                </a>
                            </c:if>
                            <c:if test="${sessionScope.user != null}">
                                <c:if test="${sessionScope.user.getRole() eq '1'}">
                                    <a class="header-nav" href="/G5_SMARTBEAUTY/profile?id=${sessionScope.user.getUid()}">
                                        ${sessionScope.user.getUsername()}
                                    </a>
                                </c:if>
                                <c:if test="${sessionScope.user.getRole() eq '2'}">
                                    <a class="header-nav" href="profile" style="margin-right: 20px">
                                        ${sessionScope.user.getUsername()}
                                    </a>
                                    <a class="header-nav" href="employee">
                                        Employee Dashboard
                                    </a>
                                </c:if>
                                <c:if test="${sessionScope.user.getRole() eq '3'}">
                                    <a class="header-nav" href="admin">
                                        Administrator Dashboard
                                    </a>
                                </c:if>

                            </c:if>
                        </li>
                        <li>
                            <c:if test="${sessionScope.user != null}">
                                <form id="logoutForm" action="/G5_SMARTBEAUTY/logout" method="post">
                                    <a id="logoutNav" class="header-nav" href="#" onclick="logoutSubmit()">
                                        Đăng xuất
                                    </a>
                                </form>
                            </c:if>
                        </li>

                        <c:choose>
                            <c:when test="${sessionScope.user != null}">
                                <c:if test="${sessionScope.user.getRole() eq '1'}">
                                    <li>
                                        <a href="/G5_SMARTBEAUTY/cart?id=${sessionScope.userid}" title="Cart">
                                            <i class="fa fa-shopping-bag" aria-hidden="true" style="color:black;"></i>
                                        </a>
                                    </li>
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <li>
                                    <a href="cart" title="Cart">
                                        <i class="fa fa-shopping-bag" aria-hidden="true" style="color:black;"></i>
                                    </a>
                                </li>
                            </c:otherwise>
                        </c:choose>

                    </ul>
                </div>
            </div>
        </header>

        <div class="wide-nav sticky-top" style="box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.125);">
            <div class="flex-row container">
                <div class="flex-col flex-center">
                    <ul class="nav header-nav nav-spacing-xlarge">
                        <li><a class="header-nav" href="/G5_SMARTBEAUTY/home" aria-current="page">Trang chủ</a></li>
                            <c:choose>
                                <c:when test="${sessionScope.user != null}">
                                    <c:choose>
                                        <c:when test="${sessionScope.user.getRole() eq '3'}">
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/service">Quản lý dịch vụ</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/product">Quản lý sản phẩm</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/order">Quản lý đặt hàng</a></li>
                                        </c:when>
                                        <c:otherwise>
                                        <li><a class="header-nav" href="#">Giới thiệu</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/service">Dịch vụ</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/product">Sản Phẩm</a></li>
                                        <li><a class="header-nav" href="#">Đặt Lịch</a></li>
                                        <li><a class="header-nav" href="/G5_SMARTBEAUTY/order?id=${sessionScope.userid}">Lịch Sử Mua Hàng</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                <li><a class="header-nav" href="#">Giới thiệu</a></li>
                                <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/service">Dịch vụ</a></li>
                                <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/product">Sản Phẩm</a></li>
                                <li><a class="header-nav" href="#">Đặt Lịch</a></li>
                                <li><a class="header-nav" href="/G5_SMARTBEAUTY/order?id=${sessionScope.userid}">Lịch Sử Mua Hàng</a></li>
                                </c:otherwise>
                            </c:choose>
                    </ul>
                </div>
            </div>
        </div>
        <div class="body-content">
            <div class="d-grid gap-2 d-md-block mb-2">
                <i class="fa-solid fa-arrow-left-long fa-2xl" style="cursor: pointer;" onclick="location.href = 'javascript:history.back()';"></i>
            </div>
            <h3 style="text-align: center; color: red;">${requestScope.mess}</h3>
            <div class="inner-item">
                <c:choose>
                    <c:when test="${sessionScope.user != null}">
                        <c:if test="${requestScope.count ne '0'}">
                            <form method="post" action="/G5_SMARTBEAUTY/cart/order">
                                <div class="input-group input-group-sm mb-3">
                                    <span class="input-group-text" id="inputGroup-sizing-sm">Người nhận hàng</span>
                                    <input type="text" class="form-control" required minlength="6" maxlength="50" pattern="^(?!\s)[\s\S]*(?<!\s)$" title="Invalid name" name="recivername" value="${sessionScope.recivername}">
                                </div>
                                <div class="input-group input-group-sm mb-3">
                                    <span class="input-group-text" id="inputGroup-sizing-sm">Số điện thoại</span>
                                    <input type="tel" class="form-control" required minlength="10" maxlength="10" pattern="[0-9]+"title="Only numbers!" name="phone" value="${sessionScope.cartphone}">
                                </div>
                                <div class="input-group input-group-sm mb-3">
                                    <span class="input-group-text" id="inputGroup-sizing-sm">Địa chỉ nhận hàng</span>
                                    <input type="text" class="form-control" required minlength="6" maxlength="1024" pattern="^(?!\s)[\s\S]*(?<!\s)$" title="Invalid address" name="address" value="${sessionScope.cartaddress}">
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Tên sản phẩm</th>
                                            <th scope="col">Đơn giá</th>
                                            <th scope="col" style="width: 100px;">Số lượng</th>
                                            <th scope="col">Thành tiền</th>
                                            <th scope="col">Xóa</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${requestScope.cartdetails}" var="detail" varStatus="loop">
                                            <tr>
                                                <th scope="row">${loop.count}</th>
                                                <td><p">${detail.productName}</p></td>
                                                <td style="display: flex;"><p id="price${loop.count}">${detail.price}</p><p>₫</p></td>
                                                <td><input class="form-control" type="number" min="1" max="${detail.quantity}" step="1" value="${detail.amount}" name="amount${loop.count}" id="amount${loop.count}" required onchange="calculate(${loop.count})" oninput="checkValue(this);"></td>
                                                <td style="display: flex;"><p id="singleprice${loop.count}">${detail.singlePrice}</p><p>₫</p></td>
                                                <td><i style="cursor: pointer;" class="fa-solid fa-trash" onclick="confirmDelete(${detail.productId})"></i></td>
                                        <input hidden type="text" name="productid${loop.count}" value="${detail.productId}">
                                        <input hidden type="text" name="productquantity${loop.count}" value="${detail.quantity}">
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                                <h4 style="color: red; display: flex; justify-content: right"><p>Tổng tiền:&nbsp</p><p id="total">${requestScope.total}</p><p>₫</p></h4>
                                <div class="d-grid gap-2 d-md-flex justify-content-md-end mt-3">
                                    <input class="btn btn-primary" type="button" value="Lưu" onclick="location.href = '#';">
                                    <button class="btn btn-primary" type="submit">Đặt hàng</button>
                                </div>
                            </form>
                        </c:if>
                        <c:if test="${requestScope.count eq '0'}">
                            <h3 style="text-align: center; color: red;">Giỏ hàng của bạn đang trống</h3>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <h3 style="text-align: center; color: red;">Bạn cần đăng nhập để sử dụng chức năng này</h3>
                    </c:otherwise>
                </c:choose>

            </div>
        </div>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-3">
                        <div class="col-inner">
                            <div style="width: 50%;">
                                <div class="img-inner dark">
                                    <img width="480" height="480" src="/G5_SMARTBEAUTY/img/logo-ft.png" style="width: 100%;height: auto;">                                    
                                </div>
                            </div>
                            <p style="color:#fffff">Hệ Thống Dưỡng Sinh Đông Y, Chăm Sóc Sức Khỏe và Sắc Đẹp HAPBEAUTY SPA</p>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="col-inner">
                            <h3><b class="service-about-heading">Hỗ trợ khách hàng</b></h3>
                            <ul class="support-about" style="list-style: disc;">
                                <li><span style="font-size: 95%;"><a href="/chinh-sach-quy-dinh/">Chính sách &amp; Quy định</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/hinh-thuc-thanh-toan/">Hình thức thanh toán</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/giao-nhan/">Chính sách giao nhận</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/chinh-sach-bao-hanh/">Chính sách bảo hành</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/chinh-sach-doi-tra-hang/">Chính sách đổi trả hàng</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/bao-mat-thong-tin-khach-hang/">Bảo mật thông tin khách hàng</a></span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="col-inner text-left">
                            <h3><b class="service-about-heading">Thông tin liên hệ</b></h3>
                            <ul>
                                <li style="color:#fffff">Địa chỉ: Tầng 3, Tòa nhà Impact, 15A Đường. Số 68 - TML, P. Thạnh Mỹ Lợi, Quận 2, TP.HCM</li>
                                <li style="color:#fffff">Điện thoại: 0846555566</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="col-inner">
                            <h3 ><b class="service-about-heading">Fanpage</b></h3>
                            <div class="fb-page" data-href="https://www.facebook.com/HapBeautyvn" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/HapBeautyvn" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/HapBeautyvn">HapBeauty - Spa &amp; Healthy Joy</a></blockquote></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
