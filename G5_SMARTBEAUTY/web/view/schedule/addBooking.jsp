<%-- 
    Document   : adminDashboard
    Created on : Aug 14, 2023, 6:33:22 AM
    Author     : LENOVO
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <link rel="shortcut icon" href="/G5_SMARTBEAUTY/img/logo-ft.png"/>
        <title>Administration Dashboard</title>
        <link href="/G5_SMARTBEAUTY/view/schedule/dashboard.css" rel="stylesheet" type="text/css"/>
        <link href="/G5_SMARTBEAUTY/view/schedule/schedule.css?version=1" rel="stylesheet" type="text/css"/>
        <!-- Font Awesome Cdn Link -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"/>
    </head>
    <body>
        <div class="containerr" >
            <nav>
                <ul>
                    <li ><a href="#" class="logo">
                            <img src="/G5_SMARTBEAUTY/img/logo-ft.png" alt="">
                            <span class="nav-item">DashBoard</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/home">
                            <i class="fas fa-home"></i>
                            <span class="nav-item">Home</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/admin/product">
                            <i class="fas fa-user"></i>
                            <span class="nav-item">Product</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/admin/service">
                            <i class="fas fa-wallet"></i>
                            <span class="nav-item">Service</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/admin/staff">
                            <i class="fas fa-chart-bar"></i>
                            <span class="nav-item">Employee</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/admin/customer">
                            <i class="fas fa-tasks"></i>
                            <span class="nav-item">Customer</span>
                        </a></li>
                    <li><a href="">
                            <i class="fas fa-cog"></i>
                            <span class="nav-item">Settings</span>
                        </a></li>
                    <li><a href="">
                            <i class="fas fa-question-circle"></i>
                            <span class="nav-item">Help</span>
                        </a></li>

                    <li class="current" ><a href="schedule">
                            <i class="fas fa-calendar-alt"></i>
                            <span class="nav-item">Schedule</span>
                        </a></li>
                    <li><a href="" class="logout">
                            <i class="fas fa-sign-out-alt"></i>
                            <span class="nav-item">Log out</span>
                        </a></li>
                </ul>
            </nav>

            <section class="main">
                <head>
                    <!-- Required meta tags -->
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

                    <!-- Bootstrap CSS -->
                    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

                    <title>Basic Boostrap Form</title>
                </head>

                <body class="bg-light">
                    <div class="container">
                        <!-- Start Form Title -->
                        <div class="py-5 text-center">
                            <div class="col">
                                <h1>Add Booking</h1>
                            </div>
                        </div>
                        <!-- End Form Title -->

                        <!-- Start Form -->
                        <form class="needs-validation" novalidate>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="CustomerName">Customer Name</label>
                                    <input name="CustomerName" type="text" class="form-control" id="CustomerName" placeholder value required>
                                    <div class="invalid-feedback">Valid customer name is required.</div>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="EmployeeName">Employee Name</label>
                                    <input name="EmployeeName" type="text" class="form-control" id="EmployeeName" placeholder value required>
                                    <div class="invalid-feedback">Valid Employee name is required.</div>
                                </div>
                            </div>

                            <div>Bed</div>
                            <div class="form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="inlineCheckbox1" value="option1"name="bed">
                                    <label class="form-check-label" for="inlineCheckbox1">Bed 1</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="inlineCheckbox2" value="option2"name="bed">
                                    <label class="form-check-label" for="inlineCheckbox2">Bed 2</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="inlineCheckbox3" value="option3" name="bed">
                                    <label class="form-check-label" for="inlineCheckbox3">Bed 3 </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="inlineCheckbox4" value="option1"name="bed">
                                    <label class="form-check-label" for="inlineCheckbox4">Bed 4</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="inlineCheckbox5" value="option2"name="bed">
                                    <label class="form-check-label" for="inlineCheckbox5">Bed 5</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" id="inlineCheckbox6" value="option3"name="bed" >
                                    <label class="form-check-label" for="inlineCheckbox6">Bed 6 </label>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="Time">Time </label>
                                    <input name="time" type="time" class="form-control" id="time" placeholder="Time">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="Date">Date </label>
                                    <input name="date" type="date" class="form-control" id="date" placeholder="Date">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputStatus">Status</label>
                                    <select name="status" id="inputStatus" class="form-control">
                                        <option selected>Choose...</option>
                                        <option value="0">pending</option>
                                        <option value="1">complete</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="inputService">Service</label>
                                    <select name="selectService" id="inputService" class="form-control">
                                        <option selected>Choose...</option>
                                        <option value="1">serv1</option>
                                        <option value="2">serv2</option>
                                        <option value="3">serv3</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-outline-success">Submit</button>
                                <button type="reset" class="btn btn-outline-danger">Reset</button>
                            </div>
                        </form>
                        <!-- End Form -->

                        <div class="my-5 text-center text-small">
                            <p class="mb-1">© 2023 Smart Beauty</p>
                        </div>
                    </div>

                    <!-- Optional JavaScript -->
                    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
                    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
                    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
                    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
                </body>

            </section>
        </div>

    </body>
</html>
