<%-- 
    Document   : adminDashboard
    Created on : Aug 14, 2023, 6:33:22 AM
    Author     : LENOVO
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <link rel="shortcut icon" href="/G5_SMARTBEAUTY/img/logo-ft.png"/>
        <title>Administration Dashboard</title>
        <link href="/G5_SMARTBEAUTY/view/schedule/dashboard.css?version=1" rel="stylesheet" type="text/css"/>
        <link href="/G5_SMARTBEAUTY/view/schedule/schedule.css?version=2" rel="stylesheet" type="text/css"/>
        <!-- Font Awesome Cdn Link -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"/>
    </head>
    <body>
        <div class="containerr" >
            <nav>
                <ul>
                    <li ><a href="#" class="logo">
                            <img src="/G5_SMARTBEAUTY/img/logo-ft.png" alt="">
                            <span class="nav-item">DashBoard</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/home">
                            <i class="fas fa-home"></i>
                            <span class="nav-item">Home</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/material">
                            <i class="fas fa-sharp fa-regular fa-folder"></i>
                            <span class="nav-item">Material</span>
                        </a></li>
                    <li><a href="cdmin/staff">
                            <i class="fas fa-chart-bar"></i>
                            <span class="nav-item">Employee</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/admin/customer">
                            <i class="fas fa-tasks"></i>
                            <span class="nav-item">Customer</span>
                        </a></li>
                    <li class="current" ><a href="">
                            <i class="fas fa-calendar-alt"></i>
                            <span class="nav-item">Schedule</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/logout" class="logout">
                            <i class="fas fa-sign-out-alt"></i>
                            <span class="nav-item">Log out</span>
                        </a></li>
                </ul>
            </nav>

            <section class="main">
                <div class="text-center" style="display: flex; justify-content: space-around;">
                    <a class="buttonADD" href="#" role="button" style=" background-color: limegreen;margin-bottom: 20px; width: fit-content;">
                        <i class="fas fa-plus"></i> ADD Booking</a> 
                    <form class="selectToday" style="display: flex" action="schedule" method="">

                        <input type="date" name="selectdate1" style="margin-bottom: 20px; text-align: center" value="${datess}" required min="1900-01-01">
                        <button style="margin: 0px 0px 20px 5px; padding: 3px; text-align: center; background: greenyellow" type="submit">Search</button>
                    </form>
                </div>

                <div class="table-responsive">
                    <table class="table table-bordered text-center">
                        <thead>
                            <tr class="bg-light-gray">
                                <th class="text-uppercase">Bed Number
                                </th>
                                <th class="text-uppercase">Bed 1</th>
                                <th class="text-uppercase">Bed 2</th>
                                <th class="text-uppercase">Bed 3</th>
                                <th class="text-uppercase">Bed 4</th>
                                <th class="text-uppercase">Bed 5</th>
                                <th class="text-uppercase">Bed 6</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td class="align-middle">09:00</td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 9 && schedule.getBed() == 1 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>

                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 9 && schedule.getBed() == 2 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 9 && schedule.getBed() == 3 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td> 
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 9 && schedule.getBed() == 4 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 9 && schedule.getBed() == 5 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>

                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 9 && schedule.getBed() == 6 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>


                            </tr>



                            <tr>
                                <td class="align-middle">10:30</td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 10 && schedule.getSlot().getMinutes() == 30 && schedule.getBed() == 1 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>

                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 10 && schedule.getSlot().getMinutes() == 30 && schedule.getBed() == 2 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 10 && schedule.getSlot().getMinutes() == 30 && schedule.getBed() == 3 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td> 
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 10 && schedule.getSlot().getMinutes() == 30 && schedule.getBed() == 4 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 10 && schedule.getSlot().getMinutes() == 30 && schedule.getBed() == 5 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>

                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 10 && schedule.getSlot().getMinutes() == 30 && schedule.getBed() == 6 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                            </tr>

                            <tr>
                                <td class="align-middle">13:00</td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 13 && schedule.getBed() == 1 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>

                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 13 && schedule.getBed() == 2 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 13 && schedule.getBed() == 3 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td> 
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 13 && schedule.getBed() == 4 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 13 && schedule.getBed() == 5 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>

                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 13 && schedule.getBed() == 6 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                            </tr>

                            <tr>
                                <td class="align-middle">14:30</td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 14 && schedule.getSlot().getMinutes() == 30 && schedule.getBed() == 1 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>

                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 14 && schedule.getSlot().getMinutes() == 30 && schedule.getBed() == 2 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 14 && schedule.getSlot().getMinutes() == 30 && schedule.getBed() == 3 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td> 
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 14 && schedule.getSlot().getMinutes() == 30 && schedule.getBed() == 4 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 14 && schedule.getSlot().getMinutes() == 30 && schedule.getBed() == 5 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>

                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 14 && schedule.getSlot().getMinutes() == 30 && schedule.getBed() == 6 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                            </tr>

                            <tr>
                                <td class="align-middle">16:00</td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 16 && schedule.getBed() == 1 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>

                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 16 && schedule.getBed() == 2 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 16 && schedule.getBed() == 3 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td> 
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 16 && schedule.getBed() == 4 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 16 && schedule.getBed() == 5 && stopLoop eq false }">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>

                                <td>
                                    <c:set var="stopLoop" value="false" scope="page"/>
                                    <c:forEach items="${schedule}" var="schedule">
                                        <c:if test="${schedule.getSlot().getHours() == 16 && schedule.getBed() == 6 && stopLoop eq false}">
                                            <span>Staff: ${schedule.getEmployee()}</span>
                                            <div class="margin-10px-top font-size14">Cus: ${schedule.getCustomer()}</div>
                                            <div class="itemtd">
                                                <c:if test="${schedule.isStatus()}">
                                                    <small>Status: <span style="color: green;">${'&#x2714;'}</span></small>
                                                    </c:if>
                                                    <c:if test="${!schedule.isStatus()}">
                                                    <small>Status: <span style="color: gray;">${'&#x2718;'}</span></small>
                                                    </c:if>
                                                <button onclick="viewUser('${schedule.getShid()}', '${schedule.getCustomer()}', '${schedule.getEmployee()}', '${schedule.getService()}', '${schedule.isStatus()}')">View</button>                                 
                                                <c:if test="${datess.compareTo(datecheck)<=0 }">
                                                    <button onclick="openUserPopup('${schedule.getShid()}', '${schedule.getEmployeeid()}', '${schedule.isStatus()}')">UP</button>
                                                </c:if>
                                            </div>
                                            <c:set var="stopLoop" value="true" scope="page"/>
                                        </c:if>
                                    </c:forEach>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </section>
        </div>
        <!--                                    //updatte-->
        <div class="modal hide" id="changeModal">
            <div style="position: relative;height: 100%;">
                <div class="modal__inner">
                    <form id="popupForm" action="schedule" method="post">
                        <div class="modal__header">
                            <p>Update Schedule</p>
                            <i class="fas fa-times"></i>
                        </div>
                        <p style="color: red;text-align: center;font-weight: 800;">${errorStaffCreate}</p>
                        <div class="modal__body">
                            <input id="shid" readonly type="hidden" name="shid" value="">
                            <h4 class="required">Employee</h4>
                            <select name="employ" required>
                                <c:forEach items="${employ}" var="employ">
                                    <option value="${employ.getUid()}">${employ.getDisplayName()}</option>
                                </c:forEach>
                            </select>
                            <h4 class="required">Status</h4>
                            <div style="text-align-last: center;">
                                <input style="width:unset" type="radio" name="status" value="false" required checked>
                                <label style="margin-right: 5em" for="html">Not Done</label>
                                <input style="width:unset" type="radio" name="status" value="true">
                                <label for="css">Done</label><br>
                            </div>
                        </div>
                        <div class="modal__footer">
                            <button type="reset">Close</button>
                            <button name="mode" type="submit">Update</button>
                        </div>
                    </form>  
                </div>
            </div>
        </div>
        <!--VIEWING FORM-->
        <div class="modal hide" id="viewModal">
            <div style="position: relative; height: 100%;">
                <div class="modal__inner">
                    <div class="modal__header">
                        <p>Information</p>
                        <i class="fas fa-times" onclick="toggleView();"></i>
                    </div>
                    <div class="modal__body" id="viewModalBody">
                        <table style="width: 100%;">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><strong> Schedule ID:</strong></td>
                                    <td name='vid'></td>
                                </tr>
                                <tr>
                                    <td><strong>Employee:</strong></td>
                                    <td name='vemploy'></td>
                                </tr>
                                <tr>
                                    <td><strong>Customer:</strong></td>
                                    <td name='vcus'></td>
                                </tr>
                                <tr>
                                    <td><strong>Service:</strong></td>
                                    <td name='vservice'></td>
                                </tr>

                                <tr>
                                    <td><strong>Status:</strong></td>
                                    <td name="vstatus"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal__footer">
                        <button onclick="toggleView();">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <script src="/G5_SMARTBEAUTY/view/schedule/popup.js" type="text/javascript">

        </script>
    </body>
</html>
