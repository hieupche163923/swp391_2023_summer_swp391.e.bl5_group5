 <%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@page 
        isErrorPage="true"
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Có lỗi bất ngờ xảy ra &nbsp;&nbsp; :( </h1>
        <h2>Hãy thử lại hoặc liên hệ với Quản trị viên:&nbsp;&nbsp;<a href="mailto:flearning2077@gmail.com">flearning2077@gmail.com</a></h2><br>
        
        <a href="/G5_SMARTBEAUTY/home"><h2>Quay về trang chủ</h2></a>
        
        <section>
            <h3 style="margin-bottom: 5px">Chi tiết</h3>
            <p style="margin-top: 0">
                <%= exception %>
            </p>
        </section>
    </body>
</html>
                             