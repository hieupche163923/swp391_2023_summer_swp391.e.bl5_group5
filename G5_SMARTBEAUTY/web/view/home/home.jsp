<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/G5_SMARTBEAUTY/img/logo-ft.png"/>
        <title>Beauty Spa</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
        <link rel="stylesheet" id="flatsome-googlefonts-css" href="//fonts.googleapis.com/css?family=Playfair+Display%3Aregular%2C700%7CRoboto%3Aregular%2Cregular%7CVollkorn%3Aregular%2C700%7CDancing+Script%3Aregular%2C400&amp;display=swap&amp;ver=3.9" type="text/css" media="all">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.10.5/font/bootstrap-icons.min.css" integrity="sha512-ZnR2wlLbSbr8/c9AgLg3jQPAattCUImNsae6NHYnS9KrIwRdcY9DxFotXhNAKIKbAXlRnujIqUWoXXwqyFOeIQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="view/home/index.css"/>
        <link href="https://getbootstrap.com/docs/5.3/assets/css/docs.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>

    </head>
    <body>

        <div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v17.0" nonce="tdHZraaT"></script>
        <!--header-->
        <header class="header-wrapper" style="font-family: 'Vollkorn', 'sans-serif';">
            <div class="navbar navbar-expand-lg">
                <div class="flex-row container" style="max-width: 1234px;">
                    <div class="flex-col flex-left">
                        <a class="empty-a" href="tel:0846555566">
                            <i class="bi bi-telephone-fill"></i> &nbsp;		      
                            <span>0846555566</span>
                        </a>
                    </div>
                    <div class="flex-col flex-center"></div>
                    <div class="flex-col">
                        <a class="empty-a" href="https://www.facebook.com/spahapbeauty/">
                            <i class="fab fa-facebook-f" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://www.instagram.com/" >
                            <i class="fab fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://twitter.com/" >
                            <i class="fab fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="mailto:flearning2077@gmail.com" >
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-row container" style="max-width: 1234px;">
                <div class="flex-col hide-for-medium flex-left">
                    <form class="nosubmit" hidden="true">
                        <input type="search" class="search-box nosubmit" placeholder="Tìm kiếm sản phẩm" value="" name="search">
                    </form>
                </div>
                <div class="flex-col logo">
                    <a href="/G5_SMARTBEAUTY/home" title="hapbeauty" rel="home">
                        <img style="max-height: 100%"
                             src="/G5_SMARTBEAUTY/img/logo-tet.jpg" 
                             class="header_logo header-logo" alt="hapbeauty">
                    </a>
                </div>
                <div class="flex-col hide-for-medium flex-right">
                    <ul class="cart">
                        <li>
                            <c:if test="${sessionScope.user == null}">
                                <a class="header-nav" href="/G5_SMARTBEAUTY/view/login/Login.jsp">
                                    ĐĂNG NHẬP
                                </a>
                            </c:if>
                            <c:if test="${sessionScope.user != null}">
                                <c:if test="${sessionScope.user.getRole() eq '1'}">
                                    <a class="header-nav"  href="profile?id=${sessionScope.user.getUid()}" style="margin-right: 20px">
                                        ${sessionScope.user.getUsername()}
                                    </a>
                                </c:if><c:if test="${sessionScope.user.getRole() eq '2'}">
                                    <a class="header-nav" href="employee">
                                        Employee Dashboard
                                    </a>
                                </c:if>
                                <c:if test="${sessionScope.user.getRole() eq '3'}">
                                    <a class="header-nav" href="admin">
                                        Administrator Dashboard
                                    </a>
                                </c:if>

                            </c:if>
                        </li>
                        <li>
                            <c:if test="${sessionScope.user != null}">
                                <form id="logoutForm" action="logout" method="post">
                                    <a id="logoutNav" class="header-nav" href="#" onclick="logoutSubmit()">
                                        Đăng xuất
                                    </a>
                                </form>
                            </c:if>
                        </li>

                        <c:choose>
                            <c:when test="${sessionScope.user != null}">
                                <c:if test="${sessionScope.user.getRole() eq '1'}">
                                    <li>
                                        <a href="cart?id=${sessionScope.userid}" title="Cart">
                                            <i class="fa fa-shopping-bag" aria-hidden="true" style="color:black;"></i>
                                        </a>
                                    </li>
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <li>
                                    <a href="cart" title="Cart">
                                        <i class="fa fa-shopping-bag" aria-hidden="true" style="color:black;"></i>
                                    </a>
                                </li>
                            </c:otherwise>
                        </c:choose>

                    </ul>
                </div>
            </div>
        </header>

        <div class="wide-nav sticky-top" style="box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.125);">
            <div class="flex-row container">
                <div class="flex-col flex-center">
                    <ul class="nav header-nav nav-spacing-xlarge">
                        <li><a class="header-nav" href="/G5_SMARTBEAUTY/home" aria-current="page">Trang chủ</a></li>
                            <c:choose>
                                <c:when test="${sessionScope.user != null}">
                                    <c:choose>
                                        <c:when test="${sessionScope.user.getRole() eq '3'}">
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/service">Quản lý dịch vụ</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/product">Quản lý sản phẩm</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/order">Quản lý đặt hàng</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/stock">Quản lý kho</a></li>
                                        </c:when>
                                        <c:otherwise>
                                        <li><a class="header-nav" href="about">Giới thiệu</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/service">Dịch vụ</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/product">Sản Phẩm</a></li>
                                        <li><a class="header-nav" href="/G5_SMARTBEAUTY/booking">Đặt Lịch</a></li>
                                        <li><a class="header-nav" href="/G5_SMARTBEAUTY/order?id=${sessionScope.userid}">Lịch Sử Mua Hàng</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                <li><a class="header-nav" href="about">Giới thiệu</a></li>
                                <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/service">Dịch vụ</a></li>
                                <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/product">Sản Phẩm</a></li>
                                <li><a class="header-nav" href="#">Đặt Lịch</a></li>
                                <li><a class="header-nav" href="/G5_SMARTBEAUTY/order?id=${sessionScope.userid}">Lịch Sử Mua Hàng</a></li>
                                </c:otherwise>
                            </c:choose>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Main Content -->
        <div class="img-inner dark">
            <img width="100%" src="/G5_SMARTBEAUTY/img/slider.jpeg" sizes="(max-width: 1020px) 100vw, 1020px" style="box-shadow:0px 10px 10px rgba(0, 0, 0, 0.075)">        
        </div>
        <section style="margin-bottom: 20px;box-shadow: 0px 10px 19px rgba(0,0,0,0.05);padding: 40px;">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-3">
                        <div class="img has-hover x md-x lg-x y md-y lg-y" style="width: 100%">
                            <div class="img-inner dark">
                                <img width="238" height="238" src="https://hapbeauty.com/wp-content/uploads/2019/04/van-miu.jpg" data-src="https://hapbeauty.com/wp-content/uploads/2019/04/van-miu.jpg" class="attachment-original size-original lazy-load-active" alt="" srcset="https://hapbeauty.com/wp-content/uploads/2019/04/van-miu.jpg 238w, https://hapbeauty.com/wp-content/uploads/2019/04/van-miu-150x150.jpg 150w, https://hapbeauty.com/wp-content/uploads/2019/04/van-miu-100x100.jpg 100w" data-srcset="https://hapbeauty.com/wp-content/uploads/2019/04/van-miu.jpg 238w, https://hapbeauty.com/wp-content/uploads/2019/04/van-miu-150x150.jpg 150w, https://hapbeauty.com/wp-content/uploads/2019/04/van-miu-100x100.jpg 100w" sizes="(max-width: 238px) 100vw, 238px">						
                            </div>
                        </div>
                    </div>
                    <div class="col-9">
                        <p>Chuỗi hệ thống dưỡng sinh, chăm sóc sức khỏe và sắc đẹp HapBeauty Spa với phương châm “Bạn ở đâu HapBeauty ở đó”, HapBeauty Spa sẽ luôn gần sát bên bạn, tạo ra không gian tận hưởng tuyệt vời cho giá trị cuộc sống mà chúng ta bấy lâu nay chưa tìm ra.</p>
                        <p>Đến với HapBeauty Spa bạn sẽ được phục vụ theo tiêu chuẩn đẳng cấp riêng biệt. Với đội ngũ kỹ thuật viên, nhân viên chuyên nghiệp cùng không gian hưởng thụ thỏa mái, sản phẩm uy tín chất lượng, bạn sẽ cảm nhận được những điều kỳ diệu nhất chỉ có tại HapBeauty Spa.</p>
                        <button class="button-about">Xem chi tiết hơn</button>
                    </div>
                </div>
            </div>
        </section>

        <div class="main">
            <section class="top-service" style="margin: 50px 0px 20px 0px;box-shadow: 0px 10px 19px rgba(0,0,0,0.05);">
                <div class="container">
                    <div class="row">
                        <div class="section-heading">
                            <span class="line"></span>
                            <span class="service-heading">DỊCH VỤ NỔI BẬT</span>
                            <span class="line"></span>
                        </div>                        
                    </div>
                    <div class="row" style="text-align: -webkit-center; margin-top:40px">
                        <c:forEach items="${topServices}" var="service" >
                            <div class="col-4 service-item">
                                <div style="width:50%">
                                    <a href="service/detail?sid=${service.getId()}">
                                        <img style="max-width: 100%;aspect-ratio:1;border-radius: 50%" src="${service.getImage()}" alt="alt"/>
                                    </a>
                                </div>
                                <h3 style="padding-top: .5em;padding-bottom: 1.4em;"><a href="service/detail?sid=${service.getId()}" class="service-about-heading"><b>${service.getName()}</b></a></h3>
                            </div>
                        </c:forEach>
                    </div>
                    <div class="row">
                        <div class="service-about col-4" style="background: #f7ede7;">
                            <div>
                                <img src="/G5_SMARTBEAUTY/img/icon-service1.png" width="45" height="45" alt="alt"/>
                            </div>
                            <div style="padding-left:20px;">
                                <h3><a class="service-about-heading"><b>CÔNG NGHỆ VƯỢT TRỘI</b><br></a></h3>
                                <p>Chúng tôi luôn áp dụng những công nghệ mới nhất, hiện đại nhất, tốt nhất cho khách hàng</p>
                            </div>
                        </div>
                        <div class="service-about col-4" style="background: #fbf4ef;">
                            <div>
                                <img src="/G5_SMARTBEAUTY/img/icon-service2.png" width="45" height="45" alt="alt"/>
                            </div>
                            <div style="padding-left:20px;">
                                <h3><a class="service-about-heading"><b>ĐỘI NGŨ CHUYÊN GIA KINH NGHIỆM</b><br>
                                    </a></h3>
                                <p>Các chuyên gia của chúng tôi đều được học tập, đào tạo bài bản theo tiêu chuẩn quốc tế.</p>
                            </div>
                        </div>
                        <div class="service-about col-4" style="background: #fdf9f6;">
                            <div>
                                <img src="/G5_SMARTBEAUTY/img/icon-service3.png" width="45" height="45" alt="alt"/>
                            </div>
                            <div style="padding-left:20px;">
                                <h3><a class="service-about-heading"><b>SẢN PHẨM TIN CẬY</b><br>
                                    </a></h3>
                                <p>Sản phẩm đều đảm bảo chất lượng tốt và an toàn nhất, có giấy công bố của bộ Y tế, dễ dàng check mã vạch sản phẩm.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="top-product" style="padding:20px 0px;">
                <div class="container" style="text-align: -webkit-center;">
                    <div class="row">
                        <div class="section-heading">
                            <span class="line"></span>
                            <span class="service-heading">Sản phẩm bán chạy</span>
                            <span class="line"></span>
                        </div>                        
                    </div>
                    <div class="row" style="text-align: -webkit-center; margin:10px 0;max-width: 1170px;">
                        <c:forEach items="${topProducts}" var="service" >
                            <div class="col-3 product-item">
                                <div class="inner-item">
                                    <div>
                                        <a href="product/detail?pid=${service.getId()}">
                                            <img style="max-width: 100%;aspect-ratio:1;border-radius: 10px;" src="${service.getImage()}" alt="alt"/>
                                        </a>
                                    </div>
                                    <h3 style="padding-top: .1em;"><a href="product/detail?pid=${service.getId()}" class="service-about-heading">${service.getName()}</a></h3>
                                    <h4>${service.getPrice()}₫</h4>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </section>

            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-3">
                            <div class="col-inner">
                                <div style="width: 50%;">
                                    <div class="img-inner dark">
                                        <img width="480" height="480" src="/G5_SMARTBEAUTY/img/logo-ft.png" style="width: 100%;height: auto;">                                    
                                    </div>
                                </div>
                                <p style="color:#fffff">Hệ Thống Dưỡng Sinh Đông Y, Chăm Sóc Sức Khỏe và Sắc Đẹp HAPBEAUTY SPA</p>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="col-inner">
                                <h3><b class="service-about-heading">Hỗ trợ khách hàng</b></h3>
                                <ul class="support-about" style="list-style: disc;">
                                    <li><span style="font-size: 95%;"><a href="#">Chính sách &amp; Quy định</a></span></li>
                                    <li><span style="font-size: 95%;"><a href="#">Hình thức thanh toán</a></span></li>
                                    <li><span style="font-size: 95%;"><a href="#">Chính sách giao nhận</a></span></li>
                                    <li><span style="font-size: 95%;"><a href="#">Chính sách bảo hành</a></span></li>
                                    <li><span style="font-size: 95%;"><a href="#">Chính sách đổi trả hàng</a></span></li>
                                    <li><span style="font-size: 95%;"><a href="#">Bảo mật thông tin khách hàng</a></span></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="col-inner text-left">
                                <h3><b class="service-about-heading">Thông tin liên hệ</b></h3>
                                <ul>
                                    <li style="color:#fffff">Địa chỉ: Tầng 3, Tòa nhà Impact, 15A Đường. Số 68 - TML, P. Thạnh Mỹ Lợi, Quận 2, TP.HCM</li>
                                    <li style="color:#fffff">Điện thoại: 0846555566</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="col-inner">
                                <h3 ><b class="service-about-heading">Fanpage</b></h3>
                                <div class="fb-page" data-href="https://www.facebook.com/HapBeautyvn" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/HapBeautyvn" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/HapBeautyvn">HapBeauty - Spa &amp; Healthy Joy</a></blockquote></div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <script>
            function formatPrice(price) {
                var a = price.toLocaleString('it-IT', {style: 'currency', currency: 'VND'}).replace(/\D00(?=\D*$)/, '');
                $(".formatted-price").html(a + '₫');
            }
            function logoutSubmit() {
                document.getElementById("logoutForm").submit();
            }
        </script>
    </body>
</html>
