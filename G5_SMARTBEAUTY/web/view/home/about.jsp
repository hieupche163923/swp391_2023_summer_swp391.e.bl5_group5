<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/G5_SMARTBEAUTY/img/logo-ft.png"/>
        <title>Beauty Spa</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
        <link rel="stylesheet" id="flatsome-googlefonts-css" href="//fonts.googleapis.com/css?family=Playfair+Display%3Aregular%2C700%7CRoboto%3Aregular%2Cregular%7CVollkorn%3Aregular%2C700%7CDancing+Script%3Aregular%2C400&amp;display=swap&amp;ver=3.9" type="text/css" media="all">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.10.5/font/bootstrap-icons.min.css" integrity="sha512-ZnR2wlLbSbr8/c9AgLg3jQPAattCUImNsae6NHYnS9KrIwRdcY9DxFotXhNAKIKbAXlRnujIqUWoXXwqyFOeIQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <!--        <link rel="stylesheet" href="view/home/index.css"/>-->
        <link href="https://getbootstrap.com/docs/5.3/assets/css/docs.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
        <style>
            .header ul{
                list-style:none;
            }
            body {
                -moz-osx-font-smoothing: grayscale;
                -webkit-font-smoothing: antialiased;
                font-family: Roboto;
            }
            .container, .row{
                max-width: 1170px;
                text-align: initial;
            }
            .navbar{
                background-image: url(/G5_SMARTBEAUTY/img/patten-bg.png);
                background-color: #d09a75;
            }
            .flex-row{
                display: flex;
                width: 100%;
                margin-left: auto;
                margin-right: auto;
                max-width: 1080px;
                justify-content: space-between;
                align-items: center;
            }
            .empty-a{
                text-decoration: none;
                color:rgba(255,255,255,0.8);
                font-weight: normal;
                margin-left: .25em;
                margin-right: .25em;
                transition: 0.3s;
            }
            .empty-a:hover{
                color:white;
            }
            .flex-left{
                margin-right: auto;
                flex: 1 1 0;
            }
            .flex-center{
                margin: 0 auto;
            }
            .flex-right{
                margin-left: auto;
                order: 3;
                flex: 1 1 0;
            }
            textarea:focus, input:focus{
                outline: none;
            }
            .logo{
                width:295px;
                height: 90px;
                text-align: center;
            }
            .search-box{
                box-shadow: none;
                height: 40px;
                color: currentColor !important;
                border-radius: 99px;
                box-sizing: border-box;
                border: 1px solid rgb(208, 154, 117);
                padding: 0 .75em;
                max-width: 100%;
                width: 100%;
            }
            .cart{
                display: flex;
                margin:0;
                justify-content: space-between;
                list-style:none;
            }
            .cart a {
                font-size: 1.2em;
                font-weight: bold !important;
            }
            input.nosubmit {
                padding: 9px 4px 9px 40px;
                background: transparent url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='20' height='20' class='bi bi-search' viewBox='0 0 16 16'%3E%3Cpath d='M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z' fill='%23d09a75' stroke='%23d09a75' stroke-width='0.2'%3E%3C/path%3E%3C/svg%3E") no-repeat 13px center;
            }

            .header-nav{
                text-decoration: none;
                color: black;
                transition: 0.3s;
            }
            .header-nav:hover{
                color: #d09a75;
            }
            .nav li:first-child {
                margin-left: 0 !important;
            }
            .nav-spacing-xlarge>li {
                margin: 0 13px;
            }
            .nav>li {
                display: inline-block;
                list-style: none;
                margin: 0;
                padding: 0;
                position: relative;
                margin: 0 13px;
                transition: background-color .3s;
            }
            .wide-nav {
                background-color: white;
                display: flex;
                min-height: 60px;
                text-align: center;
                text-transform: uppercase;
            }
            .wide-nav a{
                letter-spacing: .02em;
                font-family: Vollhorn;
                text-decoration: none;
            }
            .button-about {
                background-color: #d09a75;
                border: none;
                border-radius: 99px;
                color: white;
                padding: 5px 20px;
                font-size: 16px;
                margin: 4px 2px;
                cursor: pointer;
                transition: 0.3s;
            }
            .button-about:hover{
                background-color: #c48e6a;
            }
            .section-heading {
                display: flex;
                align-items: center;
                justify-content: space-between;
                font-size: 18px;
                font-weight: bold;
                padding: 0 20px;
            }
            .line {
                opacity: .1;
                flex-grow: 1;
                border-top: 2px solid #000;
                margin-left: 20px;
            }
            .line:first-child {
                margin-left: 0px;
                margin-right: 20px;
            }
            .section-heading > :not(.line) {
                display: flex;
                align-items: center;
            }
            .section-heading {
                white-space: nowrap;
            }
            .container p{
                margin-top: revert;
            }
            footer{
                background: url(/G5_SMARTBEAUTY/img/bg-ft.jpg);
                background-repeat: no-repeat;
                background-repeat: no-repeat;
                background-attachment: scroll;
                background-size: cover;
                background-position: center center;
                padding-top: 60px;
                padding-bottom: 60px;
            }
            .support-about a{
                text-decoration: none;
                color: #d09a75;
                &:hover{
                    color: black;
                }
            }
        </style>
    </head>
    <body>

        <div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v17.0" nonce="tdHZraaT"></script>
        <!--header-->
        <header class="header-wrapper" style="font-family: 'Vollkorn', 'sans-serif';">
            <div class="navbar navbar-expand-lg">
                <div class="flex-row container" style="max-width: 1234px;">
                    <div class="flex-col flex-left">
                        <a class="empty-a" href="tel:0846555566">
                            <i class="bi bi-telephone-fill"></i> &nbsp;		      
                            <span>0846555566</span>
                        </a>
                    </div>
                    <div class="flex-col flex-center"></div>
                    <div class="flex-col">
                        <a class="empty-a" href="https://www.facebook.com/spahapbeauty/">
                            <i class="fab fa-facebook-f" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://www.instagram.com/" >
                            <i class="fab fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://twitter.com/" >
                            <i class="fab fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="mailto:flearning2077@gmail.com" >
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-row container" style="max-width: 1234px;">
                <div class="flex-col hide-for-medium flex-left">
                    <form class="nosubmit" hidden="true">
                        <input type="search" class="search-box nosubmit" placeholder="Tìm kiếm sản phẩm" value="" name="search">
                    </form>
                </div>
                <div class="flex-col logo">
                    <a href="/G5_SMARTBEAUTY/home" title="hapbeauty" rel="home">
                        <img style="max-height: 100%"
                             src="/G5_SMARTBEAUTY/img/logo-tet.jpg" 
                             class="header_logo header-logo" alt="hapbeauty">
                    </a>
                </div>
                <div class="flex-col hide-for-medium flex-right">
                    <ul class="cart">
                        <li>
                            <c:if test="${sessionScope.user == null}">
                                <a class="header-nav" href="/G5_SMARTBEAUTY/view/login/Login.jsp">
                                    ĐĂNG NHẬP
                                </a>
                            </c:if>
                            <c:if test="${sessionScope.user != null}">
                                <c:if test="${sessionScope.user.getRole() eq '1'}">
                                    <a class="header-nav"  href="profile?id=${sessionScope.user.getUid()}" style="margin-right: 20px">
                                        ${sessionScope.user.getUsername()}
                                    </a>
                                </c:if><c:if test="${sessionScope.user.getRole() eq '2'}">
                                    <a class="header-nav" href="employee">
                                        Employee Dashboard
                                    </a>
                                </c:if>
                                <c:if test="${sessionScope.user.getRole() eq '3'}">
                                    <a class="header-nav" href="admin">
                                        Administrator Dashboard
                                    </a>
                                </c:if>

                            </c:if>
                        </li>
                        <li>
                            <c:if test="${sessionScope.user != null}">
                                <form id="logoutForm" action="logout" method="post">
                                    <a id="logoutNav" class="header-nav" href="#" onclick="logoutSubmit()">
                                        Đăng xuất
                                    </a>
                                </form>
                            </c:if>
                        </li>

                        <c:choose>
                            <c:when test="${sessionScope.user != null}">
                                <c:if test="${sessionScope.user.getRole() eq '1'}">
                                    <li>
                                        <a href="cart?id=${sessionScope.userid}" title="Cart">
                                            <i class="fa fa-shopping-bag" aria-hidden="true" style="color:black;"></i>
                                        </a>
                                    </li>
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <li>
                                    <a href="cart" title="Cart">
                                        <i class="fa fa-shopping-bag" aria-hidden="true" style="color:black;"></i>
                                    </a>
                                </li>
                            </c:otherwise>
                        </c:choose>

                    </ul>
                </div>
            </div>
        </header>

        <div class="wide-nav sticky-top" style="box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.125);">
            <div class="flex-row container">
                <div class="flex-col flex-center">
                    <ul class="nav header-nav nav-spacing-xlarge">
                        <li><a class="header-nav" href="/G5_SMARTBEAUTY/home" aria-current="page">Trang chủ</a></li>
                            <c:choose>
                                <c:when test="${sessionScope.user != null}">
                                    <c:choose>
                                        <c:when test="${sessionScope.user.getRole() eq '3'}">
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/service">Quản lý dịch vụ</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/product">Quản lý sản phẩm</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/order">Quản lý đặt hàng</a></li>
                                        </c:when>
                                        <c:otherwise>
                                        <li><a class="header-nav" href="#">Giới thiệu</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/service">Dịch vụ</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/product">Sản Phẩm</a></li>
                                        <li><a class="header-nav" href="/G5_SMARTBEAUTY/booking">Đặt Lịch</a></li>
                                        <li><a class="header-nav" href="/G5_SMARTBEAUTY/order?id=${sessionScope.userid}">Lịch Sử Mua Hàng</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                <li><a class="header-nav" href="#">Giới thiệu</a></li>
                                <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/service">Dịch vụ</a></li>
                                <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/product">Sản Phẩm</a></li>
                                <li><a class="header-nav" href="#">Đặt Lịch</a></li>
                                <li><a class="header-nav" href="/G5_SMARTBEAUTY/order?id=${sessionScope.userid}">Lịch Sử Mua Hàng</a></li>
                                </c:otherwise>
                            </c:choose>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Main Content -->
        <div class="img-inner dark">
            <img width="100%" src="/G5_SMARTBEAUTY/img/slider.jpeg" sizes="(max-width: 1020px) 100vw, 1020px" style="box-shadow:0px 10px 10px rgba(0, 0, 0, 0.075)">        
        </div>
        <section style="margin-bottom: 20px;box-shadow: 0px 10px 19px rgba(0,0,0,0.05);padding: 40px;">
            <div class="container">
                <div class="row justify-content-center">
                    <p>
                        <strong>Hệ thống dưỡng sinh, chăm sóc sức khỏe và sắc đẹp HAPBEAUTY SPA &amp; HEALTHY JOY là mô hình kinh doanh làm đẹp trực thuộc Công ty TNHH Thương mại Thủ Quán Việt Nam.</strong>
                    </p>
                    <p>Được thành lập năm 2011, Công Ty TNHH Thương Mại Thủ Quán Việt Nam là một trong những thương hiệu hàng đầu chuyên cung cấp dịch vụ, sản phẩm phục vụ ngành thẩm mỹ, làm đẹp tại Việt Nam.</p>
                </div>
            </div>
        </section>

        <div class="main">

            <section class="top-product" style="padding:20px 0px;">
                <div class="container" style="text-align: -webkit-center;">
                    <div class="row">
                        <div class="section-heading">
                            <span class="line"></span>
                            <span class="line"></span>
                        </div>                        
                    </div>
                    <div class="container">
                        <p>Các dịch vụ chính của công ty:</p>
                        <ul>
                            <li>Cung cấp mỹ phẩm chuyên ngành làm đẹp.</li>
                            <li>Cung cấp thiết bị chuyên ngành làm đẹp.</li>
                            <li>Tư vấn dịch vụ đăng ký nhãn hiệu trong và ngoài nước.</li>
                            <li>Tư vấn Gia công sản xuất sản phẩm mỹ phẩm mang Thương hiệu riêng và xin cấp phép giấy tờ pháp lý từ bộ y tế Việt Nam cho các nhà đại lý, spa và thẩm mỹ viện.</li>
                        </ul>
                        <p>Sau một thời gian phát triển hệ thống&nbsp;<strong>HAPBEAUTY SPA &amp; HEALTHY JOY</strong>&nbsp;đã có mặt tại 2 thành phố lớn : <em><strong>Hà Nội và Hồ Chí Minh</strong></em>. Chúng tôi luôn luôn đổi mới trau dồi kiến thức, áp dụng khoa học công nghệ và kỹ thuật tiên tiến nhất, dịch vụ chuyên nghiệp nhất, chất lượng cao nhất, phục vụ tận tình chu đáo nhất .</p>
                        <p>Ngoài các dịch cụ làm đẹp đã được nghiên cứu chuẩn hóa quy trình, <strong>HAPBEAUTY SPA &amp; HEALTHY JOY</strong>&nbsp;cam kết luôn cung cấp đến khách hàng các sản phẩm: <em><strong>An Toàn – Hiệu Quả – Giá Cả Hợp Lý – Niêm Yết Rõ Ràng</strong></em>.</p>
                    </div>
                    <br><br><br>
                    <div>
                        <p style="text-align: center;"><strong>HAPBEAUTYSPA &amp; HEALTHY JOY&nbsp;</strong></p>

                        <p style="text-align: center;"><strong>Tỏa sáng vẻ đẹp hoàn mỹ</strong></p>

                        <p style="text-align: center;"><span class="_5mfr"><span class="_6qdm">⏰</span></span> Open : 9 AM – 8:30 PM</p>
                    </div>
                </div>
            </section>

            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-3">
                            <div class="col-inner">
                                <div style="width: 50%;">
                                    <div class="img-inner dark">
                                        <img width="480" height="480" src="/G5_SMARTBEAUTY/img/logo-ft.png" style="width: 100%;height: auto;">                                    
                                    </div>
                                </div>
                                <p style="color:#fffff">Hệ Thống Dưỡng Sinh Đông Y, Chăm Sóc Sức Khỏe và Sắc Đẹp HAPBEAUTY SPA</p>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="col-inner">
                                <h3><b class="service-about-heading">Hỗ trợ khách hàng</b></h3>
                                <ul class="support-about" style="list-style: disc;">
                                    <li><span style="font-size: 95%;"><a href="/chinh-sach-quy-dinh/">Chính sách &amp; Quy định</a></span></li>
                                    <li><span style="font-size: 95%;"><a href="/hinh-thuc-thanh-toan/">Hình thức thanh toán</a></span></li>
                                    <li><span style="font-size: 95%;"><a href="/giao-nhan/">Chính sách giao nhận</a></span></li>
                                    <li><span style="font-size: 95%;"><a href="/chinh-sach-bao-hanh/">Chính sách bảo hành</a></span></li>
                                    <li><span style="font-size: 95%;"><a href="/chinh-sach-doi-tra-hang/">Chính sách đổi trả hàng</a></span></li>
                                    <li><span style="font-size: 95%;"><a href="/bao-mat-thong-tin-khach-hang/">Bảo mật thông tin khách hàng</a></span></li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="col-inner text-left">
                                <h3><b class="service-about-heading">Thông tin liên hệ</b></h3>
                                <ul>
                                    <li style="color:#fffff">Địa chỉ: Tầng 3, Tòa nhà Impact, 15A Đường. Số 68 - TML, P. Thạnh Mỹ Lợi, Quận 2, TP.HCM</li>
                                    <li style="color:#fffff">Điện thoại: 0846555566</li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-3">
                            <div class="col-inner">
                                <h3 ><b class="service-about-heading">Fanpage</b></h3>
                                <div class="fb-page" data-href="https://www.facebook.com/HapBeautyvn" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/HapBeautyvn" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/HapBeautyvn">HapBeauty - Spa &amp; Healthy Joy</a></blockquote></div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        <script>
            function logoutSubmit() {
                document.getElementById("logoutForm").submit();
            }
        </script>
    </body>
</html>
