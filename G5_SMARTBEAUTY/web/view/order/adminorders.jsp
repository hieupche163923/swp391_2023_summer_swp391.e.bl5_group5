<%-- 
    Document   : adminorders
    Created on : Aug 22, 2023, 10:03:52 AM
    Author     : binhp
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin Order</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
        <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
        <link rel="stylesheet" id="flatsome-googlefonts-css" href="//fonts.googleapis.com/css?family=Playfair+Display%3Aregular%2C700%7CRoboto%3Aregular%2Cregular%7CVollkorn%3Aregular%2C700%7CDancing+Script%3Aregular%2C400&amp;display=swap&amp;ver=3.9" type="text/css" media="all">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.10.5/font/bootstrap-icons.min.css" integrity="sha512-ZnR2wlLbSbr8/c9AgLg3jQPAattCUImNsae6NHYnS9KrIwRdcY9DxFotXhNAKIKbAXlRnujIqUWoXXwqyFOeIQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/G5_SMARTBEAUTY/view/home/index.css"/>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(function () {
                $("#datefrom").datepicker();
            });
            $(function () {
                $("#dateto").datepicker();
            });

            function toggleDropup() {
                var dropup = document.getElementById("myDropup");

                if (dropup.style.display === "block") {
                    dropup.style.display = "none";
                } else {
                    dropup.style.display = "block";
                }
            }
            function logoutSubmit() {
                document.getElementById("logoutForm").submit();
            }
        </script>
    </head>
    <style>
        body{
        }
        .search{
            display: flex;
            justify-content: right;
        }
        .searchbox {
            width: 20%;
            margin-left: 20px
        }
        .inner-item{
            border: 1px solid white;
            border-bottom: 4px solid white;
            box-shadow: 0px 0px 17px rgba(0,0,0,0.14);
            padding: 15px;
            border-radius: 7px;
        }
        .body-content{
            margin-left: 10%;
            margin-right: 10%;
            margin-bottom: 30px;
        }
    </style>
    <body>
        <div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v17.0" nonce="tdHZraaT"></script>
        <!--header-->
        <header class="header-wrapper" style="font-family: 'Vollkorn', 'sans-serif';">
            <div class="navbar navbar-expand-lg">
                <div class="flex-row container" style="max-width: 1234px;">
                    <div class="flex-col flex-left">
                        <a class="empty-a" href="tel:0846555566">
                            <i class="bi bi-telephone-fill"></i> &nbsp;		      
                            <span>0846555566</span>
                        </a>
                    </div>
                    <div class="flex-col flex-center"></div>
                    <div class="flex-col">
                        <a class="empty-a" href="https://www.facebook.com/spahapbeauty/">
                            <i class="fab fa-facebook-f" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://www.instagram.com/" >
                            <i class="fab fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://twitter.com/" >
                            <i class="fab fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="mailto:flearning2077@gmail.com" >
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-row container" style="max-width: 1234px;">
                <div class="flex-col hide-for-medium flex-left">
                    <form class="nosubmit" hidden="true">
                        <input type="search" class="search-box nosubmit" placeholder="Tìm kiếm sản phẩm" value="" name="search">
                    </form>
                </div>
                <div class="flex-col logo">
                    <a href="/G5_SMARTBEAUTY/home" title="hapbeauty" rel="home">
                        <img style="max-height: 100%"
                             src="/G5_SMARTBEAUTY/img/logo-tet.jpg" 
                             class="header_logo header-logo" alt="hapbeauty">
                    </a>
                </div>
                <div class="flex-col hide-for-medium flex-right">
                    <ul class="cart">
                        <li>
                            <c:if test="${sessionScope.user == null}">
                                <a class="header-nav" href="/G5_SMARTBEAUTY/view/login/Login.jsp">
                                    ĐĂNG NHẬP
                                </a>
                            </c:if>
                            <c:if test="${sessionScope.user != null}">
                                <c:if test="${sessionScope.user.getRole() eq '1'}">
                                    <a class="header-nav" href="/G5_SMARTBEAUTY/profile">
                                        ${sessionScope.user.getUsername()}
                                    </a>
                                </c:if>
                                <c:if test="${sessionScope.user.getRole() eq '2'}">
                                    <a class="header-nav" href="/G5_SMARTBEAUTY/profile" style="margin-right: 20px">
                                        ${sessionScope.user.getUsername()}
                                    </a>
                                    <a class="header-nav" href="/G5_SMARTBEAUTY/employee">
                                        Employee Dashboard
                                    </a>
                                </c:if>
                                <c:if test="${sessionScope.user.getRole() eq '3'}">
                                    <a class="header-nav" href="/G5_SMARTBEAUTY/admin">
                                        Administrator Dashboard
                                    </a>
                                </c:if>

                            </c:if>
                        </li>
                        <li>
                            <c:if test="${sessionScope.user != null}">
                                <form id="logoutForm" action="/G5_SMARTBEAUTY/logout" method="post">
                                    <a id="logoutNav" class="header-nav" href="#" onclick="logoutSubmit()">
                                        Đăng xuất
                                    </a>
                                </form>
                            </c:if>
                        </li>
                        <c:choose>
                            <c:when test="${sessionScope.user != null}">
                                <c:if test="${sessionScope.user.getRole() eq '1'}">
                                    <li>
                                        <a href="/G5_SMARTBEAUTY/cart?id=${sessionScope.userid}" title="Cart">
                                            <i class="fa fa-shopping-bag" aria-hidden="true" style="color:black;"></i>
                                        </a>
                                    </li>
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <li>
                                    <a href="cart" title="Cart">
                                        <i class="fa fa-shopping-bag" aria-hidden="true" style="color:black;"></i>
                                    </a>
                                </li>
                            </c:otherwise>
                        </c:choose>

                    </ul>
                </div>
            </div>
        </header>

        <div class="wide-nav sticky-top" style="box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.125);">
            <div class="flex-row container">
                <div class="flex-col flex-center">
                    <ul class="nav header-nav nav-spacing-xlarge">
                        <li><a class="header-nav" href="/G5_SMARTBEAUTY/home" aria-current="page">Trang chủ</a></li>
                            <c:choose>
                                <c:when test="${sessionScope.user != null}">
                                    <c:choose>
                                        <c:when test="${sessionScope.user.getRole() eq '3'}">
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/service">Quản lý dịch vụ</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/product">Quản lý sản phẩm</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/order">Quản lý đặt hàng</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/stock">Quản lý kho</a></li>
                                        </c:when>
                                        <c:otherwise>
                                        <li><a class="header-nav" href="#">Giới thiệu</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/service">Dịch vụ</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/product">Sản Phẩm</a></li>
                                        <li><a class="header-nav" href="#">Đặt Lịch</a></li>
                                        <li><a class="header-nav" href="#">Lịch Sử Mua Hàng</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                <li><a class="header-nav" href="#">Giới thiệu</a></li>
                                <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/service">Dịch vụ</a></li>
                                <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/product">Sản Phẩm</a></li>
                                <li><a class="header-nav" href="#">Đặt Lịch</a></li>
                                <li><a class="header-nav" href="#">Lịch Sử Mua Hàng</a></li>
                                </c:otherwise>
                            </c:choose>
                    </ul>
                </div>
            </div>
        </div>
        <div class="body-content">
            <form method="get" class="mt-5 mb-5">
                <div class="search">
                    <div class="searchbox">
                        <h5>Từ ngày</h5>
                        <input type="date" class="form-control" required name="datefrom" value="${requestScope.datefrom}" min="1900-01-01">
                    </div>
                    <div class="searchbox">
                        <h5>Tới ngày</h5>
                        <input type="date" class="form-control" required name="dateto" value="${requestScope.dateto}" min="1900-01-01">
                    </div>
                </div>
                <div class="d-grid gap-2 d-md-flex justify-content-md-end mt-3">
                    <button class="btn btn-primary" type="submit">Tìm kiếm</button>
                </div>
            </form>
            <div class="inner-item">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Account khách hàng</th>
                            <th scope="col">Người nhận</th>
                            <th scope="col">Địa chỉ</th>
                            <th scope="col">Số điện thoại</th>
                            <th scope="col">Ngày mua</th>
                            <th scope="col">Trạng thái</th>
                            <th scope="col">Xem chi tiết</th>
                            <th scope="col">Chuyển trạng thái</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${requestScope.orders}" var="order" varStatus="loop">
                            <tr>
                                <th scope="row">${loop.count}</th>
                                <td>${order.customerName}</td>
                                <td>${order.revicerName}</td>
                                <td>${order.address}</td>
                                <td>${order.phone}</td>
                                <td>${order.orderDate}</td>
                                <td>${order.statusMess}</td>
                                <td><i class="fa-solid fa-circle-info fa-xl" onclick="location.href = '/G5_SMARTBEAUTY/admin/order/detail?id=' + ${order.orderId};" style="cursor: pointer;"></i></td>
                                <td>
                                    <c:choose>
                                        <c:when test="${order.status eq '1'}">
                                            <i class="fa-solid fa-xmark fa-xl" onclick="location.href = '/G5_SMARTBEAUTY/order/cancel?id=' + ${order.orderId};" style="cursor: pointer;"></i>
                                            <i class="fa-solid fa-box fa-xl" onclick="location.href = '/G5_SMARTBEAUTY/admin/order/change?id=' + ${order.orderId} + '&status=' + ${order.status};" style="cursor: pointer;"></i>
                                        </c:when>
                                        <c:when test="${order.status eq '2'}">
                                            <i class="fa-solid fa-motorcycle fa-xl" onclick="location.href = '/G5_SMARTBEAUTY/admin/order/change?id=' + ${order.orderId} + '&status=' + ${order.status};" style="cursor: pointer;"></i>
                                        </c:when>
                                        <c:when test="${order.status eq '3'}">
                                            Đang giao hàng
                                        </c:when>
                                        <c:when test="${order.status eq '4'}">
                                            Giao hàng thành công
                                        </c:when>
                                        <c:when test="${order.status eq '5'}">
                                            <i class="fa-solid fa-check fa-xl" onclick="location.href = '/G5_SMARTBEAUTY/admin/order/change?id=' + ${order.orderId} + '&status=' + ${order.status};" style="cursor: pointer;"></i>
                                        </c:when>
                                        <c:otherwise>
                                            Đã hủy
                                        </c:otherwise>
                                    </c:choose>

                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-3">
                        <div class="col-inner">
                            <div style="width: 50%;">
                                <div class="img-inner dark">
                                    <img width="480" height="480" src="/G5_SMARTBEAUTY/img/logo-ft.png" style="width: 100%;height: auto;">                                    
                                </div>
                            </div>
                            <p style="color:#fffff">Hệ Thống Dưỡng Sinh Đông Y, Chăm Sóc Sức Khỏe và Sắc Đẹp HAPBEAUTY SPA</p>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="col-inner">
                            <h3><b class="service-about-heading">Hỗ trợ khách hàng</b></h3>
                            <ul class="support-about" style="list-style: disc;">
                                <li><span style="font-size: 95%;"><a href="/chinh-sach-quy-dinh/">Chính sách &amp; Quy định</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/hinh-thuc-thanh-toan/">Hình thức thanh toán</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/giao-nhan/">Chính sách giao nhận</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/chinh-sach-bao-hanh/">Chính sách bảo hành</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/chinh-sach-doi-tra-hang/">Chính sách đổi trả hàng</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/bao-mat-thong-tin-khach-hang/">Bảo mật thông tin khách hàng</a></span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="col-inner text-left">
                            <h3><b class="service-about-heading">Thông tin liên hệ</b></h3>
                            <ul>
                                <li style="color:#fffff">Địa chỉ: Tầng 3, Tòa nhà Impact, 15A Đường. Số 68 - TML, P. Thạnh Mỹ Lợi, Quận 2, TP.HCM</li>
                                <li style="color:#fffff">Điện thoại: 0846555566</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="col-inner">
                            <h3 ><b class="service-about-heading">Fanpage</b></h3>
                            <div class="fb-page" data-href="https://www.facebook.com/HapBeautyvn" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/HapBeautyvn" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/HapBeautyvn">HapBeauty - Spa &amp; Healthy Joy</a></blockquote></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
