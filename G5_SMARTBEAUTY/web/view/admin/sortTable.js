document.addEventListener("DOMContentLoaded", function () {
    const headers = document.querySelectorAll(".sort-link");
    const tbody = document.querySelector("tbody");
    const rows = Array.from(tbody.querySelectorAll("tr"));

    headers.forEach(header => {
        header.addEventListener("click", function () {
            const columnIndex = parseInt(header.getAttribute("data-column"));
            const currentOrder = header.classList.contains("asc") ? "desc" : "asc";

            headers.forEach(h => h.classList.remove("asc", "desc"));
            header.classList.add(currentOrder);

            sortTable(columnIndex, currentOrder);
        });
    });

    function sortTable(columnIndex, order) {
        rows.sort((a, b) => {
            const aValue = a.cells[columnIndex].textContent;
            const bValue = b.cells[columnIndex].textContent;

            if (order === "asc") {
                if (!isNaN(aValue) && !isNaN(bValue)) {
                    return parseFloat(aValue) - parseFloat(bValue);
                }
                return aValue.localeCompare(bValue);
            } else if (order === "desc") {
                if (!isNaN(aValue) && !isNaN(bValue)) {
                    return parseFloat(bValue) - parseFloat(aValue);
                }
                return bValue.localeCompare(aValue);
            }
        });

        tbody.innerHTML = "";
        rows.forEach(row => tbody.appendChild(row));
    }
});
