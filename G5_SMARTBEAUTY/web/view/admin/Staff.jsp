<%-- 
    Document   : adminDashboard
    Created on : Aug 14, 2023, 6:33:22 AM
    Author     : LENOVO
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <link rel="shortcut icon" href="/G5_SMARTBEAUTY/img/logo-ft.png"/>
        <title>Administration Dashboard</title>
        <link rel="stylesheet" href="/G5_SMARTBEAUTY/view/admin/staff.css?version=2" />
        <!-- Font Awesome Cdn Link -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"/>
    </head>
    <body>
        <%
            int totalPages = (Integer) request.getAttribute("totalPage");
            int currentPage;
            String pageParam = request.getParameter("page");
            if (pageParam == null) {
            currentPage = 1; // Default to the first page
            } else {
            currentPage = Integer.parseInt(pageParam);
            if (currentPage < 1) {
            currentPage = 1;
            } else if (currentPage > totalPages) {
            currentPage = totalPages;
            }
            }

            int maxVisibleLinks = 5; // Set the maximum number of visible page links
            int halfVisibleLinks = (maxVisibleLinks - 1) / 2;
            
            //combine with search
            String qSearchValue = "";
            String qSearchURL = "";
            if(request.getParameter("q") != null) qSearchValue = request.getParameter("q");
            if(!qSearchValue.isEmpty()){
                qSearchURL = "&q="+qSearchValue;
            }
            
            String message = "";
            if(request.getParameter("message") != null) message = request.getParameter("message");
        %>
        <div class="containerr">
            <nav>
                <ul>
                    <li><a href="/G5_SMARTBEAUTY/admin" class="logo">
                            <img src="/G5_SMARTBEAUTY/img/logo-ft.png" alt="">
                            <span class="nav-item">DashBoard</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/home">
                            <i class="fas fa-home"></i>
                            <span class="nav-item">Home</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/material">
                            <i class="fas fa-sharp fa-regular fa-folder"></i>
                            <span class="nav-item">Material</span>
                        </a></li>
                    <li class="current"><a href="">
                            <i class="fas fa-chart-bar"></i>
                            <span class="nav-item">Employee</span>
                        </a></li>
                    <li ><a href="/G5_SMARTBEAUTY/admin/customer">
                            <i class="fas fa-tasks"></i>
                            <span class="nav-item">Customer</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/admin/schedule">
                            <i class="fas fa-calendar-alt"></i>
                            <span class="nav-item">Schedule</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/logout" class="logout">
                            <i class="fas fa-sign-out-alt"></i>
                            <span class="nav-item">Log out</span>
                        </a></li>
                </ul>
            </nav>

            <section class="main">
                <section class="main-course">
                    <div class="items-controller" style="visibility: hidden;height: 0;width: 0;">
                        <select name="" id="itemperpage">
                            <option value="15" selected>15</option>
                        </select>
                    </div>
                    <ul style="list-style: none;display: flex;justify-content: space-between;">
                        <li>
                            <h1>Employees</h1>
                        </li>
                        <li>
                            <h1 style="color: red;font-weight: 800;text-transform: none"><%= message%></h1>
                        </li>
                        <li>
                            <ul style="list-style: none;display: flex;justify-content: space-between;">
                                <li style="display: flex;align-items: center;">
                                    <form action="staff">
                                        <label for="search">Search: </label>&nbsp;&nbsp;
                                        <input style="width: 234px" name="q" type="search" class="search-box nosubmit" value="${searchValue}" placeholder="ID | Username | Email" id="search">
                                    </form>
                                </li>
                                <li>&nbsp;&nbsp;&nbsp;&nbsp;</li>
                                <li>
                                    <button class="open-modal-btn" onclick="clearPopup()"><i class="fas fa-plus"></i>&nbsp;&nbsp;&nbsp;<h1> Employee</h1></button>
                                </li>
                            </ul>
                        </li>
                    </ul>

                    <div class="course-box">
                        <div class="tableFixHead" style="height: 100%;overflow-y: scroll">
                            <table class="content-table" id="content-table" style="width:100%">
                                <thead style="text-align-last: center;">
                                    <tr>
                                        <th data-column="0" class="sort-link" style="width: 5px;padding-right: 0;">#</th>
                                        <th data-column="1" class="sort-link">Full Name</th>
                                        <th data-column="2" class="sort-link">Address</th>
                                        <th data-column="3" class="sort-link">Email</th>
                                        <th data-column="4" class="sort-link">Phone</th>
                                        <th data-column="5" class="sort-link">Date of birth</th>
                                        <th data-column="6" class="sort-link">Gender</th>
                                        <th data-column="7" class="sort-link">Status</th>
                                        <th >Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:set var="order" value="${requestScope.orderNum+1}" />
                                    <c:forEach items="${empList}" var="e" varStatus="count">
                                        <tr>
                                            <td class="table-center" style="">${order}</td>
                                            <td>${e.getDisplayName()}</td>
                                            <td>${e.getAddress()}</td>
                                            <td style="text-transform: none">${e.getEmail()}</td>
                                            <td class="table-center">${e.getPhone()}</td>
                                            <td class="table-center">${e.getDob()}</td>
                                            <td class="table-center">
                                                <c:if test="${e.getGender() eq '1'}">Male
                                                </c:if>
                                                <c:if test="${e.getGender() eq '0'}">Female
                                                </c:if>
                                            </td>
                                            <td class="table-center">
                                                <c:if test="${e.isStatus() eq 'true'}">Enable
                                                </c:if>
                                                <c:if test="${e.isStatus() eq 'false'}">Disable
                                                </c:if>

                                            </td>
                                            <td class="table-center" style="display: flex;justify-content: space-evenly;">
                                                <i style="cursor: pointer" class="fas fa-user-cog" onclick="openUserPopup('${e.getUid()}', '${e.getDisplayName()}', '${e.getAddress()}', '${e.getEmail()}', '${e.getPhone()}', '${e.getDob()}', '${e.getGender()}')"></i>
                                                <i style="cursor: pointer" class="fas fa-user" onclick="viewUser('${e.getUid()}', '${e.getUsername()}', '${e.getDisplayName()}', '${e.getAddress()}', '${e.getEmail()}', '${e.getPhone()}', '${e.getDob()}', '${e.getGender()}', '${e.isStatus()}')"></i>
                                                <c:if test="${e.isStatus() eq 'true'}"><i style="cursor: pointer" class="fas fa-lock" style="color: #ff0000;" onclick="disableUser('${e.getUid()}', '${e.getUsername()}')"></i>
                                                </c:if>
                                                <c:if test="${e.isStatus() eq 'false'}"><i style="cursor: pointer" class="fas fa-unlock-alt" style="color: #ff0000;" onclick="enableUser('${e.getUid()}', '${e.getUsername()}')"></i>
                                                </c:if>
                                            </td>
                                        </tr>
                                        <c:set var="order" value="${order+1}" />
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="bottom-field">
                        <div class="bottom-field">
                            <ul class="pagination">
                                <% if (currentPage > 1) { %>
                                <li class="prev"><a href="staff?page=<%= currentPage - 1 %><%= qSearchURL %>">&#139;</a></li>
                                    <% } %>

                                <% if (currentPage - halfVisibleLinks > 1) { %>
                                <li><a href="staff?page=1<%= qSearchURL %>">1</a></li>
                                    <% if (currentPage - halfVisibleLinks > 2) { %>
                                <li class="ellipsis">...</li>
                                    <% } %>
                                    <% } %>

                                <% for (int i = Math.max(1, currentPage - halfVisibleLinks); i <= Math.min(currentPage + halfVisibleLinks, totalPages); i++) { %>
                                <li <% if (i == currentPage) { %>class="active"<% } %>><a href="staff?page=<%= i %><%= qSearchURL %>"><%= i %></a></li>
                                    <% } %>

                                <% if (currentPage + halfVisibleLinks < totalPages) { %>
                                <% if (currentPage + halfVisibleLinks < totalPages - 1) { %>
                                <li class="ellipsis">...</li>
                                    <% } %>
                                <li><a href="staff?page=<%= totalPages %><%= qSearchURL %>"><%= totalPages %></a></li>
                                    <% } %>

                                <% if (currentPage < totalPages) { %>
                                <li class="next"><a href="staff?page=<%= currentPage + 1 %><%= qSearchURL %>">&#155;</a></li>
                                    <% } %>
                            </ul>
                        </div>
                    </div>
                </section>
            </section>
        </div>

        <!--CREATE / UPDATE FORM popup-->
        <div class="modal hide" id="changeModal">
            <div style="position: relative;height: 100%;">
                <div class="modal__inner">
                    <form id="popupForm" action="staff" method="post">
                        <div class="modal__header">
                            <p>New Staff's Information</p>
                            <i class="fas fa-times"></i>
                        </div>
                        <p style="color: red;text-align: center;font-weight: 800;">${errorStaffCreate}</p>
                        <div class="modal__body">
                            <input id="id" readonly type="hidden" name="id" value="">
                            <div class="confirmPass" style="display:block">
                                <h4 class="required">Username</h4>
                                <input type="text" name="username" required minlength="6" maxlength="32" pattern="[A-Za-z0-9_!@#$%^&*()]+"title="Spaces not allowed !">
                                <h4 class="required">Password</h4>
                                <input type="password" name="password" required onchange="checkConfirmation()" minlength="6" maxlength="32" pattern="[A-Za-z0-9!@#$%^&* ]+"title="Accept characters: A-Za-z0-9!@#$%^&* !">
                                <h4 class="required">Confirm Password</h4>
                                <input type="password" name="repass" onchange="checkConfirmation()" required >
                            </div>
                            <h4 class="required">Display name</h4>
                            <input type="text" name="fullname" required minlength="6" maxlength="50" pattern="^(?!\s)[\s\S]*(?<!\s)$" title="Invalid name">
                            <h4 class="required">Address</h4>
                            <input type="text" name="address" required minlength="6" maxlength="254" pattern="^(?!\s)[\s\S]*(?<!\s)$" title="Invalid address">
                            <h4 class="required">Phone</h4>
                            <input type="text" name="phone" minlength="10" maxlength="10" required pattern="[0-9]+"title="Only numbers!">
                            <h4 class="required">Email</h4>
                            <input type="email" name="email" required maxlength="50" pattern="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$" title="Invalid email!">
                            <h4 class="required">Date of Birth</h4>
                            <input type="date" name="birthday" id="datePickerId" value="1990-10-22" min="1900-01-01"/>
                            <h4 class="required">Gender</h4>
                            <div style="text-align-last: center;">
                                <input style="width:unset" type="radio" name="gender" value="1"  checked="checked" required>
                                <label style="margin-right: 5em" for="html">Male</label>
                                <input style="width:unset" type="radio" name="gender" value="0">
                                <label for="css">Female</label><br>
                            </div>
                        </div>
                        <div class="modal__footer">
                            <button type="reset">Close</button>
                            <button name="mode" type="submit">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!--VIEWING FORM-->
        <div class="modal hide" id="viewModal">
            <div style="position: relative; height: 100%;">
                <div class="modal__inner">
                    <div class="modal__header">
                        <p>User Information</p>
                        <i class="fas fa-times" onclick="toggleView();"></i>
                    </div>
                    <div class="modal__body" id="viewModalBody">
                        <table style="width: 100%;">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><strong>Account ID:</strong></td>
                                    <td name='vid'></td>
                                </tr>
                                <tr>
                                    <td><strong>Username:</strong></td>
                                    <td name='vusername'></td>
                                </tr>
                                <tr>
                                    <td><strong>Display name:</strong></td>
                                    <td name='vfullname'></td>
                                </tr>
                                <tr>
                                    <td><strong>Current address:</strong></td>
                                    <td name='vaddress'></td>
                                </tr>
                                <tr>
                                    <td><strong>Contact email:</strong></td>
                                    <td name='vemail'></td>
                                </tr>
                                <tr>
                                    <td><strong>Phone number:</strong></td>
                                    <td name='vphone'></td>
                                </tr>
                                <tr>
                                    <td><strong>Birth Date:</strong></td>
                                    <td name='vdob'></td>
                                </tr>
                                <tr>
                                    <td><strong>Gender:</strong></td>
                                    <td name='vgender'></td>
                                </tr>
                                <tr>
                                    <td><strong>Status:</strong></td>
                                    <td name="vstatus"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal__footer">
                        <button onclick="toggleView();">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function disableUser(id, name) {
                var response = confirm("Are you sure you want to disable this user? : " + name);
                if (response) {
                    window.location.href = "staff?id=" + id + "&status=false";
                }
            }
            function enableUser(id, name) {
                var response = confirm("Are you sure you want to enable this user? : " + name);
                if (response) {
                    window.location.href = "staff?id=" + id + "&status=true";
                }
            }
        </script>
        <script src="/G5_SMARTBEAUTY/view/admin/popup.js?version=4"></script>
        <script src="/G5_SMARTBEAUTY/view/admin/staff.js?version=2"></script>
        <<script src="/G5_SMARTBEAUTY/view/admin/sortTable.js?version=2"></script>
    </body>
</html>
