const modalView = document.querySelector('#viewModal');

function viewUser(id, username, displayName, address, email, phone, dob, gender, status) {
    const viewHeader = modalView.querySelector('#viewModal .modal__header p');
    const viewBody = modalView.querySelector('#viewModalBody');

    let genderText = gender === 0 ? "Female" : "Male";
    let statusText = status === false ? "Disable" : "Enable";
    // Set the modal header text
    viewHeader.textContent = 'User Information: ' + displayName;

    // Populate the modal body with user information
    viewBody.querySelector('td[name="vid"]').textContent = id;
    viewBody.querySelector('td[name="vusername"]').textContent = username;
    viewBody.querySelector('td[name="vfullname"]').textContent = displayName;
    viewBody.querySelector('td[name="vaddress"]').textContent = address;
    viewBody.querySelector('td[name="vemail"]').textContent = email;
    viewBody.querySelector('td[name="vphone"]').textContent = phone;
    viewBody.querySelector('td[name="vdob"]').textContent = dob;
    viewBody.querySelector('td[name="vgender"]').textContent = genderText;
    viewBody.querySelector('td[name="vstatus"]').textContent = statusText;
    // Remove the 'hide' class to show the view modal
    toggleView();
}

// Function to close the view modal
function toggleView() {
    modalView.classList.toggle('hide');
}

const modal = document.querySelector('.modal');
const staffForm = modal.querySelector('#popupForm');
const openModalBtn = document.querySelector('.open-modal-btn');
const iconCloseModal = modal.querySelector('.modal__header i');
const buttonCloseModal = modal.querySelector('.modal__footer button');

function toggleModal() {
    modal.classList.toggle('hide');
}

iconCloseModal.addEventListener('click', toggleModal);
buttonCloseModal.addEventListener('click', toggleModal);

function createView() {
    var confirmDiv = document.querySelector('.confirmPass');
    confirmDiv.style.display = "block";
    var pass = confirmDiv.querySelector('input[name=password]');
    pass.required = true;
    pass.setAttribute("onchange", "checkConfirmation()");
    var input = confirmDiv.querySelector('input[name=repass]');
    input.required = true;
    input.setAttribute("onchange", "checkConfirmation()");
    var username = confirmDiv.querySelector('input[name=username]');
    username.required = true;
}

function editView() {
    var confirmDiv = document.querySelector('.confirmPass');
    confirmDiv.style.display = "none";
    var pass = confirmDiv.querySelector('input[name=password]');
    pass.required = false;
    pass.removeAttribute("onchange");
    var input = confirmDiv.querySelector('input[name=repass]');
    input.required = false;
    input.removeAttribute("onchange");
    var username = confirmDiv.querySelector('input[name=username]');
    username.required = false;
}

function openUserPopup(id, displayName, address, email, phone, dob, gender) {
    const modalHeader = modal.querySelector('.modal__header p');
    const modalBody = modal.querySelector('.modal__body');
    const heading = modalBody.querySelectorAll(".required");
    editView();

    // Set the modal header text
    modalHeader.textContent = 'User Information: ' + displayName;
    staffForm.id.value = id;
    staffForm.fullname.value = displayName;
    staffForm.address.value = address;
    staffForm.phone.value = phone;
    staffForm.email.value = email;
    staffForm.birthday.value = dob;
    staffForm.gender.value = gender;
    staffForm.mode.textContent = 'Update';

    // Remove the 'hide' class to show the modal
    toggleModal();
}

function clearPopup() {
    createView();
    const modalHeader = modal.querySelector('.modal__header p');
    modalHeader.textContent = 'New Staff\'s Information';

    staffForm.id.value = '';
    staffForm.username.value = ''; // Clear previous values if needed
    staffForm.password.value = '';
    staffForm.repass.value = '';
    staffForm.fullname.value = '';
    staffForm.address.value = '';
    staffForm.phone.value = '';
    staffForm.email.value = '';
    staffForm.birthday.value = '1990-10-22';
    staffForm.gender[1].checked = false; // Female
    staffForm.gender[0].checked = true;  // Male
    staffForm.mode.textContent = 'Create';
    toggleModal();
}

function checkConfirmation() {
    const password = document.querySelector('input[name=password]');
    const confirm = document.querySelector('input[name=repass]');
    console.log(password.value +" || "+confirm.value);
    if (confirm.value == password.value) {
        confirm.setCustomValidity('');
    } else {
        confirm.setCustomValidity('Passwords do not match');
    }
}