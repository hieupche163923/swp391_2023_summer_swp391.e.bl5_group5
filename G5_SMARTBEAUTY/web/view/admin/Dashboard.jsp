<%-- 
    Document   : adminDashboard
    Created on : Aug 14, 2023, 6:33:22 AM
    Author     : LENOVO
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <link rel="shortcut icon" href="/G5_SMARTBEAUTY/img/logo-ft.png"/>
        <title>Administration Dashboard</title>
        <link rel="stylesheet" href="view/admin/dashboard.css?version=1" />
        <!-- Font Awesome Cdn Link -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css"/>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        <script type="text/javascript">
            google.charts.load('current', {packages: ['corechart', 'line']});
            google.charts.setOnLoadCallback(drawCurveTypes);

            function drawCurveTypes() {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'X');
                data.addColumn('number', 'Product');

                data.addRows([
            <c:forEach items="${requestScope.productChart}" var="chart">
                    ['${chart.getName()}', ${chart.getPrice()}],
            </c:forEach>
                ]);

                var options = {
                    hAxis: {
                        title: 'Month'
                    },
                    vAxis: {
                        title: 'VND'
                    },
                    series: {
                        1: {curveType: 'function'}
                    }
                };

                var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                chart.draw(data, options);
            }
        </script>
    </head>
    <body>
        <div class="containerr">
            <nav>
                <ul>
                    <li class="current"><a href="#" class="logo">
                            <img src="/G5_SMARTBEAUTY/img/logo-ft.png" alt="">
                            <span class="nav-item">DashBoard</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/home">
                            <i class="fas fa-home"></i>
                            <span class="nav-item">Home</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/material">
                            <i class="fas fa-sharp fa-regular fa-folder"></i>
                            <span class="nav-item">Material</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/admin/staff">
                            <i class="fas fa-chart-bar"></i>
                            <span class="nav-item">Employee</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/admin/customer">
                            <i class="fas fa-tasks"></i>
                            <span class="nav-item">Customer</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/admin/schedule">
                            <i class="fas fa-calendar-alt"></i>
                            <span class="nav-item">Schedule</span>
                        </a></li>
                    <li><a href="/G5_SMARTBEAUTY/logout" class="logout">
                            <i class="fas fa-sign-out-alt"></i>
                            <span class="nav-item">Log out</span>
                        </a></li>
                </ul>
            </nav>

            <section class="main">
                <div class="main-top">
                    <h1>Website Overview</h1>
                </div>
                <div class="main-skills">
                    <div class="card">
                        <i class="fas fa-user-alt"></i>
                        <h3>Active Customers</h3>
                        <p>${requestScope.activeCus}/${requestScope.totalCus}</p>
                        <button onclick="window.location.href = 'admin/customer'">Manage Customers</button>
                    </div>
                    <div class="card">
                        <i class="fas fa-user-tie"></i>
                        <h3>Active Employees</h3>
                        <p>${requestScope.activeEmp}/${requestScope.totalEmp}</p>
                        <button onclick="window.location.href = 'admin/staff'">Manage Employees</button>
                    </div>
                    <div class="card">
                        <i class="fas fa-box-open"></i>
                        <h3>Total products</h3>
                        <p>${requestScope.totalProducts}</p>
                        <button onclick="window.location.href = 'admin/product'">Products</button>
                    </div>
                    <div class="card">
                        <i class="fas fa-spa"></i>
                        <h3>Total Services</h3>
                        <p>${requestScope.totalServices}</p>
                        <button onclick="window.location.href = 'service'">Services</button>
                    </div>
                </div>

                <section class="main-course">
                    <div class="course-box">
                        <ul>
                            <li class="active">Earning this year</li>
                        </ul>
                        <div id="chart_div"></div>
                    </div>
                </section>
            </section>
        </div>
    </body>
</html>
