<%-- 
    Document   : servicelist
    Created on : Aug 14, 2023, 2:58:56 AM
    Author     : doanv
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Service</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
        <link rel="stylesheet" id="flatsome-googlefonts-css" href="//fonts.googleapis.com/css?family=Playfair+Display%3Aregular%2C700%7CRoboto%3Aregular%2Cregular%7CVollkorn%3Aregular%2C700%7CDancing+Script%3Aregular%2C400&amp;display=swap&amp;ver=3.9" type="text/css" media="all">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.10.5/font/bootstrap-icons.min.css" integrity="sha512-ZnR2wlLbSbr8/c9AgLg3jQPAattCUImNsae6NHYnS9KrIwRdcY9DxFotXhNAKIKbAXlRnujIqUWoXXwqyFOeIQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/G5_SMARTBEAUTY/view/home/index.css"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(function () {
                $("#datepicker").datepicker();
            });
            function confirmAdd() {
                var message = confirm("Add this Product?");
                if (message === true) {
                    var x = document.forms["addform"]["pname"]["price"]["quantity"]["importdate"].value;
                    if (x == "") {
                        alert("Field must be filled out");
                        return false;
                    } else {
                        alert("Add success!!!");
                    }
                } else {
                    return false;
                }
            }

            function validateForm() {
                var x = document.forms["addform"]["pname"]["price"]["quantity"]["importdate"].value;
                if (x == "") {
                    alert("Field must be filled out");
                    return false;
                }
            }
            function toggleDropup() {
                var dropup = document.getElementById("myDropup");

                if (dropup.style.display === "block") {
                    dropup.style.display = "none";
                } else {
                    dropup.style.display = "block";
                }
            }
        </script>
    </head>
    <style>
        body{

        }
        .search{
            margin-bottom: 30px;
            display: flex;
            justify-content: center;
        }
        .content{
            display: flex;
            justify-content: flex-start;
            flex-wrap: wrap;
        }
        .searchbox {
            width: 30%;
            margin-left: auto;
            margin-right: 0;
        }
        .inner-content{
            border: solid black 1px;
            border-radius: 10px;
            text-align: center;
            width: fit-content;
            height: fit-content;
            margin-top: 10px;
            margin-right: 10px;

        }
        .inner-content > img{
            width: 172px;
            height: 120px;
        }
        .body-content{
            margin-left: 10%;
            margin-right: 10%;
        }
        .inner-item{
            border: 1px solid white;
            border-bottom: 4px solid white;
            box-shadow: 0px 0px 17px rgba(0,0,0,0.14);
            padding: 15px;
            border-radius: 7px;
        }
    </style>
    <body>
        <div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v17.0" nonce="tdHZraaT"></script>
        <!--header-->
        <header class="header-wrapper" style="font-family: 'Vollkorn', 'sans-serif';">
            <div class="navbar navbar-expand-lg">
                <div class="flex-row container" style="">
                    <div class="flex-col flex-left">
                        <a class="empty-a" href="tel:0846555566">
                            <i class="bi bi-telephone-fill"></i> &nbsp;		      
                            <span>0846555566</span>
                        </a>
                    </div>
                    <div class="flex-col flex-center"></div>
                    <div class="flex-col">
                        <a class="empty-a" href="https://www.facebook.com/spahapbeauty/">
                            <i class="fab fa-facebook-f" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://www.instagram.com/" >
                            <i class="fab fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://twitter.com/" >
                            <i class="fab fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="mailto:flearning2077@gmail.com" >
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-row container">
                <div class="flex-col hide-for-medium flex-left">
                    <form class="nosubmit" hidden="true">
                        <input type="search" class="search-box nosubmit" placeholder="Tìm kiếm sản phẩm" value="" name="search">
                    </form>
                </div>
                <div class="flex-col logo">
                    <a href="https://hapbeauty.com/" title="hapbeauty" rel="home">
                        <img style="max-height: 100%"
                             src="/G5_SMARTBEAUTY/img/logo-tet.jpg" 
                             class="header_logo header-logo" alt="hapbeauty">
                    </a>
                </div>
                <div class="flex-col hide-for-medium flex-right">
                    <ul class="cart">
                        <li>
                            <c:if test="${sessionScope.user == null}">
                                <a class="header-nav" href="/G5_SMARTBEAUTY/view/login/Login.jsp">
                                    ĐĂNG NHẬP
                                </a>
                            </c:if>
                            <c:if test="${sessionScope.user != null}">
                                <c:if test="${sessionScope.user.getRole() eq '1'}">
                                    <a class="header-nav" href="profile">
                                        ${sessionScope.user.getUsername()}
                                    </a>
                                </c:if>
                                <c:if test="${sessionScope.user.getRole() eq '2'}">
                                    <a class="header-nav" href="profile" style="margin-right: 20px">
                                        ${sessionScope.user.getUsername()}
                                    </a>
                                    <a class="header-nav" href="employee">
                                        Employee Dashboard
                                    </a>
                                </c:if>
                                <c:if test="${sessionScope.user.getRole() eq '3'}">
                                    <a class="header-nav" href="../admin">
                                        Administrator Dashboard
                                    </a>
                                </c:if>
                            </c:if>
                        </li>
                        <li>
                            <c:if test="${sessionScope.user != null}">
                                <button onclick="toggleDropup()">${sessionScope.user.getUsername()}</button>
                                <div id="myDropup" class="dropup-content" style="display: none;">
                                    <a href="profile?id=${sessionScope.user.getUid()}">Profile</a>
                                    <form action="/G5_SMARTBEAUTY/logout" method="post">
                                        <input type="submit" value="Đăng xuất">
                                    </form>
                                </div>
                            </c:if>
                        </li>
                        <li>
                            <a href="#" title="Cart">
                                <i class="fa fa-shopping-bag" aria-hidden="true" style="color:black;"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="wide-nav sticky-top" style="box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.125);">
            <div class="flex-row container">
                <div class="flex-col flex-center">
                    <ul class="nav header-nav nav-spacing-xlarge">
                        <li><a class="header-nav" href="/G5_SMARTBEAUTY/home" aria-current="page">Trang chủ</a></li>
                        <li><a class="header-nav" href="#">Giới thiệu</a></li>
                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/service">Dịch vụ</a>
                        </li>
                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/product">Sản phẩm</a>
                        </li>
                        <li><a class="header-nav" href="#">Liên hệ</a></li>
                        <li><a class="header-nav" href="#">Tin tức</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <form method="get" class="mt-5" action="product">
            <div class="search">
                <div class="searchbox">
                    <h5>Name</h5>
                    <input type="text" class="form-control" name="pname" value="${requestScope.searchName}">
                </div>
                <div class="searchbox">
                    <h5>Category</h5>
                    <select class="form-select" name="category">
                        <option value="">Default</option>
                        <c:forEach items="${requestScope.categories}" var="c">
                            <option value="${c.id}">${c.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="searchbox">
                    <h5>Price</h5>
                    <select class="form-select" name="price">
                        <option value="">Default</option>
                        <option value="1">Ascending</option>
                        <option value="2">Descending</option>
                    </select>
                </div>
            </div>
            <div class="d-grid gap-2 d-md-flex justify-content-md-end mt-3">
                <button class="btn btn-primary" type="submit">Search</button>
            </div>
        </form>
        <div class="content">
            <c:forEach items="${requestScope.servicelist}" var="s" varStatus="loop">
                <div class="inner-content">
                    <img src="${s.getImage()}" onclick="location.href = 'service/detail?sid=' + ${s.getId()};"/>
                    <h5>${s.name}</h5>                    
                    <h6>${s.price}&#36;</h6>
                </div>
            </c:forEach>
        </div>
        <button onclick="location.href = 'service/add'">Add new Service</button>
    </body>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-3">
                    <div class="col-inner">
                        <div style="width: 50%;">
                            <div class="img-inner dark">
                                <img width="480" height="480" src="/G5_SMARTBEAUTY/img/logo-ft.png" style="width: 100%;height: auto;">                                    
                            </div>
                        </div>
                        <p style="color:#fffff">Hệ Thống Dưỡng Sinh Đông Y, Chăm Sóc Sức Khỏe và Sắc Đẹp HAPBEAUTY SPA</p>
                    </div>
                </div>
                <div class="col-3">
                    <div class="col-inner">
                        <h3><b class="service-about-heading">Hỗ trợ khách hàng</b></h3>
                        <ul class="support-about" style="list-style: disc;">
                            <li><span style="font-size: 95%;"><a href="/chinh-sach-quy-dinh/">Chính sách &amp; Quy định</a></span></li>
                            <li><span style="font-size: 95%;"><a href="/hinh-thuc-thanh-toan/">Hình thức thanh toán</a></span></li>
                            <li><span style="font-size: 95%;"><a href="/giao-nhan/">Chính sách giao nhận</a></span></li>
                            <li><span style="font-size: 95%;"><a href="/chinh-sach-bao-hanh/">Chính sách bảo hành</a></span></li>
                            <li><span style="font-size: 95%;"><a href="/chinh-sach-doi-tra-hang/">Chính sách đổi trả hàng</a></span></li>
                            <li><span style="font-size: 95%;"><a href="/bao-mat-thong-tin-khach-hang/">Bảo mật thông tin khách hàng</a></span></li>
                        </ul>
                    </div>
                </div>

                <div class="col-3">
                    <div class="col-inner text-left">
                        <h3><b class="service-about-heading">Thông tin liên hệ</b></h3>
                        <ul>
                            <li style="color:#fffff">Địa chỉ: Tầng 3, Tòa nhà Impact, 15A Đường. Số 68 - TML, P. Thạnh Mỹ Lợi, Quận 2, TP.HCM</li>
                            <li style="color:#fffff">Điện thoại: 0846555566</li>
                        </ul>
                    </div>
                </div>

                <div class="col-3">
                    <div class="col-inner">
                        <h3 ><b class="service-about-heading">Fanpage</b></h3>
                        <div class="fb-page" data-href="https://www.facebook.com/HapBeautyvn" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/HapBeautyvn" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/HapBeautyvn">HapBeauty - Spa &amp; Healthy Joy</a></blockquote></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</html>
