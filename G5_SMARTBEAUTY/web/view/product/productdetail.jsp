<%-- 
    Document   : productdetail
    Created on : Aug 14, 2023, 1:47:48 AM
    Author     : binhp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Product Detail</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
        <link rel="stylesheet" id="flatsome-googlefonts-css" href="//fonts.googleapis.com/css?family=Playfair+Display%3Aregular%2C700%7CRoboto%3Aregular%2Cregular%7CVollkorn%3Aregular%2C700%7CDancing+Script%3Aregular%2C400&amp;display=swap&amp;ver=3.9" type="text/css" media="all">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.10.5/font/bootstrap-icons.min.css" integrity="sha512-ZnR2wlLbSbr8/c9AgLg3jQPAattCUImNsae6NHYnS9KrIwRdcY9DxFotXhNAKIKbAXlRnujIqUWoXXwqyFOeIQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/G5_SMARTBEAUTY/view/home/index.css"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    </head>
    <style>
        body{

        }
        .content{
            display: flex;
            justify-content: left;
            margin-right: 20px;
            margin-bottom: 30px;
        }
        .header{
            margin-bottom: 50px;
        }
        .comment-submit{
            border: solid red 1px;
            padding: 10px;

        }
        .comment{
            border: 1px solid white;
            border-bottom: 4px solid white;
            box-shadow: 0px 0px 17px rgba(0,0,0,0.14);
            padding: 15px;
            border-radius: 7px;
        }
        .rating-box > select{
            width: 30%;
            margin-bottom: 10px;
        }
        .inner-item{
            border: 1px solid white;
            border-bottom: 4px solid white;
            box-shadow: 0px 0px 17px rgba(0,0,0,0.14);
            padding: 15px;
            border-radius: 7px;
        }
        .body-content{
            margin-left: 20%;
            margin-right: 20%;
            margin-bottom: 30px;
            margin-top: 30px;
        }
        .textarea {
            font-family: inherit;
            font-size: inherit;
        }
        .textarea {
            display: block;
            width: 100%;
            overflow: hidden;
            min-height: 40px;
            line-height: 20px;
        }
    </style>
    <script>
        function toggleDropup() {
            var dropup = document.getElementById("myDropup");

            if (dropup.style.display === "block") {
                dropup.style.display = "none";
            } else {
                dropup.style.display = "block";
            }
        }
        function logoutSubmit() {
            document.getElementById("logoutForm").submit();
        }
        function int(value) {
            return parseInt(value);
        }
        function checkValue(sender) {
            let min = sender.min;
            let max = sender.max;
            let value = int(sender.value);
            if (value > max) {
                sender.value = min;
            } else if (value < min) {
                sender.value = max;
            }
        }
    </script>
    <body>
        <div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v17.0" nonce="tdHZraaT"></script>
        <!--header-->
        <header class="header-wrapper" style="font-family: 'Vollkorn', 'sans-serif';">
            <div class="navbar navbar-expand-lg">
                <div class="flex-row container" style="max-width: 1234px;">
                    <div class="flex-col flex-left">
                        <a class="empty-a" href="tel:0846555566">
                            <i class="bi bi-telephone-fill"></i> &nbsp;		      
                            <span>0846555566</span>
                        </a>
                    </div>
                    <div class="flex-col flex-center"></div>
                    <div class="flex-col">
                        <a class="empty-a" href="https://www.facebook.com/spahapbeauty/">
                            <i class="fab fa-facebook-f" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://www.instagram.com/" >
                            <i class="fab fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://twitter.com/" >
                            <i class="fab fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="mailto:flearning2077@gmail.com" >
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-row container" style="max-width: 1234px;">
                <div class="flex-col hide-for-medium flex-left">
                    <form class="nosubmit" hidden="true">
                        <input type="search" class="search-box nosubmit" placeholder="Tìm kiếm sản phẩm" value="" name="search">
                    </form>
                </div>
                <div class="flex-col logo">
                    <a href="/G5_SMARTBEAUTY/home" title="hapbeauty" rel="home">
                        <img style="max-height: 100%"
                             src="/G5_SMARTBEAUTY/img/logo-tet.jpg" 
                             class="header_logo header-logo" alt="hapbeauty">
                    </a>
                </div>
                <div class="flex-col hide-for-medium flex-right">
                    <ul class="cart">
                        <li>
                            <c:if test="${sessionScope.user == null}">
                                <a class="header-nav" href="/G5_SMARTBEAUTY/view/login/Login.jsp">
                                    ĐĂNG NHẬP
                                </a>
                            </c:if>
                            <c:if test="${sessionScope.user != null}">
                                <c:if test="${sessionScope.user.getRole() eq '1'}">
                                    <a class="header-nav" href="/G5_SMARTBEAUTY/profile?id=${sessionScope.user.getUid()}">
                                        ${sessionScope.user.getUsername()}
                                    </a>
                                </c:if>
                                <c:if test="${sessionScope.user.getRole() eq '2'}">
                                    <a class="header-nav" href="/G5_SMARTBEAUTY/profile?id=${sessionScope.user.getUid()}" style="margin-right: 20px">
                                        ${sessionScope.user.getUsername()}
                                    </a>
                                    <a class="header-nav" href="/G5_SMARTBEAUTY/employee">
                                        Employee Dashboard
                                    </a>
                                </c:if>
                                <c:if test="${sessionScope.user.getRole() eq '3'}">
                                    <a class="header-nav" href="/G5_SMARTBEAUTY/admin">
                                        Administrator Dashboard
                                    </a>
                                </c:if>

                            </c:if>
                        </li>
                        <li>
                            <c:if test="${sessionScope.user != null}">
                                <form id="logoutForm" action="/G5_SMARTBEAUTY/logout" method="post">
                                    <a id="logoutNav" class="header-nav" href="#" onclick="logoutSubmit()">
                                        Đăng xuất
                                    </a>
                                </form>
                            </c:if>
                        </li>
                        <c:choose>
                            <c:when test="${sessionScope.user != null}">
                                <c:if test="${sessionScope.user.getRole() eq '1'}">
                                    <li>
                                        <a href="/G5_SMARTBEAUTY/cart?id=${sessionScope.userid}" title="Cart">
                                            <i class="fa fa-shopping-bag" aria-hidden="true" style="color:black;"></i>
                                        </a>
                                    </li>
                                </c:if>
                            </c:when>
                            <c:otherwise>
                                <li>
                                    <a href="cart" title="Cart">
                                        <i class="fa fa-shopping-bag" aria-hidden="true" style="color:black;"></i>
                                    </a>
                                </li>
                            </c:otherwise>
                        </c:choose>

                    </ul>
                </div>
            </div>
        </header>

        <div class="wide-nav sticky-top" style="box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.125);">
            <div class="flex-row container">
                <div class="flex-col flex-center">
                    <ul class="nav header-nav nav-spacing-xlarge">
                        <li><a class="header-nav" href="/G5_SMARTBEAUTY/home" aria-current="page">Trang chủ</a></li>
                            <c:choose>
                                <c:when test="${sessionScope.user != null}">
                                    <c:choose>
                                        <c:when test="${sessionScope.user.getRole() eq '3'}">
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/service">Quản lý dịch vụ</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/product">Quản lý sản phẩm</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/order">Quản lý đặt hàng</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/admin/stock">Quản lý kho</a></li>
                                        </c:when>
                                        <c:otherwise>
                                        <li><a class="header-nav" href="#">Giới thiệu</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/service">Dịch vụ</a></li>
                                        <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/product">Sản Phẩm</a></li>
                                        <li><a class="header-nav" href="#">Đặt Lịch</a></li>
                                        <li><a class="header-nav" href="/G5_SMARTBEAUTY/order?id=${sessionScope.userid}">Lịch Sử Mua Hàng</a></li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:when>
                                <c:otherwise>
                                <li><a class="header-nav" href="#">Giới thiệu</a></li>
                                <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/service">Dịch vụ</a></li>
                                <li class="dropdown"><a class="header-nav" href="/G5_SMARTBEAUTY/product">Sản Phẩm</a></li>
                                <li><a class="header-nav" href="#">Đặt Lịch</a></li>
                                <li><a class="header-nav" href="/G5_SMARTBEAUTY/order?id=${sessionScope.userid}">Lịch Sử Mua Hàng</a></li>
                                </c:otherwise>
                            </c:choose>
                    </ul>
                </div>
            </div>
        </div>
        <div class="body-content">

            <div class="content">
                <div style="margin-right: 20px;" class="inner-item">
                    <img src="${requestScope.product.image}" style="width: 500px; height: 500px;"/>
                </div>
                <div style="width: -webkit-fill-available;">
                    <h3>${requestScope.product.name}</h3>
                    <h4>${requestScope.product.price}₫</h4>
                    <h6 style="color: red;">Kho: ${requestScope.product.quantity}</h6>
                    <h6 style="color: red;">Danh mục: ${requestScope.product.category}</h6>
                    <h6 style="color: red;">Nhà sản xuất: ${requestScope.product.supplier}</h6>
                    <c:if test="${sessionScope.user != null}">
                        <c:if test="${sessionScope.user.getRole() eq '1'}">
                            <c:if test="${requestScope.status eq '1'}">
                                <form class="cart" action="/G5_SMARTBEAUTY/cart/add" method="post">
                                    <div class="quantity buttons_added me-3">
                                        <input type="text" value="${requestScope.productid}" name="productid" hidden>
                                        <input type="number" class="form-control" step="1" min="1" max="${requestScope.product.quantity}" name="amount" value="1" size="4" required oninput="checkValue(this);">
                                    </div>
                                    <c:if test="${requestScope.product.quantity eq '0'}">
                                        <h6 style="color: red;">Sản phẩm này tạm hết hàng</h6>
                                    </c:if>
                                    <c:if test="${requestScope.product.quantity ne '0'}">
                                        <button type="submit" class="btn btn-primary">Thêm vào giỏ hàng</button>
                                    </c:if>
                                </form>
                            </c:if>
                            <c:if test="${requestScope.status eq '0'}">
                                <h4>Sản phẩm này đã tạm dừng kinh doanh</h4>
                            </c:if>
                        </c:if>
                        <c:if test="${sessionScope.user.getRole() eq '2'}">
                            <h4>You are not a customer</h4>
                        </c:if>
                        <c:if test="${sessionScope.user.getRole() eq '3'}">
                            <h4>You are not a customer</h4>
                        </c:if>
                    </c:if>
                    <c:if test="${sessionScope.user == null}">
                        <c:if test="${requestScope.status eq '1'}">
                            <div class="cart">
                                <div class="quantity buttons_added me-3">
                                    <input type="number" class="form-control" step="1" min="1" max="${requestScope.product.quantity}" name="quantity" value="1" size="4" required oninput="checkValue(this);">
                                </div>
                                <c:if test="${requestScope.product.quantity eq '0'}">
                                    <h6 style="color: red;">Sản phẩm này tạm hết hàng</h6>
                                </c:if>
                                <c:if test="${requestScope.product.quantity ne '0'}">
                                    <button type="submit" class="btn btn-primary" onclick="location.href = '/G5_SMARTBEAUTY/view/login/Login.jsp';">Thêm vào giỏ hàng</button>
                                </c:if>
                            </div>
                        </c:if>
                        <c:if test="${requestScope.status eq '0'}">
                            <h4>Sản phẩm này đã tạm dừng kinh doanh</h4>
                        </c:if>
                    </c:if>
                    <h5 style="color: red; text-align: left; margin-top: 10px; margin-bottom: 30px;">${requestScope.mess}</h5>
                    <div class="form-floating">
                        <p><strong>Mô tả sản phẩm:</strong> <span class="textarea" role="textbox">${requestScope.product.description}</span></p>
                    </div>
                </div>
            </div>
            <div class="comment">
                <h4>Đánh giá sản phẩm</h4>
                <h5>User 1: I just buy it yesterday and it is so good!!!</h5>
                <form method="post" class="comment-submit" style="margin-top: 30px;">
                    <div class="rating-box">
                        <h5>Mức độ hài lòng</h5>
                        <select class="form-select" name="rating">
                            <option value="1">Tốt</option>
                            <option value="2">Bình thường</option>
                            <option value="3">Chê</option>
                        </select>
                    </div>
                    <div class="comment-box">
                        <h5>Đánh giá của bạn</h5>
                        <textarea cols="80"rows="8" required></textarea>
                    </div>
                    <button class="btn btn-primary" type="button">Đăng</button>
                </form>
            </div>
        </div>
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-3">
                        <div class="col-inner">
                            <div style="width: 50%;">
                                <div class="img-inner dark">
                                    <img width="480" height="480" src="/G5_SMARTBEAUTY/img/logo-ft.png" style="width: 100%;height: auto;">                                    
                                </div>
                            </div>
                            <p style="color:#fffff">Hệ Thống Dưỡng Sinh Đông Y, Chăm Sóc Sức Khỏe và Sắc Đẹp HAPBEAUTY SPA</p>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="col-inner">
                            <h3><b class="service-about-heading">Hỗ trợ khách hàng</b></h3>
                            <ul class="support-about" style="list-style: disc;">
                                <li><span style="font-size: 95%;"><a href="/chinh-sach-quy-dinh/">Chính sách &amp; Quy định</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/hinh-thuc-thanh-toan/">Hình thức thanh toán</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/giao-nhan/">Chính sách giao nhận</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/chinh-sach-bao-hanh/">Chính sách bảo hành</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/chinh-sach-doi-tra-hang/">Chính sách đổi trả hàng</a></span></li>
                                <li><span style="font-size: 95%;"><a href="/bao-mat-thong-tin-khach-hang/">Bảo mật thông tin khách hàng</a></span></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="col-inner text-left">
                            <h3><b class="service-about-heading">Thông tin liên hệ</b></h3>
                            <ul>
                                <li style="color:#fffff">Địa chỉ: Tầng 3, Tòa nhà Impact, 15A Đường. Số 68 - TML, P. Thạnh Mỹ Lợi, Quận 2, TP.HCM</li>
                                <li style="color:#fffff">Điện thoại: 0846555566</li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-3">
                        <div class="col-inner">
                            <h3 ><b class="service-about-heading">Fanpage</b></h3>
                            <div class="fb-page" data-href="https://www.facebook.com/HapBeautyvn" data-tabs="" data-width="" data-height="" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/HapBeautyvn" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/HapBeautyvn">HapBeauty - Spa &amp; Healthy Joy</a></blockquote></div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
