<%-- 
    Document   : addMaterial
    Created on : Aug 15, 2023, 2:43:30 PM
    Author     : khanh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Edit Material</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
        <link rel="stylesheet" id="flatsome-googlefonts-css" href="//fonts.googleapis.com/css?family=Playfair+Display%3Aregular%2C700%7CRoboto%3Aregular%2Cregular%7CVollkorn%3Aregular%2C700%7CDancing+Script%3Aregular%2C400&amp;display=swap&amp;ver=3.9" type="text/css" media="all">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.10.5/font/bootstrap-icons.min.css" integrity="sha512-ZnR2wlLbSbr8/c9AgLg3jQPAattCUImNsae6NHYnS9KrIwRdcY9DxFotXhNAKIKbAXlRnujIqUWoXXwqyFOeIQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/G5_SMARTBEAUTY/view/home/index.css"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(function () {
                $("#datepicker").datepicker();
            });
            function confirmEdit() {
                var message = confirm("Edit this Material?");
                if (message === true) {
                    var x = document.forms["addform"]["mname"]["mid"]["quantity"]["category"]["description"].value;
                    if (x == "") {
                        alert("Field must be filled out");
                        return false;
                    } else {
                        alert("Add success!!!");
                    }
                } else {
                    return false;
                }
            }

            function validateForm() {
                var x = document.forms["addform"]["mname"]["mid"]["quantity"]["category"]["description"].value;
                if (x == "") {
                    alert("Field must be filled out");
                    return false;
                }
            }
        </script>
    </head>
    <style>
        body{
            margin-left: 5%;
            margin-right: 5%;
        }
        .search{
            margin-bottom: 30px;
            display: flex;
            justify-content: center;
        }
        .content{
            display: flex;
            justify-content: flex-start;
            flex-wrap: wrap;
        }
        .searchbox {
            width: 30%;
            margin-left: auto;
            margin-right: 0;
        }
        .inner-content{
            border: solid black 1px;
            border-radius: 10px;
            text-align: center;
            width: fit-content;
            height: fit-content;
            margin-top: 10px;
            margin-right: 10px;

        }
        .inner-content > img{
            width: 172px;
            height: 120px;
        }
    </style>
    <body>
        <div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v17.0" nonce="tdHZraaT"></script>
        <!--header-->
        <header class="header-wrapper" style="font-family: 'Vollkorn', 'sans-serif';">
            <div class="navbar navbar-expand-lg">
                <div class="flex-row container" style="">
                    <div class="flex-col flex-left">
                        <a class="empty-a" href="tel:0846555566">
                            <i class="bi bi-telephone-fill"></i> &nbsp;		      
                            <span>0846555566</span>
                        </a>
                    </div>
                    <div class="flex-col flex-center"></div>
                    <div class="flex-col">
                        <a class="empty-a" href="https://www.facebook.com/spahapbeauty/">
                            <i class="fab fa-facebook-f" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://www.instagram.com/" >
                            <i class="fab fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://twitter.com/" >
                            <i class="fab fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="mailto:flearning2077@gmail.com" >
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-row container">
                <div class="flex-col hide-for-medium flex-left">
                    <form class="nosubmit">
                        <input type="search" class="search-box nosubmit" placeholder="Tìm kiếm sản phẩm" value="" name="search">
                    </form>
                </div>
                <div class="flex-col logo">
                    <a href="https://hapbeauty.com/" title="hapbeauty" rel="home">
                        <img style="max-height: 100%"
                             src="/G5_SMARTBEAUTY/img/logo-tet.jpg" 
                             class="header_logo header-logo" alt="hapbeauty">
                    </a>
                </div>
                <div class="flex-col hide-for-medium flex-right">
                    <ul class="cart">
                        <li>
                            <a class="header-nav" href="../login/Login.jsp">
                                <c:if test="${sessionScope.user == null}">ĐĂNG NHẬP
                                </c:if>
                            </a>
                            <a class="header-nav" href="profile?id=${sessionScope.user.uid}">
                                <c:if test="${sessionScope.user != null}">${sessionScope.user.getUsername()}</c:if>
                                </a>
                            </li>

                            <li>
                                <a href="https://hapbeauty.com/gio-hang/" title="Cart">
                                    <i class="fa fa-shopping-bag" aria-hidden="true" style="color:black;"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </header>
            <div class="wide-nav sticky-top mb-5" style="box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.125);">
                <div class="flex-row container">
                    <div class="flex-col flex-center">
                        <ul class="nav header-nav nav-spacing-xlarge">
                            <li><a class="header-nav" href="https://hapbeauty.com/" aria-current="page">Trang chủ</a></li>
                            <li><a class="header-nav" href="https://hapbeauty.com/gioi-thieu-2/">Giới thiệu</a></li>
                            <li class="dropdown"><a class="header-nav" href="#">Dịch vụ<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                <ul class="dropdown-content">
                                    <li class="dropdown-menu-item"><a class="dropdown-header">DỊCH VỤ</a>
                                        <ul class="sub-menu nav-column nav-dropdown-simple">
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/dieu-tri-nam-chuyen-sau/">Điều trị nám chuyên sâu</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/khoi-phuc-da-nhiem-corticoid/">Khôi phục da hư tổn</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/lieu-trinh-dieu-tri-mun-kiem-soat-dau/">Điều trị mụn chuyên sâu</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/triet-long-vinh-vien/">Triệt lông vĩnh viễn</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/dich-vu-giam-beo/">Dịch Vụ Giảm Béo</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/dich-vu-lam-trang/">Chăm sóc da siêu dưỡng chất, dưỡng ẩm sáng mịn</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/lieu-trinh-co-tre-hoa-tuoi-thanh-xuan/">Liệu trình cơ trẻ hóa tuổi thanh xuân</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/dich-vu-nang-co-tre-hoa/">Dịch Vụ Nâng Cơ Trẻ Hóa</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/cham-soc-da-co-ban/">Chăm Sóc Da Cơ Bản</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/cham-soc-da-chuyen-sau/">Chăm Sóc Da Chuyên Sâu</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-menu-item"><a class="dropdown-header">DƯỠNG SINH ĐÔNG Y</a>
                                        <ul class="sub-menu">
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/dieu-tri-dau-co-vai-gay-dong-y/">Điều trị đau cổ vai gáy Đông Y</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/thanh-loc-thai-doc-co-the-dong-y/">Thanh lọc thải độc cơ thể Đông Y</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/lieu-trinh-duong-mau/">Liệu trình dưỡng máu</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/lieu-trinh-bo-than-cuong-than/">Liệu trình bổ thận cường thận</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/thong-kinh-duong-buong-trung-dong-y/">Thống kinh dưỡng buồng trứng Đông Y</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/lieu-trinh-khung-chau-dong-y/">Liệu trình khung chậu Đông Y</a></li>
                                            <li><a href="https://hapbeauty.com/danh-muc-san-pham/thai-doc-gan-dong-y/">Thải độc gan Đông Y</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown"><a class="header-nav" href="#">Sản phẩm<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                <ul class="dropdown-content">
                                    <div class="dropdown-menu-item">
                                        <li><a href="https://hapbeauty.com/danh-muc-san-pham/san-pham/san-pham-hapflower-chuyen-sau/">HapFlower Chuyên Sâu</a></li>
                                        <li><a href="https://hapbeauty.com/danh-muc-san-pham/san-pham/san-pham-hapflower-co-ban/">HapFlower Cơ Bản</a></li>
                                        <li><a href="https://hapbeauty.com/danh-muc-san-pham/milyclean/">MilyClean</a></li>
                                        <li><a href="https://hapbeauty.com/danh-muc-san-pham/formylook">FormyLook</a></li>
                                    </div>
                                </ul>
                            </li>
                            <li><a class="header-nav" href="https://hapbeauty.com/thong-tin-khach-hang/">Liên hệ</a></li>
                            <li><a class="header-nav" href="https://hapbeauty.com/category/tin-tuc/">Tin tức</a></li>
                        </ul>
                    </div>
                </div>
            </div>            <form name="addform" method="post" onsubmit="return validateForm()" required>


                <div class="input-content">
                    <div class="input-group input-group-sm mb-3">
                        <span class="input-group-text" id="inputGroup-sizing-sm">Material name </span>
                        <input  type="text" class="form-control" required name="mname" value="${requestScope.m.mname}">
                </div>

                <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Quantity</span>
                    <input type="number" class="form-control" required name="quantity"value="${requestScope.m.quantity}">
                </div>
                <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Category</span>
                    <select class="form-select" name="category" required >
                        <c:forEach items="${requestScope.categories}" var="c">
                            <option ${c.id==requestScope.m.category ?'selected':""} value="${c.id}">${c.name}   </option>
                        </c:forEach>
                    </select>
                </div>
                <div class="input-group input-group-sm mb-3">
                    <span class="input-group-text" id="inputGroup-sizing-sm">Material description </span>
                    <input type="text" class="form-control" required name="description"value="${requestScope.m.description}">
                </div>
            </div>
            <div class="d-grid gap-2 d-md-flex justify-content-md-end mt-3">
                <button class="btn btn-primary" type="submit" onclick="confirmEdit()">Save</button>
            </div>
        </form>
    </body>
</html>
