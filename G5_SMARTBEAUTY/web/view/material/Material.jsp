<%-- 
    Document   : adminproducts
    Created on : Aug 13, 2023, 11:07:11 PM
    Author     : binhp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin Material</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
        <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet'>
        <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
        <link rel="stylesheet" id="flatsome-googlefonts-css" href="//fonts.googleapis.com/css?family=Playfair+Display%3Aregular%2C700%7CRoboto%3Aregular%2Cregular%7CVollkorn%3Aregular%2C700%7CDancing+Script%3Aregular%2C400&amp;display=swap&amp;ver=3.9" type="text/css" media="all">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-icons/1.10.5/font/bootstrap-icons.min.css" integrity="sha512-ZnR2wlLbSbr8/c9AgLg3jQPAattCUImNsae6NHYnS9KrIwRdcY9DxFotXhNAKIKbAXlRnujIqUWoXXwqyFOeIQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/G5_SMARTBEAUTY/view/home/index.css"/>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script>
    </head>
    <style>
        body{
            margin-left: 5%;
            margin-right: 5%;
        }
        .search{
            margin-bottom: 30px;
            display: flex;
            justify-content: center;
        }
        .content{
            display: flex;
            justify-content: flex-start;
            flex-wrap: wrap;
        }
        .searchbox {
            width: 30%;
            margin-left: auto;
            margin-right: 0;
        }
        .inner-content{
            border: solid black 1px;
            border-radius: 10px;
            text-align: center;
            width: fit-content;
            height: fit-content;
            margin-top: 10px;
            margin-right: 10px;
            padding: 5px;

        }
        .inner-content > img{
            width: 172px;
            height: 120px;
        }
    </style>
    <script>
        function confirmDelete(id) {
            var message = confirm("Delete this Material");
            if (message === true) {
                window.location.href = "http://localhost:9999/G5_SMARTBEAUTY/DeleteMaterial?mid=" + id;
            } else {
                return false;
            }
        }
    </script>
    <body>
        <div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v17.0" nonce="tdHZraaT"></script>
        <!--header-->
        <header class="header-wrapper" style="font-family: 'Vollkorn', 'sans-serif';">
            <div class="navbar navbar-expand-lg">
                <div class="flex-row container" style="">
                    <div class="flex-col flex-left">
                        <a class="empty-a" href="tel:0846555566">
                            <i class="bi bi-telephone-fill"></i> &nbsp;		      
                            <span>0846555566</span>
                        </a>
                    </div>
                    <div class="flex-col flex-center"></div>
                    <div class="flex-col">
                        <a class="empty-a" href="https://www.facebook.com/spahapbeauty/">
                            <i class="fab fa-facebook-f" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://www.instagram.com/" >
                            <i class="fab fa-instagram" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="https://twitter.com/" >
                            <i class="fab fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a class="empty-a" href="mailto:flearning2077@gmail.com" >
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="flex-row container">
                <div class="flex-col hide-for-medium flex-left">
                    <form class="nosubmit">
                        <input type="search" class="search-box nosubmit" placeholder="Tìm kiếm sản phẩm" value="" name="search">
                    </form>
                </div>
                <div class="flex-col logo">
                    <a href="https://hapbeauty.com/" title="hapbeauty" rel="home">
                        <img style="max-height: 100%"
                             src="/G5_SMARTBEAUTY/img/logo-tet.jpg" 
                             class="header_logo header-logo" alt="hapbeauty">
                    </a>
                </div>
                <div class="flex-col hide-for-medium flex-right">
                    <ul class="cart">
                        <li>
                            <a class="header-nav" href="../login/Login.jsp">
                                <c:if test="${sessionScope.user == null}">ĐĂNG NHẬP
                                </c:if>
                            </a>
                            <a class="header-nav" href="profile?id=${sessionScope.user.uid}">
                                <c:if test="${sessionScope.user != null}">${sessionScope.user.getUsername()}</c:if>
                                </a>
                            </li>

                            <li>
                                <a href="https://hapbeauty.com/gio-hang/" title="Cart">
                                    <i class="fa fa-shopping-bag" aria-hidden="true" style="color:black;"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </header>
            <div class="wide-nav sticky-top" style="box-shadow: 0px 10px 10px rgba(0, 0, 0, 0.125);">
                <div class="flex-row container">
                    <div class="flex-col flex-center">
                        <ul class="nav header-nav nav-spacing-xlarge">
                            <li><a class="header-nav" href="../home" aria-current="page">Trang chủ</a></li>
                            <li><a class="header-nav" href="https://hapbeauty.com/gioi-thieu-2/">Giới thiệu</a></li>
                            <li class="dropdown"><a class="header-nav" href="service">Dịch vụ</a>
                            </li>
                            <li class="dropdown"><a class="header-nav" href="product">Sản phẩm</a>
                            </li>
                            <li><a class="header-nav" href="https://hapbeauty.com/thong-tin-khach-hang/">Liên hệ</a></li>
                            <li><a class="header-nav" href="https://hapbeauty.com/category/tin-tuc/">Tin tức</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <form method="get">
                <input class="searchBox"type="text" name="query" size="30" required>
                <input class="searchButton"type="submit" size="15" value="Search">
            </form>
            <div class="content">
            <c:forEach items="${requestScope.material}" var="material" varStatus="loop">
                <div class="inner-content">
                    <img src="/G5_SMARTBEAUTY/img/logo-ft.png" onclick="location.href = '/G5_SMARTBEAUTY/material/detail?mid=' + ${material.getMid()};"/>
                    <h5>${material.mname}</h5>                    
                    <h6>${material.mid}</h6>

                    <button type="button" class="btn btn-primary" onclick="confirmDelete(${material.mid})">Delete</button>
                </div>
            </c:forEach>
        </div>                
    </div>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end mt-3">
        <input class="btn btn-primary me-md-2" type="button" name="add" value="Add new material" onclick="location.href = '/G5_SMARTBEAUTY/material/add';">
    </div>
</body>
</html>
