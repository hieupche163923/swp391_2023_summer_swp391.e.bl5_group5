<%-- 
    Document   : Register
    Created on : Jun 4, 2023, 1:11:27 PM
    Author     : msii
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register page</title>

        <!--        lLink css-->
        <link rel="stylesheet" href="Register.css"/>


    </head>
    <body>



        <div class="center">
            <div class="logo-logsig">
                <!-- Tạo đối tượng RequestDispatcher -->
                <%
                String servletPath = "/home";
                RequestDispatcher dispatcher = request.getRequestDispatcher(servletPath);
                %>

                <!-- Tạo đường dẫn đến servlet -->
                <a class="card" href="<%= request.getContextPath() %><%= servletPath %>"><img src="../../img/logo-ft.png" width="400px" height="370px" alt="logoHome"/></a>
            </div>
            <h1>REGISTER</h1>
            <div class="text-danger"> ${mess1}</div>
            <form action="registercontroller" method="post">
                <!--                <div class="txt_field">
                                    <input type="text" name="userid" required>
                                    <span></span>
                                    <label>UserID: Only Number</label>
                                </div>-->
                <div class="txt_field">
                    <input type="text" name="username" value="${userfake.username}" required minlength="5" maxlength="32" pattern="[A-Za-z0-9].+"title="Spaces not allowed !">
                    <span></span>
                    <label>Username</label>
                </div>
                <div class="txt_field">
                    <input type="password" name="password" value="${userfake.password}" required minlength="6" maxlength="32" pattern="[A-Za-z0-9!@#$%^&*]+"title="Spaces not allowed !">
                    <span></span>
                    <label>Password</label>
                </div>
                <div class="txt_field">
                    <input type="text" name="fullname" value="${userfake.displayName}" required minlength="6" maxlength="50" pattern="^(?!\s)[\s\S]*(?<!\s)$" title="Invalid name">
                    <span></span>
                    <label>Full Name</label>
                </div>
                <div class="txt_field">
                    <input type="text" name="address" value="${userfake.address}" required minlength="4" maxlength="254" pattern="^(?!\s)[\s\S]*(?<!\s)$" title="Invalid address">
                    <span></span>
                    <label>Address</label>
                </div>
                <div class="text-danger"> ${mess2}</div>
                <div class="txt_field">
                    <input type="text" name="phone" value="${userfake.phone}" required>
                    <span></span>
                    <label>Phone</label>
                </div>
                <div class="text-danger"> ${mess3}</div>
                <div class="text-danger"> ${mess0}</div>
                <div class="txt_field">
                    <input type="text" name="email" value="${userfake.email}" required>
                    <span></span>
                    <label>Email</label>
                </div>
                <div class="txt_field">
                    <input type="date" name="birthday" id="datePickerId" value="${userfake.dob}" required min="1900-01-01"/>
                    <!--                    <input type="date" id="birthday" name="birthday">-->
                </div>
                <div class="Gender">
                    <input type="radio" name="gender" value="1"  checked="checked">
                      <label for="html">Male</label><br>
                      <input type="radio" name="gender" value="0">
                      <label for="css">Female</label><br>
                </div>

                <input type="submit" value="SUBMIT">
                <p>
                    By clicking the Sign Up button,you agree to our <br />
                    <a href="#">Terms and Condition</a> and <a href="#">Policy Privacy</a>
                </p>
                <div class="signup_link">
                    Already have an account? <a href="../login/Login.jsp">Login here</a>
                </div>
            </form>
        </div>
        <script>
            const currentDate = new Date();
            datePickerId.max = new Date(currentDate.getFullYear() - 10, currentDate.getMonth(), currentDate.getDate()).toLocaleDateString('sv-SE');
        </script>
    </body>
</html>
