 // Check for saved login credentials when the page loads
    $(document).ready(function() {
        if (localStorage.getItem('username') && localStorage.getItem('password')) {
            $('#remember-me').prop('checked', true); // Check the "Remember Me" checkbox
            $('input[name=username]').val(localStorage.getItem('username')); // Fill in the username field with saved value
            $('input[name=password]').val(localStorage.getItem('password')); // Fill in the password field with saved value
        }
    });

    // Save login credentials to local storage when "Remember Me" is checked
    $('#remember-me').change(function() {
        if ($(this).is(':checked')) {
            localStorage.setItem('username', $('input[name=username]').val());
            localStorage.setItem('password', $('input[name=password]').val());
        } else {
            localStorage.removeItem('username');
            localStorage.removeItem('password');
        }
    });