/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dao.CartDAO;
import dao.UserDAO;
import model.User;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author msii
 */
public class LoginController extends HttpServlet {

    protected void doGET(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("/view/login/Login.jsp").forward(request, response);
    }
    //check login ct

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter pr = response.getWriter();
            String xName = request.getParameter("username");
            String xPass = request.getParameter("password");
            User x;
            UserDAO t = new UserDAO();
            CartDAO cartDAO = new CartDAO();
            x = t.getUser(xName, xPass);
          
            if (x == null) {
                request.setAttribute("mess", "<p>Sorry, username and/or password are/is invalid!</p>");
                request.getRequestDispatcher("/view/login/Login.jsp").forward(request, response);
            } else if (!x.isStatus()) {
                request.setAttribute("mess", "<strong><p>Your account has been disabled ! Contact us if this is a mistake</p><br>"
                        + "Email: flearning2077@gmail.com</strong>");
                request.getRequestDispatcher("/view/login/Login.jsp").forward(request, response);
            } else {
                try {
                    User id = t.getID(xName);
                    if (!cartDAO.checkExistCart(id.getUid()) && id.getRole() == 1) {
                        cartDAO.createCartForUser(id.getUid());
                    }
                    request.getSession().setAttribute("user", x);
                    request.getSession().setAttribute("userprofile", id.getUid());
                    request.getSession().setAttribute("userid", id.getUid());
                    String fullpath = request.getContextPath();
                    response.sendRedirect(fullpath + "/home");
                } catch (SQLException ex) {
                    Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (SQLException e) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
