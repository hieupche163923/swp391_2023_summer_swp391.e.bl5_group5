/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dao.UserDAO;
import java.io.IOException;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;
import static util.Validate.toSHA1;

/**
 *
 * @author msii
 */
public class NewPasswordController extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            HttpSession session = request.getSession();
            String email = (String) session.getAttribute("email");
            String newPassword = request.getParameter("password");
            String confPassword = request.getParameter("confPassword");
            RequestDispatcher dispatcher = null;
            User user = new User();
            UserDAO userdao = new UserDAO();

            if (newPassword != null && confPassword != null && newPassword.equals(confPassword)) {
                user = userdao.getUserbyemail(email);
                //Ma hoa pass
                String MHpass = toSHA1(newPassword);
                userdao.changePassword(MHpass, user.getUid());

                if (userdao.getUserbyemail(email).getPassword().equals(MHpass)) {
                    request.setAttribute("status", "resetSuccess");
                    dispatcher = request.getRequestDispatcher("view/login/Login.jsp");
                } else {
                    request.setAttribute("status", "resetFailed");
                    dispatcher = request.getRequestDispatcher("/view/user/newPassword.jsp");
                }
                dispatcher.forward(request, response);

            } else {
                request.setAttribute("status", "New password and confirm receive password are not the same");
                request.getRequestDispatcher("/view/user/newPassword.jsp").forward(request, response);
            }
        } catch (SQLException e){
            Logger.getLogger(ChangePasswordController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

}
