/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dao.UserDAO;
import model.User;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static util.Validate.isValidEmail;
import static util.Validate.isValidPhoneNumber;

/**
 *
 * @author msii
 */
public class ProfileController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            UserDAO d = new UserDAO();
            int userprofile = (int) request.getSession().getAttribute("userprofile");
            int id = Integer.parseInt(request.getParameter("id"));
            if (id != userprofile) {
                response.getWriter().println("Access Denied!!!");
            } else {
                User u = d.getUserById(userprofile);
                request.setAttribute("user1", u);
                HttpSession session = request.getSession();
                session.setAttribute("uid", userprofile);
                request.getRequestDispatcher("/view/user/profile.jsp").forward(request, response);
            }
        } catch (SQLException e){
            Logger.getLogger(ProfileController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            UserDAO d = new UserDAO();
            HttpSession session = request.getSession();
            int id = (int) session.getAttribute("userprofile");
            String username = request.getParameter("username").trim();
            String email = request.getParameter("email").trim();
            String name = request.getParameter("name").trim();
            String gender = request.getParameter("gender").trim();

            String dob = request.getParameter("dob");
            Date xxDob = Date.valueOf(dob);

            String address = request.getParameter("address").trim();
            String phone = request.getParameter("phone").trim();

            User userx = d.getUserbyusername(username);

            if (!isValidEmail(email)) {
                request.setAttribute("mess3", "<p>The Email is wrong</p>");

            }
//        if(d.getUserbyemail(email)!=null && userx.email.compareTo(email)!=0){
            if (d.getUserbyemail(email).getUid() != id) {
                request.setAttribute("mess0", "<p>The User " + email + " already exists!</p>");
            }

            if (!isValidPhoneNumber(phone)) {
                request.setAttribute("mess2", "<p>The Phone is wrong</p>");

            }
            if (!isValidPhoneNumber(phone) || !isValidEmail(email) || d.getUserbyemail(email).getUid() != id) {
                User u = new User(0, username, null, name, address, email, phone, xxDob, gender, 0);
                request.setAttribute("user1", u);
                request.getRequestDispatcher("/view/user/profile.jsp").forward(request, response);
                return;
            }
            d.editUser(id, email, name, gender, xxDob, address, phone);
            response.sendRedirect("profile?id=" + id);
        } catch (SQLException e) {
            Logger.getLogger(ProfileController.class.getName()).log(Level.SEVERE, null, e);
        }

    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */

}
