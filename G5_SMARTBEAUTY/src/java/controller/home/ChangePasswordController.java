/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dao.UserDAO;
import model.User;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import static util.Validate.toSHA1;

/**
 *
 * @author msii
 */
public class ChangePasswordController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            UserDAO d = new UserDAO();
            int userprofile = (int) request.getSession().getAttribute("userprofile");
            int id = Integer.parseInt(request.getParameter("id"));
            if (id != userprofile) {
                response.getWriter().println("Access Denied!!!");
            } else {
                User u = d.getUserById(userprofile);
                request.setAttribute("user1", u);
                HttpSession session = request.getSession();
                session.setAttribute("uid", userprofile);
                request.getRequestDispatcher("/view/user/changepassword.jsp").forward(request, response);
            }
        } catch (SQLException e) {
            Logger.getLogger(ChangePasswordController.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            UserDAO d = new UserDAO();
            HttpSession session = request.getSession();
            int id = (int) session.getAttribute("userprofile");
            User u = d.getUserById(id);
            String password = u.getPassword();

            String old_pass = request.getParameter("old_pass");
            String old_passMH = toSHA1(old_pass);
            String new_password = request.getParameter("pass");
            String re_password = request.getParameter("repass");
            if (!password.equals(old_passMH)) {
                request.setAttribute("error1", "Password and OLD password must be the same!");
                request.getRequestDispatcher("/view/user/changepassword.jsp").forward(request, response);

            } else if (!new_password.equals(re_password)) {
                request.setAttribute("erro2", "Password and Re-enter password must be the same!");
                request.getRequestDispatcher("/view/user/changepassword.jsp").forward(request, response);

            } else {
                String NewPass = toSHA1(new_password);
                d.changePassword(NewPass, id);
                request.setAttribute("success", "change password successful!");
//                response.sendRedirect("profile?id=" + id);
                request.getRequestDispatcher("/view/user/changepassword.jsp").forward(request, response);
            }
        } catch (SQLException e) {
            Logger.getLogger(ChangePasswordController.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
