/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.home;

import dao.UserDAO;
import model.User;
import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import static util.Validate.*;

import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author msii
 */
public class RegisterController extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter pr = response.getWriter();

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            PrintWriter pr = response.getWriter();
//        Khai bao
            String xUsername, xPassword, xFullname, xAddress, xGender, xEmail, xPhone, xDob;

            Date xxDob = null;

            xUsername = request.getParameter("username").trim();

            UserDAO u = new UserDAO();
            User x = u.getUserbyusername(xUsername);

            xPassword = request.getParameter("password").trim();
            xFullname = request.getParameter("fullname").trim();
            xAddress = request.getParameter("address").trim();
            xPhone = request.getParameter("phone").trim();
            xEmail = request.getParameter("email").trim();
            xDob = request.getParameter("birthday");
            xxDob = Date.valueOf(xDob);
            xGender = request.getParameter("gender").trim();

            //Ma hoa pass
            String MHpass = toSHA1(xPassword);

            //Check for duplicate Username
            if (x != null) {
                request.setAttribute("mess1", "<p>The User " + xUsername + " already exists!</p>");

            }
            if (u.getUserbyemail(xEmail) != null) {
                request.setAttribute("mess0", "<p>The User " + xEmail + " already exists!</p>");
            }
            if (!isValidEmail(xEmail)) {
                request.setAttribute("mess3", "<p>The Email is wrong</p>");
            }

            if (!isValidPhoneNumber(xPhone)) {
                request.setAttribute("mess2", "<p>The Phone is wrong</p>");

            }
            if (x != null || !isValidPhoneNumber(xPhone) || !isValidEmail(xEmail) || u.getUserbyemail(xEmail) != null) {
                User user = new User(0, xUsername, xPassword, xFullname, xAddress, xEmail, xPhone, xxDob, xGender, 0);
                request.setAttribute("userfake", user);
                request.getRequestDispatcher("/view/login/Register.jsp").forward(request, response);
                return;
            }

            u.CustomerRegister(xUsername, MHpass, xFullname, xAddress, xEmail, xxDob, xPhone, xGender, "1", "1");

            request.getRequestDispatcher("/view/login/Login.jsp").include(request, response);
        } catch (SQLException e) {
            Logger.getLogger(ProfileController.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
