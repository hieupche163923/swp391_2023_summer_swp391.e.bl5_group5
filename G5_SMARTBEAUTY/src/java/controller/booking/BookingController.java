/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.booking;

import dao.ServiceDAO;
import dao.BookingDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Service;
import model.Booking;

/**
 *
 * @author doanv
 */
public class BookingController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Booking</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Booking at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ArrayList<Service> bookingdetail = new ArrayList();
        ServiceDAO dao = new ServiceDAO();
        if ((ArrayList<Integer>) request.getSession().getAttribute("booking") != null) {
            for (int i : (ArrayList<Integer>) request.getSession().getAttribute("booking")) {
                bookingdetail.add(dao.GetByID(i));
            }
        }
        request.setAttribute("bookingdetail", bookingdetail);
        request.getRequestDispatcher("view/booking/booking.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String rawdate = request.getParameter("date");
        String rawstart = request.getParameter("start");
        Booking b = new Booking();
        b.setId(0);
        b.setCustomerid((int) request.getSession().getAttribute("userid"));
        b.setStart(rawstart);
        b.setDate(Date.valueOf(rawdate));
        ArrayList<Integer> listservice = (ArrayList<Integer>) request.getSession().getAttribute("booking");
        int sum = 0;
        for (int i : listservice) {
            ServiceDAO dao = new ServiceDAO();
            sum += dao.GetByID(i).getTime() + 10;
        }
        b.setTime(sum);
        BookingDAO bookdao = new BookingDAO();
        bookdao.addNewBooking(b);
        int id = bookdao.getLatestBooking();
        for (int i : listservice) {
            try {
                bookdao.addBookingDetail(id, i);
            } catch (SQLException ex) {
                Logger.getLogger(Booking.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        response.sendRedirect("/G5_SMARTBEAUTY/home");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
