/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.service;

import util.ImageUtil;
import dao.ServiceDAO;
import dao.CategoryDAO;
import model.Category;
import model.Service;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author doanv
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 50, // 50MB
        maxRequestSize = 1024 * 1024 * 50)
public class ServiceAdd extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            CategoryDAO dao = new CategoryDAO();
            ArrayList<Category> list = dao.getAllCategories();
            request.setAttribute("categories", list);
            request.getRequestDispatcher("../../view/service/serviceadd.jsp").forward(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ServiceAdd.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Part part = request.getPart("img");
        ServiceDAO dao = new ServiceDAO();
        String rawname = request.getParameter("sname"),
                rawprice = request.getParameter("price"),
                rawtime = request.getParameter("time"),
                rawcate = request.getParameter("cate");

        int price = Integer.parseInt(rawprice),
                cate = Integer.parseInt(rawcate),
                time = Integer.parseInt(rawtime);
        Service s = new Service(0, rawname, price, time, cate, "");
        dao.AddService(s);
        if (part.getSize() != 0) {
            ImageUtil iu = new ImageUtil();
            Service sv = dao.GetLatestService();
            String fileName = iu.extractFileName(part, sv.getId());
            fileName = new File(fileName).getName();
            part.write(iu.getFolderUpload().getAbsolutePath() + File.separator + fileName);
            sv.setImage("img/service/" + sv.getId() + ".png");
            dao.ServiceUpdate(sv);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ServiceEditDetail.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        response.sendRedirect("../service");

    }

}
