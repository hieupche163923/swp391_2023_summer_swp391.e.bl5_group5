/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.service;

import util.ImageUtil;
import dao.ServiceDAO;
import dao.CategoryDAO;
import model.Service;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author doanv
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2, // 2MB
        maxFileSize = 1024 * 1024 * 50, // 50MB
        maxRequestSize = 1024 * 1024 * 50)
public class ServiceEditDetail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ServiceDetail</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ServiceDetail at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            ServiceDAO dao = new ServiceDAO();
            CategoryDAO cateDao = new CategoryDAO();
            String rawID = request.getParameter("sid");
            int id = Integer.parseInt(rawID);
            Service s = dao.GetByID(id);
            request.setAttribute("s", s);
            request.setAttribute("categories", cateDao.getAllCategories());
            request.getRequestDispatcher("../../view/service/serviceeditdetail.jsp").forward(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ServiceEditDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String rawid = request.getParameter("sid"),
                rawname = request.getParameter("sname"),
                rawprice = request.getParameter("price"),
                rawtime = request.getParameter("time"),
                rawcate = request.getParameter("cate");
        int id = Integer.parseInt(rawid.trim()),
                cate = Integer.parseInt(rawcate),
                time = Integer.parseInt(rawtime);
        Float price = Float.parseFloat(rawprice);


        //Load image
        Part part = request.getPart("img");
        if (part.getSize() != 0) {
            ImageUtil iu = new ImageUtil();
            String fileName = iu.extractFileName(part, id);
            fileName = new File(fileName).getName();
            part.write(iu.getFolderUpload().getAbsolutePath() + File.separator + fileName);
        }
        /////////////////////////////////////////////////////////
        Service s = new Service();
        s.setId(id);
        s.setName(rawname);
        s.setPrice(price);
        s.setTime(time);
        s.setCategory(cate);
        s.setImage("img/service/" + rawid.trim() + ".png");
        ServiceDAO dao = new ServiceDAO();
        dao.ServiceUpdate(s);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ServiceEditDetail.class.getName()).log(Level.SEVERE, null, ex);
        }
        response.sendRedirect("../service");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
