/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.service;

import controller.product.ProductController;
import dao.CategoryDAO;
import dao.ProductDAO;
import model.Service;
import dao.ServiceDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Category;
import model.Product;

/**
 *
 * @author doanv
 */
public class ServiceController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CategoryDAO categoryDAO = new CategoryDAO();
        ServiceDAO dao = new ServiceDAO();
        ArrayList<Category> listCategories = null;
        ArrayList<Service> listServices = null;
        String productName = request.getParameter("pname");
        String category = request.getParameter("category");
        String price = request.getParameter("price");

        try {
            listCategories = categoryDAO.getAllCategories();

            if (productName!=null || category != null || price != null) {
                listServices = dao.filterServices(productName, category, price);
            } else {
                listServices = dao.ServiceList();
            }
            if (listServices != null && listCategories != null) {
                request.setAttribute("servicelist", listServices);
                request.setAttribute("categories", listCategories);
                request.setAttribute("searchName", productName);
                request.getRequestDispatcher("../view/service/servicelist.jsp").forward(request, response);
            }
        } catch (ServletException | IOException | SQLException ex) {
            Logger.getLogger(ProductController.class.getName()).log(Level.SEVERE, null, ex);
        }
//        ServiceDAO dao = new ServiceDAO();
//        ArrayList<Service> list = dao.ServiceList();
//        request.setAttribute("servicelist", list);
//        request.getRequestDispatcher("./view/service/servicelist.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

}
