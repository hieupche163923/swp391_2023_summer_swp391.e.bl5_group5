/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import dao.ProductDAO;
import dao.ServiceDAO;
import dao.UserDAO;
import jakarta.servlet.ServletContext;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LENOVO
 */
@WebServlet(name = "AdminController", urlPatterns = {"/admin"})
public class AdminController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            UserDAO udao = new UserDAO();
            ServletContext ctx = getServletContext();
            int activeCust = (Integer) ctx.getAttribute("currentCustomers");
            int activeEmp = (Integer) ctx.getAttribute("currentEmployees");
            int totalCust = udao.getUserByRoleID(1).size();
            int totalEmp = udao.getUserByRoleID(2).size();
            request.setAttribute("activeCus", activeCust);
            request.setAttribute("activeEmp", activeEmp);
            request.setAttribute("totalCus", totalCust);
            request.setAttribute("totalEmp", totalEmp);
            ProductDAO pdao = new ProductDAO();
            int totalProduct = pdao.getAllProducts().size();
            request.setAttribute("productChart", pdao.getMonthlyEarning());
            ServiceDAO sdao = new ServiceDAO();
            int totalService = sdao.ServiceList().size();
            request.setAttribute("totalProducts", totalProduct);
            request.setAttribute("totalServices", totalService);
            
            request.getRequestDispatcher("/view/admin/Dashboard.jsp").forward(request, response);
        } catch (SQLException e){
            Logger.getLogger(AdminController.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }
}
