/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.admin;

import dao.UserDAO;
import model.User;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static util.Validate.isValidEmail;
import static util.Validate.isValidPhoneNumber;
import static util.Validate.toSHA1;

/**
 *
 * @author LENOVO
 */
@WebServlet(name = "AdminStaffController", urlPatterns = {"/admin/staff"})
public class AdminStaffController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            UserDAO dao = new UserDAO();
            if (request.getParameter("id") != null && request.getParameter("status") != null) {
                int uid = Integer.parseInt(request.getParameter("id"));
                boolean status = Boolean.parseBoolean(request.getParameter("status"));
                dao.toggleStatus(status, uid);
            }
            String filter = "";
            if (request.getParameter("q") != null) {
                filter = (String) request.getParameter("q");
            }
            request.setAttribute("searchValue", filter);
            
            int offset = 0, currentPage = 1;
            if(request.getParameter("page") != null) currentPage = Integer.parseInt(request.getParameter("page"));
            int itemPerPage = 15;
            offset = itemPerPage * (currentPage - 1);
            List<User> list = dao.getFilteredUsers(2, filter, offset, itemPerPage);
            int pages = (int) Math.ceil((double)dao.countUserByRole("2")/itemPerPage);
            request.setAttribute("empList", list);
            request.setAttribute("totalPage", pages);
            request.setAttribute("orderNum", offset);
            
            request.getRequestDispatcher("/view/admin/Staff.jsp").forward(request, response);
        } catch (SQLException e) {
            Logger.getLogger(AdminStaffController.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String idStr, xUsername, xPassword, xFullname, xAddress, xGender, xStatus, xEmail, xPhone, xDob, error = null, success = null;

            Date xxDob = null;

            xUsername = request.getParameter("username").trim();

            UserDAO u = new UserDAO();
            request.setAttribute("empList", u.getUserByRoleID(2));
            User x = u.getUserbyusername(xUsername);

            idStr = request.getParameter("id").trim();
            xPassword = request.getParameter("password").trim();
            xFullname = request.getParameter("fullname").trim();
            xAddress = request.getParameter("address").trim();
            xPhone = request.getParameter("phone").trim();
            xEmail = request.getParameter("email").trim();
            xDob = request.getParameter("birthday");
            xxDob = Date.valueOf(xDob);
            xGender = request.getParameter("gender").trim();
            if(request.getParameter("status") != null) xStatus = request.getParameter("status").trim();
            
            //Check for duplicate Username
            if (idStr == null || idStr.isEmpty()) {
                if (x != null) {
                    error = "The Account: " + xUsername + " already existed!";
                }
                if (u.getUserbyemail(xEmail) != null) {
                    error = "Email: " + xEmail + " existed!";
                }
            }

            if (!isValidEmail(xEmail)) {
                error = "Invalid email: " + xEmail + " !";
            }
            if (!isValidPhoneNumber(xPhone)) {
                error = "Invalid phone: " + xPhone + " !";
            }
            if (error != null) {
//                request.setAttribute("errorStaffCreate", error);
//                request.getRequestDispatcher("/view/admin/Staff.jsp").forward(request, response);
                response.sendRedirect("/G5_SMARTBEAUTY/admin/staff?message="+error);
                return;
            }
            if (idStr == null || idStr.isEmpty()) {
                //Ma hoa pass
                String MHpass = toSHA1(xPassword);
                u.CustomerRegister(xUsername, MHpass, xFullname, xAddress, xEmail, xxDob, xPhone, xGender, "2", "true");
                success = "User " + xUsername + " created successfully!";
            } else {
                int id = Integer.parseInt(idStr);
                u.editUser(id, xEmail, xFullname, xGender, xxDob, xAddress, xPhone);
                success = "User " + xUsername + " updated successfully!";
            }

//            request.setAttribute("Success", success);
//            request.setAttribute("empList", u.getUserByRoleID(2));
            response.sendRedirect("/G5_SMARTBEAUTY/admin/staff?message="+success);
        } catch (SQLException e) {
            Logger.getLogger(AdminStaffController.class.getName()).log(Level.SEVERE, null, e);
        }
    }

}
