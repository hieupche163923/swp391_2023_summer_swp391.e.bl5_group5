/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.cart;

import dao.CartDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import static java.lang.Thread.sleep;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.CartDetail;

/**
 *
 * @author binhp
 */
public class CartController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        String rawId = request.getParameter("id");
        int id = -1;
        int cartId = -1;
        float total = 0;
        ArrayList<CartDetail> cartDetails = new ArrayList<>();
        Object rawUserId = request.getSession().getAttribute("userid");
        CartDAO cartDAO = new CartDAO();

        if (rawId != null) {
            id = Integer.parseInt(rawId);
        }

        if (rawUserId != null) {
            int userId = (int) rawUserId;
            if (id != userId) {
                id = userId;
            }
        }

        try {
            //get cart id
            cartId = cartDAO.getCartIdByUserId(id);
            if (cartId != -1) {
                //get list products in cart
                cartDetails = cartDAO.getCartDetails(cartId);
                for (CartDetail detail : cartDetails) {
                    //calculate total price
                    total += detail.getAmount() * detail.getPrice();
                    
                    //set single price
                    detail.setSinglePrice(detail.getAmount() * detail.getPrice());
                }
            }
            request.setAttribute("cartid", cartId);
            request.setAttribute("cartdetails", cartDetails);
            request.setAttribute("count", cartDetails.size());
            request.setAttribute("total", total);
            request.getSession().setAttribute("cartcount", cartDetails.size());
            request.getSession().removeAttribute("recivername");
            request.getSession().removeAttribute("cartphone");
            request.getSession().removeAttribute("cartaddress");
            sleep(1000);
            request.getRequestDispatcher("view/cart/cart.jsp").forward(request, response);
        } catch (SQLException | InterruptedException ex) {
            Logger.getLogger(CartController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
