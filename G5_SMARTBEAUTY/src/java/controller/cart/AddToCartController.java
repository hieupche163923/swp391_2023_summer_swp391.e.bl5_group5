/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.cart;

import dao.CartDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import static java.lang.Thread.sleep;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author binhp
 */
public class AddToCartController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            //processRequest(request, response);
            String rawProductId = request.getParameter("productid");
            String rawAmount = request.getParameter("amount");
            Object rawUserId = request.getSession().getAttribute("userid");
            int userId = -1;
            int cartId = -1;
            int productId = -1;
            int amount = -1;
            int addStatus = 0;

            CartDAO cartDAO = new CartDAO();

            if (rawProductId != null) {
                productId = Integer.parseInt(rawProductId);
            }
            if (rawAmount != null) {
                amount = Integer.parseInt(rawAmount);
            }
            if (rawUserId != null) {
                userId = (int) rawUserId;
                cartId = cartDAO.getCartIdByUserId(userId);
            }

            //get amount
            int tempAmount = cartDAO.checkProductExist(cartId, productId);
            if (tempAmount == -1) {
                // add to cart if product not in cart
                cartDAO.addToCart(cartId, productId, amount);
                addStatus = 1;
            } else {
                // add to cart if product in cart
                amount += tempAmount;
                cartDAO.updateProductAmount(cartId, productId, amount);
                addStatus = 1;
            }
            
            request.getSession().setAttribute("addstatus", addStatus);
            sleep(1000);
            response.sendRedirect("add/message?pid=" + productId);
        } catch (SQLException | InterruptedException ex) {
            Logger.getLogger(AddToCartController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
