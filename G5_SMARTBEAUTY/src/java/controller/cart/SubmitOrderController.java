/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.cart;

import dao.CartDAO;
import dao.OrderDAO;
import dao.ProductDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import static java.lang.Thread.sleep;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;

/**
 *
 * @author binhp
 */
public class SubmitOrderController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int userId = -1;
            int cartId = -1;
            String reciverName = request.getParameter("recivername").trim();
            String phone = request.getParameter("phone").trim();
            String address = request.getParameter("address").trim();
            String mess = "";
            Object rawUserId = request.getSession().getAttribute("userid");
            OrderDAO orderDAO = new OrderDAO();
            CartDAO cartDAO = new CartDAO();
            ProductDAO productDAO = new ProductDAO();
            
            //check user id
            if (rawUserId != null) {
                userId = (int) rawUserId;
                
                //get cart id
                cartId = cartDAO.getCartIdByUserId(userId);
            }

            //number of product in cart
            int cartCount = (int) request.getSession().getAttribute("cartcount");

            //if have any product in cart
            if (cartCount > 0) {
                //submit order
                orderDAO.submitOrder(userId, reciverName, address, phone);

                //get id of lastest order
                Order lastestOrder = orderDAO.getLastestOrder();

                //get list detail on screen
                for (int i = 1; i <= cartCount; i++) {
                    int productId = Integer.parseInt(request.getParameter("productid" + i));
                    int amount = Integer.parseInt(request.getParameter("amount" + i));
                    int quantity = Integer.parseInt(request.getParameter("productquantity" + i));

                    //add product in order
                    orderDAO.submitOrderDetails(lastestOrder.getOrderId(), productId, amount);
                    int tempQuantity = productDAO.getProductById(productId).getQuantity();
                    tempQuantity = tempQuantity - amount >= 0 ? tempQuantity - amount : 0;

                    //delete quantity after submit order
                    productDAO.updateQuantityByTd(productId, tempQuantity);
                }

                //delete cart after submit
                cartDAO.deleteCartDetails(cartId);
            }
            
            request.setAttribute("mess", mess);
            request.getSession().setAttribute("cartmessage", mess);
            request.getSession().setAttribute("recivername", reciverName);
            request.getSession().setAttribute("cartphone", phone);
            request.getSession().setAttribute("cartaddress", address);
            sleep(1000);
            response.sendRedirect("../cart/message?id=" + userId);
        } catch (SQLException | InterruptedException ex) {
            Logger.getLogger(SubmitOrderController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
