/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.material;

import dao.CategoryDAO;
import dao.MaterialDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Material;

/**
 *
 * @author khanh
 */
public class EditMaterial extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet editMaterial</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet editMaterial at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
// processRequest(request, response);

        try {
            MaterialDAO dao = new MaterialDAO();
            CategoryDAO cateDao = new CategoryDAO();
            String rawID = request.getParameter("mid");
            int id = Integer.parseInt(rawID);
            Material m = dao.getMaterialById(id);
            request.setAttribute("m", m);
            request.setAttribute("categories", cateDao.getAllCategories());
            request.getRequestDispatcher("/view/material/editMaterial.jsp").forward(request, response);
        } catch (Exception e) {
            response.sendRedirect("/error");
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        processRequest(request, response);
        try {
//            MaterialDAO dao = new MaterialDAO();
//            CategoryDAO cateDao = new CategoryDAO();
//            String rawID = request.getParameter("mid");
//            int id = Integer.parseInt(rawID);
//            Material m = new Material();
//            m.setMid(id);
//            m.setMname(request.getParameter("mname"));
//            m.setCategory(request.getParameter("category"));
//            m.setQuantity(request.getParameter("quantity"));
//            m.setDescription(request.getParameter("description"));
            String rawid = request.getParameter("mid"),
                    rawname = request.getParameter("mname"),
                    rawprice = request.getParameter("quantity"),
                    rawdesc = request.getParameter("description"),
                    rawcate = request.getParameter("category");
            int id = Integer.parseInt(rawid),
                    quantity = Integer.parseInt(rawprice),
                    category = Integer.parseInt(rawcate);
            Material m = new Material();
            m.setMid(id);
            m.setMname(rawname);
            m.setQuantity(quantity);
            m.setDescription(rawdesc);
            m.setCategory(category);
            MaterialDAO dao = new MaterialDAO();
            dao.updateMaterial(m);
            response.sendRedirect("http://localhost:9999/G5_SMARTBEAUTY/material");
        } catch (Exception e) {
            response.sendRedirect("/error");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
