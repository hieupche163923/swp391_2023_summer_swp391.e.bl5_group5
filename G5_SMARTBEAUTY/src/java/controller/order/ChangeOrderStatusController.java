/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.order;

import dao.OrderDAO;
import dao.ProductDAO;
import dao.StockDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.OrderDetail;

/**
 *
 * @author binhp
 */
public class ChangeOrderStatusController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            int id = Integer.parseInt(request.getParameter("id"));
            int orderStatus = Integer.parseInt(request.getParameter("status"));
            OrderDAO orderDAO = new OrderDAO();
            ProductDAO productDAO = new ProductDAO();
            StockDAO stockDAO = new StockDAO();

            if (orderStatus == 1) {
                ArrayList<OrderDetail> orderDetails = orderDAO.getOrderDetailById(String.valueOf(id));
                for (OrderDetail detail : orderDetails) {
                    stockDAO.exportProduct(detail.getProductId(), detail.getAmount());
                }

            } else if (orderStatus == 2) {
                
            } else if (orderStatus == 5) {
                ArrayList<OrderDetail> orderDetails = orderDAO.getOrderDetailById(String.valueOf(id));
                for (OrderDetail detail : orderDetails) {
                    int tempQuantity = productDAO.getProductById(detail.getProductId()).getQuantity();
                    tempQuantity += detail.getAmount();
                    productDAO.updateQuantityByTd(detail.getProductId(), tempQuantity);
                    stockDAO.importProduct(detail.getProductId(), detail.getAmount());
                }

            }

            orderStatus++;
            orderDAO.changeOrderStatus(id, orderStatus);

            request.setAttribute("mess", "Đổi thành công");
            request.getRequestDispatcher("../order").forward(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ChangeOrderStatusController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
