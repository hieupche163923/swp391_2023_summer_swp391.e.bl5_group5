/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.order;

import dao.OrderDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import static java.lang.Thread.sleep;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Order;

/**
 *
 * @author binhp
 */
public class OrderController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            //Get id on url
            String rawId = request.getParameter("id");
            String dateFrom = request.getParameter("datefrom");
            String dateTo = request.getParameter("dateto");
            String mess = "";
            int id = -1;
            ArrayList<Order> orders = new ArrayList<>();
            OrderDAO orderDAO = new OrderDAO();
            
            //get id on session
            Object sessionId = request.getSession().getAttribute("userid");
            
            //check id get on url
            if (rawId.equals("")) {
                request.getRequestDispatcher("view/order/order.jsp").forward(request, response);
                return;
            } else {
                id = Integer.parseInt(rawId);
            }

            //compare id
            if (sessionId != null) {
                id = (int) sessionId;
            }

            //if user input date from and date to
            if (dateFrom != null || dateTo != null) {
                if (Date.valueOf(dateFrom).compareTo(Date.valueOf(dateTo)) > 0) {
                    mess = "Ngày không hợp lệ";
                } else {
                    //list orders with filter
                    orders = orderDAO.filterOrdersById(id, dateFrom, dateTo);
                }
            } else {
                //list filter without filter
                orders = orderDAO.getOrdersById(id);
            }

            request.setAttribute("list", orders);
            request.setAttribute("count", orders.size());
            request.setAttribute("dateto", dateTo);
            request.setAttribute("datefrom", dateFrom);
            request.setAttribute("mess", mess);
            sleep(1000);
            request.getRequestDispatcher("view/order/order.jsp").forward(request, response);

        } catch (SQLException | InterruptedException ex) {
            Logger.getLogger(OrderController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
