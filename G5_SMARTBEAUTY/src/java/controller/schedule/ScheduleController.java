/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.schedule;

import dao.ScheduleDAO;
import dao.UserDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Schedule;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author msii
 */
public class ScheduleController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        response.setContentType("text/html;charset=UTF-8");

        String sdate = request.getParameter("selectdate1");
        Date dates = null;
        HttpSession session = request.getSession();
        LocalDate currentDay = LocalDate.now();
        
        try {
            if (sdate == null || sdate.isEmpty()) {

                dates = Date.valueOf(currentDay);

                session.setAttribute("datess", dates);
            } else {
                dates = Date.valueOf(sdate);
                session.setAttribute("datess", dates);
            }
            Date datecheck = Date.valueOf(currentDay);
            session.setAttribute("datecheck", datecheck);
            
            // Các xử lý khác khi sdate không rỗng
            ScheduleDAO scheduledao = new ScheduleDAO();
            UserDAO userdao = new UserDAO();
            List<User> employ = userdao.getUserByRoleID(2);
            List<User> custum = userdao.getUserByRoleID(1);
            ArrayList<Schedule> schedule = scheduledao.getschedule(dates);
            if (schedule == null) {
                response.getWriter().print("schedule is null");
            }
            request.setAttribute("employ", employ);
            request.setAttribute("custum", custum);
            request.setAttribute("schedule", schedule);
            request.getRequestDispatcher("/view/schedule/Schedule.jsp").forward(request, response);
        } catch (IllegalArgumentException e) {
            response.getWriter().print("Invalid date format");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(ScheduleController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
//            processRequest(request, response);
            HttpSession session = request.getSession();
            Date sdate = (Date) session.getAttribute("datess");
            int shid = Integer.parseInt(request.getParameter("shid"));
            int employ = Integer.parseInt(request.getParameter("employ"));
            Boolean status = Boolean.parseBoolean(request.getParameter("status"));
            ScheduleDAO dao = new ScheduleDAO();

            dao.updateschedule(shid, employ, status);
            response.sendRedirect("/G5_SMARTBEAUTY/admin/schedule?selectdate1=" + sdate);
        } catch (SQLException ex) {
            Logger.getLogger(ScheduleController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
