/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author binhp
 */
public class Order {

    private int orderId;
    private int customerId;
    private String customerName;
    private Date orderDate;
    private String revicerName;
    private String phone;
    private String address;
    private int status;
    private String statusMess;

    public Order() {
    }

    public Order(int orderId, int customerId, String customerName, Date orderDate, String reciverName, String phone, String address, int status) {
        this.orderId = orderId;
        this.customerId = customerId;
        this.customerName = customerName;
        this.orderDate = orderDate;
        this.revicerName = reciverName;
        this.phone = phone;
        this.address = address;
        this.status = status;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getRevicerName() {
        return revicerName;
    }

    public void setRevicerName(String revicerName) {
        this.revicerName = revicerName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusMess() {
        return statusMess;
    }

    public void setStatusMess(String statusMess) {
        this.statusMess = statusMess;
    }
}
