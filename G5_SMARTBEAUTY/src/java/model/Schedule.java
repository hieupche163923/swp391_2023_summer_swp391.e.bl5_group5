/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.sql.Time;

/**
 *
 * @author msii
 */
public class Schedule {

    private int shid;
    private String customer;
    private int employeeid;
    private String employee;
    private int bid;
    private boolean status;
    private Time slot;
    private Date bookdate;
    private int bed;
    private String service;

    public Schedule() {
    }

    public Schedule(int shid, String customer, int employeeid, String employee, int bid, boolean status, Time slot, Date bookdate, int bed, String service) {
        this.shid = shid;
        this.customer = customer;
        this.employeeid = employeeid;
        this.employee = employee;
        this.bid = bid;
        this.status = status;
        this.slot = slot;
        this.bookdate = bookdate;
        this.bed = bed;
        this.service = service;
    }

    public int getShid() {
        return shid;
    }

    public void setShid(int shid) {
        this.shid = shid;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public int getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(int employeeid) {
        this.employeeid = employeeid;
    }

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Time getSlot() {
        return slot;
    }

    public void setSlot(Time slot) {
        this.slot = slot;
    }

    public Date getBookdate() {
        return bookdate;
    }

    public void setBookdate(Date bookdate) {
        this.bookdate = bookdate;
    }

    public int getBed() {
        return bed;
    }

    public void setBed(int bed) {
        this.bed = bed;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

   
}
