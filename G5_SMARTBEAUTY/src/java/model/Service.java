/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author LENOVO
 */
public class Service {
    private int id;
    private String name;
    private float price;
    private int time;
    private int category;
    private String image;

    public Service() {
    }

    public Service(int id, String name, float price, int time, int category, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.time = time;
        this.category = category;
        this.image = image;
    }

    public Service(int id, String name, float price, int time, int category) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.time = time;
        this.category = category;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

  
}
