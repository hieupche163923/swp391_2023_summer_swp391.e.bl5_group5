/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package util;

import jakarta.servlet.ServletContext;
import jakarta.servlet.http.HttpSessionAttributeListener;
import jakarta.servlet.http.HttpSessionBindingEvent;
import jakarta.servlet.http.HttpSessionEvent;
import model.User;

/**
 *
 * @author LENOVO
 */
public class SessionListener implements HttpSessionAttributeListener {
    ServletContext ctx = null;
    static int currentCustomer = 0, currentEmployee = 0;

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        String attributeName = event.getName();
        User sessionUser = (User) event.getValue();
        ctx = event.getSession().getServletContext();
        ctx.setAttribute("currentCustomers", currentCustomer);
        ctx.setAttribute("currentEmployees", currentEmployee);
        if (attributeName.equals("user")) {
            if (sessionUser.getRole() == 1) {
                currentCustomer++;
                ctx.setAttribute("currentCustomers", currentCustomer);
            }
            if (sessionUser.getRole() == 2) {
                currentEmployee++;
                ctx.setAttribute("currentEmployees", currentEmployee);
            }
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {
        String attributeName = event.getName();
        User sessionUser = (User) event.getValue();
        ctx = event.getSession().getServletContext();
        ctx.setAttribute("currentCustomers", currentCustomer);
        ctx.setAttribute("currentEmployees", currentEmployee);
        if (attributeName.equals("user")) {
            if (sessionUser.getRole() == 1) {
                currentCustomer--;
                ctx.setAttribute("currentCustomers", currentCustomer);
            }
            if (sessionUser.getRole() == 2) {
                currentEmployee--;
                ctx.setAttribute("currentEmployees", currentEmployee);
            }
        }
    }
}
