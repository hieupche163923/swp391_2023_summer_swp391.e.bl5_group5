/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import model.DAO;
import model.Product;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author LENOVO
 */
public class ProductDAO extends DAO {

    public void finalize() {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Product> topProduct() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql;
        ArrayList<Product> list = new ArrayList<>();
        sql = "SELECT TOP 8\n"
                + "    p.pid,\n"
                + "    p.pname,\n"
                + "    p.price,\n"
                + "    p.quantity,\n"
                + "    p.importdate,\n"
                + "    p.category,\n"
                + "    p.image,\n"
                + "    p.supplier,\n"
                + "    COUNT(od.productid) AS OrderCount\n"
                + "FROM [dbo].[Product] p\n"
                + "LEFT JOIN [dbo].[Order_Detail] od ON p.pid = od.productid\n"
                + "GROUP BY p.pid, p.pname, p.price, p.quantity, p.importdate, p.category, p.image, p.supplier\n"
                + "ORDER BY OrderCount DESC;";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                float price = rs.getFloat(3);
                int quantity = rs.getInt(4);
                Date importdate = rs.getDate(5);
                String category = rs.getString(6);
                String image = rs.getString(7);
                String supplier = rs.getString(8);
                list.add(new Product(id, name, price, quantity, importdate, category, image, supplier));
            }
        } catch (SQLException e) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public ArrayList<Product> getMonthlyEarning() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql;
        ArrayList<Product> chart = new ArrayList<>();
        sql = "SELECT\n"
                + "    LEFT(DATENAME(MONTH, o.date), 3) AS Month,\n"
                + "    SUM(p.price * od.amount) AS TotalEarnings\n"
                + "FROM\n"
                + "    [dbo].[Order] o\n"
                + "JOIN\n"
                + "    [dbo].[Order_Detail] od ON o.orderid = od.oderid\n"
                + "JOIN\n"
                + "    [dbo].[Product] p ON od.productid = p.pid\n"
                + "WHERE\n"
                + "    YEAR(o.date) = YEAR(GETDATE())\n"
                + "GROUP BY\n"
                + "    LEFT(DATENAME(MONTH, o.date), 3),\n"
                + "    MONTH(o.date)\n"
                + "ORDER BY\n"
                + "    MONTH(o.date)";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                String month = rs.getString(1);
                float earning = rs.getFloat(2);
                chart.add(new Product(month, earning));
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return chart;
    }

    public ArrayList<Product> getAllProducts() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql;
        ArrayList<Product> listProducts = new ArrayList<>();
        sql = "SELECT p.pid, p.pname, p.price, p.quantity, p.importdate, c.cname, p.image, p.supplier, p.[status]\n"
                + "FROM [Product] p\n"
                + "INNER JOIN [Category] c\n"
                + "ON p.category = c.cid\n"
                + "ORDER BY p.[status] DESC";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setId(rs.getInt("pid"));
                product.setName(rs.getString("pname"));
                product.setPrice(rs.getFloat("price"));
                product.setQuantity(rs.getInt("quantity"));
                product.setImportdate(rs.getDate("importdate"));
                product.setCategory(rs.getString("cname"));
                product.setImage(rs.getString("image"));
                product.setSupplier(rs.getString("supplier"));
                product.setStatus(rs.getBoolean("status"));
                listProducts.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return listProducts;
    }

    public ArrayList<Product> getAllEnabledProducts() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql;
        ArrayList<Product> listProducts = new ArrayList<>();
        sql = "SELECT p.pid, p.pname, p.price, p.quantity, p.importdate, c.cname, p.image, p.supplier, p.[status]\n"
                + "FROM [Product] p\n"
                + "INNER JOIN [Category] c\n"
                + "ON p.category = c.cid\n"
                + "WHERE p.[status] = 1";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setId(rs.getInt("pid"));
                product.setName(rs.getString("pname"));
                product.setPrice(rs.getFloat("price"));
                product.setQuantity(rs.getInt("quantity"));
                product.setImportdate(rs.getDate("importdate"));
                product.setCategory(rs.getString("cname"));
                product.setImage(rs.getString("image"));
                product.setSupplier(rs.getString("supplier"));
                product.setStatus(rs.getBoolean("status"));
                listProducts.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return listProducts;
    }

    public void addNewProduct(String pname, String price, String quantity, String category, String supplier, String description) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql;
        sql = "INSERT INTO [Product]\n"
                + "           ([pname]\n"
                + "           ,[price]\n"
                + "           ,[quantity]\n"
                + "           ,[importdate]\n"
                + "           ,[category]\n"
                + "           ,[image]\n"
                + "           ,[supplier]\n"
                + "           ,[status]\n"
                + "           ,[description])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,GETDATE()\n"
                + "           ,?\n"
                + "           ,'/G5_SMARTBEAUTY/img/logo-tet.jpg'\n"
                + "           ,?"
                + "           ,1"
                + "           ,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, pname);
            ps.setString(2, price);
            ps.setString(3, quantity);
            ps.setString(4, category);
            ps.setString(5, supplier);
            ps.setString(6, description);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public void deleteProduct(int id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql;
        sql = "DELETE FROM [Product]\n"
                + "      WHERE pid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public Product getProductById(int pid) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql;
        sql = "SELECT p.pid, p.pname, p.price, p.quantity, p.importdate, c.cname, p.image, p.supplier, p.[status], p.[description]\n"
                + "FROM [Product] p\n"
                + "INNER JOIN [Category] c\n"
                + "ON p.category = c.cid\n"
                + "WHERE p.pid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, pid);
            rs = ps.executeQuery();
            if (rs.next()) {
                Product product = new Product();
                product.setId(rs.getInt("pid"));
                product.setName(rs.getString("pname"));
                product.setPrice(rs.getFloat("price"));
                product.setQuantity(rs.getInt("quantity"));
                product.setImportdate(rs.getDate("importdate"));
                product.setCategory(rs.getString("cname"));
                product.setImage(rs.getString("image"));
                product.setSupplier(rs.getString("supplier"));
                product.setStatus(rs.getBoolean("status"));
                product.setDescription(rs.getString("description"));
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

        return null;
    }

    public void updateProductById(int pid, String pname, String price, String category, String supplier, String description) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql;
        sql = "UPDATE [Product]\n"
                + "   SET [pname] = ?\n"
                + "      ,[price] = ?\n"
                + "      ,[category] = ?\n"
                + "      ,[supplier] = ?\n"
                + "      ,[description] = ?\n"
                + " WHERE pid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, pname);
            ps.setString(2, price);
            ps.setString(3, category);
            ps.setString(4, supplier);
            ps.setString(5, description);
            ps.setInt(6, pid);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public ArrayList<Product> filterProducts(String pname, String category, String price) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql;

        ArrayList<Product> listProducts = new ArrayList<>();
        sql = "SELECT p.pid, p.pname, p.price, p.quantity, p.importdate, c.cname, p.image, p.supplier, p.[status]\n"
                + "FROM [Product] p\n"
                + "INNER JOIN [Category] c\n"
                + "ON p.category = c.cid\n"
                + "WHERE 1=1\n";

        if (pname != null) {
            sql += "AND p.pname LIKE '" + "%" + pname + "%'" + "\n";
        }
        if (!category.equals("")) {
            sql += "AND c.cid = " + category + "\n";
        }
        if (!price.equals("")) {
            if (price.equals("1")) {
                sql += "ORDER BY p.price ASC\n";
            } else if (price.equals("2")) {
                sql += "ORDER BY p.price DESC\n";
            }
        }
        sql += "ORDER BY p.[status] DESC";

        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setId(rs.getInt("pid"));
                product.setName(rs.getString("pname"));
                product.setPrice(rs.getFloat("price"));
                product.setQuantity(rs.getInt("quantity"));
                product.setImportdate(rs.getDate("importdate"));
                product.setCategory(rs.getString("cname"));
                product.setImage(rs.getString("image"));
                product.setSupplier(rs.getString("supplier"));
                product.setStatus(rs.getBoolean("status"));
                listProducts.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return listProducts;
    }

    public ArrayList<Product> filterEnabledProducts(String pname, String category, String price) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql;

        ArrayList<Product> listProducts = new ArrayList<>();
        sql = "SELECT p.pid, p.pname, p.price, p.quantity, p.importdate, c.cname, p.image, p.supplier, p.[status]\n"
                + "FROM [Product] p\n"
                + "INNER JOIN [Category] c\n"
                + "ON p.category = c.cid\n"
                + "WHERE p.[status] = 1\n";

        if (pname != null) {
            sql += "AND p.pname LIKE '" + "%" + pname + "%'" + "\n";
        }
        if (!category.equals("")) {
            sql += "AND c.cid = " + category + "\n";
        }
        if (!price.equals("")) {
            if (price.equals("1")) {
                sql += "ORDER BY p.price ASC\n";
            } else if (price.equals("2")) {
                sql += "ORDER BY p.price DESC\n";
            }
        }
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Product product = new Product();
                product.setId(rs.getInt("pid"));
                product.setName(rs.getString("pname"));
                product.setPrice(rs.getFloat("price"));
                product.setQuantity(rs.getInt("quantity"));
                product.setImportdate(rs.getDate("importdate"));
                product.setCategory(rs.getString("cname"));
                product.setImage(rs.getString("image"));
                product.setSupplier(rs.getString("supplier"));
                product.setStatus(rs.getBoolean("status"));
                listProducts.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return listProducts;
    }

    public void disableProduct(int id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "UPDATE [Product]\n"
                + "   SET [status] = 0\n"
                + " WHERE pid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public void enableProduct(int id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "UPDATE [Product]\n"
                + "   SET [status] = 1\n"
                + " WHERE pid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public void updateQuantityByTd(int productId, int quantity) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "UPDATE [Product]\n"
                + "   SET [quantity] = ?\n"
                + " WHERE pid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, quantity);
            ps.setInt(2, productId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
}
