/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import model.DAO;
import model.Service;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author doanv
 */
public class ServiceDAO extends DAO {
    
    public void finalize() {
        try {
            if (con != null) {
                con.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Service> topService() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        ArrayList<Service> list = new ArrayList<>();
        String sql = "SELECT TOP 6\n"
                + "    s.sid,\n"
                + "    s.sname,\n"
                + "    s.price,\n"
                + "    s.time,\n"
                + "    s.category,\n"
                + "    s.image,\n"
                + "    COUNT(b.sid) AS BookingCount\n"
                + "FROM [dbo].[Service] s\n"
                + "LEFT JOIN [dbo].[Booking_Detail] b ON s.sid = b.sid\n"
                + "GROUP BY s.sid, s.sname, s.price, s.time, s.category, s.image\n"
                + "ORDER BY BookingCount DESC;";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            int id, category;
            float price;
            String name, image;
            int time;
            while (rs.next()) {
                id = rs.getInt(1);
                name = rs.getString(2);
                price = rs.getInt(3);
                time = rs.getInt(4);
                category = rs.getInt(5);
                image = rs.getString(6);
                list.add(new Service(id, name, price, time, category, image));
            }
        } catch (Exception e) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            ps.close();
            rs.close();
        }
        return list;
    }
    
    public ArrayList<Service> ServiceList() {
        ArrayList<Service> list = new ArrayList<>();
        Service s;
        PreparedStatement ps;
        ResultSet rs;
        String sql = "select * from Service";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                s = new Service();
                s.setId(rs.getInt("sid"));
                s.setName(rs.getString("sname"));
                s.setCategory(rs.getInt("category"));
                s.setTime(rs.getInt("time"));
                s.setPrice(rs.getFloat("price"));
                s.setImage(rs.getString("image"));
                list.add(s);
            }
            return list;
        } catch (SQLException ex) {
            return null;
        }
    }
    
    public Service GetByID(int id) {
        try {
            Service s = null;
            PreparedStatement ps;
            ResultSet rs;
            String sql = "select * from Service \n"
                    + "where sid = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                s = new Service(rs.getInt(1),
                        rs.getString(2),
                        rs.getFloat(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getString("image")
                );
            }
            return s;
            
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
        
    }
    
    public void ServiceUpdate(Service s) {
        try {
            PreparedStatement ps;
            String sql = "update Service\n"
                    + "set sname =? ,price=?,time=?,category=?,image=?\n"
                    + "where sid=?";
            ps = con.prepareStatement(sql);
            ps.setString(1, s.getName());
            ps.setFloat(2, s.getPrice());
            ps.setInt(3, s.getTime());
            ps.setInt(4, s.getCategory());
            ps.setString(5, s.getImage());
            ps.setInt(6, s.getId());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void ServiceDelete(int id) {
        try {
            PreparedStatement ps;
            String sql = "DELETE FROM [dbo].[Service]\n"
                    + "      WHERE sid = ?";
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void AddService(Service s) {
        try {
            String sql = "INSERT INTO [dbo].[Service]\n"
                    + "           ([sname]\n"
                    + "           ,[price]\n"
                    + "           ,[time]\n"
                    + "           ,[category])\n"
                    //                    + "           ,[image])\n"
                    + "     VALUES\n"
                    + "           (?,?,?,?)";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, s.getName());
            ps.setFloat(2, s.getPrice());
            ps.setInt(3, s.getTime());
            ps.setInt(4, s.getCategory());
            ps.executeUpdate();
//            String url1="";
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Service GetLatestService() {
        try {
            String sql = "select top 1 * from Service \n"
                    + "order by sid desc";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            Service s = new Service();
            while (rs.next()) {
                s.setId(rs.getInt("sid"));
                s.setName(rs.getString("sname"));
                s.setPrice(rs.getFloat("price"));
                s.setTime(rs.getInt("time"));
                s.setCategory(rs.getInt("category"));
                s.setImage(rs.getString("image"));
            }
            return s;
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void AddNewImage(int id) {
        try {
            String sql = "update Service\n"
                    + "set image=?\n"
                    + "where sid=?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, "img/view/" + id + ".png");
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(ServiceDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public ArrayList<Service> filterServices(String pname, String category, String price) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql;
        
        ArrayList<Service> list = new ArrayList<>();
        sql = "SELECT s.sid, s.sname, s.price,s.time, s.category, c.cname, s.image\n"
                + "FROM [Service] s\n"
                + "INNER JOIN [Category] c\n"
                + "ON s.category = c.cid\n"
                + "WHERE 1=1\n";
        
        if (pname != null) {
            sql += "AND s.sname LIKE '" + "%" + pname + "%'" + "\n";
        }
        if (!category.equals("")) {
            sql += "AND c.cid = " + category + "\n";
        }
        if (!price.equals("")) {
            if (price.equals("1")) {
                sql += "ORDER BY s.price ASC\n";
            } else if (price.equals("2")) {
                sql += "ORDER BY s.price DESC\n";
            }
        }
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Service s = new Service();
                s.setId(rs.getInt("sid"));
                s.setName(rs.getString("sname"));
                s.setPrice(rs.getFloat("price"));
                s.setTime(rs.getInt("time"));
                s.setCategory(rs.getInt("category"));
                s.setImage(rs.getString("image"));
                list.add(s);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }
    
}
