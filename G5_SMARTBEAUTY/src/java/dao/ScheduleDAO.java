/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DAO;
import model.Schedule;

/**
 *
 * @author msii
 */
public class ScheduleDAO extends DAO {

    public void finalize() {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //lay list booking theo slot
    public ArrayList<Schedule> getschedule(Date bookdate) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql;
        ArrayList<Schedule> listBook = new ArrayList<>();
        sql = "SELECT c.employeid, b.customername, b.starttime, b.bookdate, c.[status], c.bed, b.bid, c.schid, s.sname\n"
                + "                FROM [Schedule] c join Booking b on c.bid=b.bid\n"
                + "                join Booking_Detail bd on b.bid= bd.bid\n"
                + "                join Service s on bd.sid = s.sid\n"
                + "                where b.bookdate =?;";
        try {
            ps = con.prepareStatement(sql);
            ps.setDate(1, bookdate);
            rs = ps.executeQuery();
            String cname, sname, ename;
            Time slot;
            Date bd;
            boolean status;
            int bed, enameid, bid, schid;
            UserDAO user = new UserDAO();

            while (rs.next()) {
                enameid = rs.getInt(1);
                cname = rs.getString(2);
                slot = rs.getTime(3);
                bd = rs.getDate(4);
                status = rs.getBoolean(5);
                bed = rs.getInt(6);
                bid = rs.getInt(7);
                schid = rs.getInt(8);
                sname = rs.getString(9);
                ename = user.getUserById(enameid).getDisplayName();
                listBook.add(new Schedule(schid, cname, enameid, ename, bid, status, slot, bookdate, bed, sname));
            }
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

        return listBook;
    }

    public void addBookingSchedule(String customer, String employee, String service, boolean status, Time slot, Date bookdate, int bed) {
        PreparedStatement ps;
        ResultSet rs;
        String sql;

        try {
            sql = "\"INSERT INTO [dbo].[Schedule]\\n\"\n"
                    + "                + \"           ([mname]\\n\"\n"
                    + "                + \"           ,[quantity]\\n\"\n"
                    + "                + \"           ,[category]\\n\"\n"
                    + "                + \"           ,[description])\\n\"\n"
                    + "                + \"     VALUES\\n\"\n"
                    + "                + \"           (?,?,?,?)\";";
        } catch (Exception e) {
        }
    }

    public void updateschedule(int shid, int employeeid, boolean status) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "UPDATE [Schedule]\n"
                + "   SET [employeid] = ? ,[status] = ?\n"
                + " WHERE [schid]=?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, employeeid);
            ps.setBoolean(2, status);
            ps.setInt(3, shid);

            ps.executeUpdate();
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

    }

//    public static void main(String[] args) throws SQLException {
//        ScheduleDAO s = new ScheduleDAO();
//        ArrayList<Schedule> listBook = new ArrayList<>();
//       
//         LocalDateTime currentDay = LocalDateTime.now();
//          Date dates = Date.valueOf("2023-08-25");
//
//        listBook = s.getschedule(dates);
//        for (Schedule schedule : listBook) {
//            System.out.println(currentDay);
//        }
//        System.out.println("null");
//    }
    
//      public static void main(String[] args) {
//        LocalTime currentTime = LocalTime.now();
//        System.out.println("Thời gian hiện tại: " + currentTime.getHour());
//    }
}
