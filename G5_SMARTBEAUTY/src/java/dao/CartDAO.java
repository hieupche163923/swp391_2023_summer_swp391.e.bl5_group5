/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DAO;
import model.CartDetail;

/**
 *
 * @author binhp
 */
public class CartDAO extends DAO {

    public void finalize() {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean checkExistCart(int id) throws SQLException {
        boolean check = false;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT c.cartid\n"
                + "FROM Cart c INNER JOIN [User] u\n"
                + "ON c.userid = u.uid\n"
                + "WHERE u.uid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                check = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return check;
    }

    public void createCartForUser(int id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "INSERT INTO [Cart]\n"
                + "           ([cartid]\n"
                + "           ,[userid])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public int getCartIdByUserId(int id) throws SQLException {
        int cartId = -1;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT c.cartid\n"
                + "FROM Cart c INNER JOIN [User] u\n"
                + "ON c.userid = u.uid\n"
                + "WHERE u.uid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                cartId = rs.getInt("cartid");
            }
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return cartId;
    }

    public ArrayList<CartDetail> getCartDetails(int cartId) throws SQLException {
        ArrayList<CartDetail> cartDetails = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT p.pid, p.pname, p.price, c.amount, p.[status], p.quantity\n"
                + "FROM Cart_Detail c LEFT JOIN Product p\n"
                + "ON c.pid = p.pid\n"
                + "WHERE c.cartid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, cartId);
            rs = ps.executeQuery();
            while (rs.next()) {
                CartDetail detail = new CartDetail();
                detail.setCartId(cartId);
                detail.setProductId(rs.getInt("pid"));
                detail.setProductName(rs.getString("pname"));
                detail.setPrice(rs.getFloat("price"));
                detail.setAmount(rs.getInt("amount"));
                detail.setStatus(rs.getBoolean("status"));
                detail.setQuantity(rs.getInt("quantity"));
                cartDetails.add(detail);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return cartDetails;
    }

    public int checkProductExist(int cartId, int productId) throws SQLException {
        int amount = -1;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT c.amount\n"
                + "FROM [Cart_Detail] c\n"
                + "WHERE c.cartid = ?\n"
                + "AND c.pid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, cartId);
            ps.setInt(2, productId);
            rs = ps.executeQuery();
            if (rs.next()) {
                amount = rs.getInt("amount");
            }
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

        return amount;
    }

    public void addToCart(int cartId, int productId, int amount) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "INSERT INTO [Cart_Detail]\n"
                + "           ([cartid]\n"
                + "           ,[pid]\n"
                + "           ,[amount])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, cartId);
            ps.setInt(2, productId);
            ps.setInt(3, amount);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public void updateProductAmount(int cartId, int productId, int amount) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "UPDATE [Cart_Detail]\n"
                + "   SET [amount] = ?\n"
                + " WHERE [cartid] = ?\n"
                + " AND [pid] = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, amount);
            ps.setInt(2, cartId);
            ps.setInt(3, productId);
            ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public void deleteCartDetails(int cartId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "DELETE FROM [Cart_Detail]\n"
                + "      WHERE cartid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, cartId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public void deleteCartDetailByTd(int cartId, int productId) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "DELETE FROM [Cart_Detail]\n"
                + "      WHERE cartid = ?\n"
                + "	  AND pid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, cartId);
            ps.setInt(2, productId);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
}
