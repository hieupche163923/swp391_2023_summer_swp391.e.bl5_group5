/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import model.DAO;
import model.Material;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author khanh
 */
public class MaterialDAO extends DAO {

    PreparedStatement ps;
    ResultSet rs;
    String sql;

    public void finalize() throws Throwable {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            super.finalize();
        }
    }

    public ArrayList<Material> getAllMaterial() {
        ArrayList<Material> listMaterials = new ArrayList<>();
        sql = "SELECT m.mid, m.mname, m.quantity, m.category,m.description\n"
                + "FROM [Material] m\n"
                + "INNER JOIN [Category] c\n"
                + "ON m.category = c.cid";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Material material = new Material();
                material.setMid(rs.getInt("mid"));
                material.setMname(rs.getString("mname"));
                material.setQuantity(rs.getInt("quantity"));
                material.setCategory(rs.getInt("category"));
                material.setDescription(rs.getString("description"));
                listMaterials.add(material);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listMaterials;
    }

    public void addNewMaterial(String mname, int quantity, int category, String description) {
        sql = "INSERT INTO [dbo].[Material]\n"
                + "           ([mname]\n"
                + "           ,[quantity]\n"
                + "           ,[category]\n"
                + "           ,[description])\n"
                + "     VALUES\n"
                + "           (?,?,?,?)";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, mname);
            ps.setInt(2, quantity);
            ps.setInt(3, category);
            ps.setString(4, description);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteMaterial(int mid) {
        sql = "DELETE FROM [Material]\n"
                + "      WHERE mid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, mid);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Material getMaterialById(int mid) {
        sql = "SELECT m.mid, m.mname, m.quantity, m.category,m.description\n"
                + "             FROM [Material] m\n"
                + "                INNER JOIN [Category] c\n"
                + "                ON m.category = c.cid\n"
                + "                WHERE m.mid= ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, mid);
            rs = ps.executeQuery();
            while (rs.next()) {
                Material material = new Material();
                material.setMid(rs.getInt("mid"));
                material.setMname(rs.getString("mname"));
                material.setQuantity(rs.getInt("quantity"));
                material.setCategory(rs.getInt("category"));
                material.setDescription(rs.getString("description"));
                return material;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    public ArrayList<Material> MaterialList() {
        ArrayList<Material> list = new ArrayList<>();
        Material m;
        PreparedStatement ps;
        ResultSet rs;
        String sql = "select * from Material";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                m = new Material();
                m.setMid(rs.getInt("mid"));
                m.setMname(rs.getString("mname"));
                m.setCategory(rs.getInt("category"));
                m.setQuantity(rs.getInt("quantity"));
                m.setDescription(rs.getString("description"));
                list.add(m);
            }
            return list;
        } catch (SQLException ex) {
            return null;
        }
    }

    public ArrayList<Material> searchMaterial(String query) {
        ArrayList<Material> listMaterials = new ArrayList<>();
        sql = "SELECT m.mid, m.mname, m.quantity, m.category,m.description\n"
                + "          FROM [Material] m\n"
                + "               INNER JOIN [Category] c\n"
                + "              ON m.category = c.cid\n"
                + "               WHERE m.mname LIKE '%" + query + "%'";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Material material = new Material();
                material.setMid(rs.getInt("mid"));
                material.setMname(rs.getString("mname"));
                material.setQuantity(rs.getInt("quantity"));
                material.setCategory(rs.getInt("category"));
                material.setDescription(rs.getString("description"));
                listMaterials.add(material);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MaterialDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listMaterials;
    }

    public boolean updateMaterial(Material m) {
        try {
            String sql = "UPDATE [dbo].[Material]\n"
                    + "   SET [mname] = ?\n"
                    + "      ,[quantity] = ?\n"
                    + "      ,[category] = ?\n"
                    + "      ,[description] = ?\n"
                    + " WHERE mid = ?";
            PreparedStatement ps;
            ps = con.prepareStatement(sql);
            ps.setString(1, m.getMname());
            ps.setInt(2, m.getQuantity());
            ps.setInt(3,m.getCategory());
            ps.setString(4,m.getDescription());
            ps.setInt(5,m.getMid());
            ps.execute();
            return true;
        } catch (SQLException ex) {
            return false;
        }

    }
}
