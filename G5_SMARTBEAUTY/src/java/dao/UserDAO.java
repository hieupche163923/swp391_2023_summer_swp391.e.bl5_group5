/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import model.User;
import model.DAO;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import static util.Validate.*;
import java.sql.Date;

/**
 *
 * @author msii
 */
public class UserDAO extends DAO {

    public void finalize() {
        try {
            if (con != null) {
                con.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<User> getAllAccount() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String xSql = "SELECT *\n"
                + "FROM [User]";
        ArrayList<User> list = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            rs = ps.executeQuery();
            int uid, role;
            String username, password, displayName, address, email, phone, gender;
            java.sql.Date dob;
            while (rs.next()) {
                uid = rs.getInt(1);
                username = rs.getString(2);
                password = rs.getString(3);
                displayName = rs.getString(4);
                address = rs.getString(5);
                email = rs.getString(6);
                phone = rs.getString(7);
                dob = rs.getDate(8);
                gender = rs.getString(9);
                role = rs.getInt(10);
                boolean status = rs.getBoolean(12);
                list.add(new User(uid, username, password, displayName, address, email, phone, dob, gender, role, status));
            }
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    public void toggleStatus(boolean status, int id) throws SQLException {
        PreparedStatement ps = null;
        String sql = "UPDATE [User]\n"
                + "   SET [status] = ?\n"
                + " WHERE uid =?";
        try {
            ps = con.prepareStatement(sql);
            ps.setBoolean(1, status);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println("Edit failed!!");
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    public int countUserByRole(String roleid) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String xSql = "SELECT Count(uid)\n"
                + "FROM [User]\n"
                + "WHERE [role] like ?";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, '%' + roleid + '%');
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return 0;
    }
    
    public List<User> getFilteredUsers(int roleid, String filter, int offset, int itemPerPage) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String xSql = "SELECT * FROM [User] WHERE [role] = ? \n"
                + " AND ([uid] LIKE ? OR [username] LIKE ? OR [email] LIKE ?) \n"
                + " ORDER BY [uid]\n"
                + " OFFSET ? ROWS\n"
                + " FETCH NEXT ? ROWS ONLY;";
        ArrayList<User> list = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, roleid);
            ps.setString(2, '%' + filter + '%');
            ps.setString(3, '%' + filter + '%');
            ps.setString(4, '%' + filter + '%');
            ps.setInt(5, offset);
            ps.setInt(6, itemPerPage);
            rs = ps.executeQuery();
            while (rs.next()) {
                int uid = rs.getInt(1);
                String username = rs.getString(2);
                String password = rs.getString(3);
                String displayName = rs.getString(4);
                String address = rs.getString(5);
                String email = rs.getString(6);
                String phone = rs.getString(7);
                java.sql.Date dob = rs.getDate(8);
                String gender = rs.getString(9);
                int role = rs.getInt(10);
                boolean status = rs.getBoolean(12);
                list.add(new User(uid, username, password, displayName, address, email, phone, dob, gender, role, status));
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

//lay user bang role
    public List<User> getUserByRoleID(int id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String xSql = "SELECT *\n"
                + "FROM [User]\n"
                + "WHERE [role] = ?;";
        ArrayList<User> list = new ArrayList<>();
        try {
            ps = con.prepareStatement(xSql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            int uid, role;
            String username, password, displayName, address, email, phone, gender;
            java.sql.Date dob;
            while (rs.next()) {
                uid = rs.getInt(1);
                username = rs.getString(2);
                password = rs.getString(3);
                displayName = rs.getString(4);
                address = rs.getString(5);
                email = rs.getString(6);
                phone = rs.getString(7);
                dob = rs.getDate(8);
                gender = rs.getString(9);
                role = rs.getInt(10);
                list.add(new User(uid, username, password, displayName, address, email, phone, dob, gender, role));
            }
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }

    //check User exit
    public User getUser(String xUsername, String Pass) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;

        String xSql = "select * from [User] where username = ? and password = ?";

        String xPassword = toSHA1(Pass);
//        String xPassword = Pass;

        User user = null;

        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, xUsername);
            ps.setString(2, xPassword);
            rs = ps.executeQuery();
            int uid, role;
            String username, password, displayName, address, email, phone, gender;
            java.sql.Date dob;
            while (rs.next()) {
                uid = rs.getInt(1);
                username = rs.getString(2);
                password = rs.getString(3);
                displayName = rs.getString(4);
                address = rs.getString(5);
                email = rs.getString(6);
                phone = rs.getString(7);
                dob = rs.getDate(8);
                gender = rs.getString(9);
                role = rs.getInt(10);
                boolean status = rs.getBoolean(12);
                user = new User(uid, username, password, displayName, address, email, phone, dob, gender, role, status);
            }
            rs.close();
            ps.close();
        } catch (SQLException e) {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return (user);
    }

    // Get roleid by username
    public User getID(String username) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT uid, role FROM [User] WHERE username = ?";

        User user = new User();
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            if (rs.next()) {
                user.setUid(rs.getInt("uid"));
                user.setRole(rs.getInt("role"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return user;
    }

    //insert Customer(Customer
    public void CustomerRegister(String username, String password, String displayName, String address, String email, Date dob, String phone, String gender, String role, String status)
            throws SQLException {
        PreparedStatement ps = null;
        String xSql = "INSERT INTO [User] ([username], [password], [displayName], [address], [email], [phone], [dob], [gender], [role], [status]) VALUES (?,?,?,?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(xSql);
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, displayName);
            ps.setString(4, address);
            ps.setString(5, email);
            ps.setString(6, phone);
            ps.setDate(7, new java.sql.Date(dob.getTime()));
            ps.setString(8, gender);
            ps.setString(9, role);
            ps.setString(10, status); // Nhập giá trị cho Role
            ps.executeUpdate();

        } catch (SQLException e) {
            // Xử lý lỗi SQLException ở đây
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (ps != null) {
                ps.close();
            }
        }
    }

    //get by Username
    public User getUserbyusername(String username) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM [User] where username = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new User(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getDate(8),
                        rs.getString(9),
                        rs.getInt(10));
            }
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return null;
    }

    //get by Username
    public User getUserbyemail(String email) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM [User] where email = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, email);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new User(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getDate(8),
                        rs.getString(9),
                        rs.getInt(10));
            }
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return null;
    }

    //lay uuser nag id
    public User getUserById(int id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM [User] where uid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                return new User(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getString(7),
                        rs.getDate(8),
                        rs.getString(9),
                        rs.getInt(10));
            }
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return null;
    }

    //profile
    public void editUser(int id, String email, String fname, String gender, Date dob, String address, String phoneNum) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "UPDATE [User]\n"
                + "   SET [displayName] = ?\n"
                + "      ,[address] = ?\n"
                + "      ,[email] = ?\n"
                + "      ,[phone] = ?\n"
                + "      ,[dob] = ?\n"
                + "      ,[gender] = ?\n"
                + " WHERE uid =?";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, fname);
            ps.setString(2, address);
            ps.setString(3, email);
            ps.setString(4, phoneNum);
            ps.setDate(5, new java.sql.Date(dob.getTime()));
            ps.setString(6, gender);
            ps.setInt(7, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public void changePassword(String password, int id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "UPDATE [User]\n"
                + "SET\n"
                + "Password = ?\n"
                + "WHERE uid = ?;";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, password);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

}
