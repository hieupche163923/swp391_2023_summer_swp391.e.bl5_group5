/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DAO;
import model.Stock;
import util.Validate;

/**
 *
 * @author binhp
 */
public class StockDAO extends DAO {

    public void finalize() {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(StockDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void importProduct(int productId, int quantity) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "INSERT INTO [dbo].[StockMovement]\n"
                + "           ([productid]\n"
                + "           ,[type]\n"
                + "           ,[quantity]\n"
                + "           ,[createdate])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,1\n"
                + "           ,?\n"
                + "           ,GETDATE())";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, productId);
            ps.setInt(2, quantity);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(StockDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public void exportProduct(int productId, int quantity) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "INSERT INTO [dbo].[StockMovement]\n"
                + "           ([productid]\n"
                + "           ,[type]\n"
                + "           ,[quantity]\n"
                + "           ,[createdate])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,0\n"
                + "           ,?\n"
                + "           ,GETDATE())";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, productId);
            ps.setInt(2, quantity);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(StockDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public ArrayList<Stock> getStocksHistory() throws SQLException {
        ArrayList<Stock> stocks = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT s.movementid, s.productid, p.pname, s.type, s.quantity, s.createdate\n"
                + "FROM StockMovement s LEFT JOIN Product p\n"
                + "ON s.productid = p.pid";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Stock stock = new Stock();
                stock.setStockId(rs.getInt("movementid"));
                stock.setProductId(rs.getInt("productid"));
                stock.setProductName(rs.getString("pname"));
                stock.setType(rs.getBoolean("type"));
                stock.setTypeName(Validate.checkStockType(rs.getBoolean("type")));
                stock.setQuantity(rs.getInt("quantity"));
                stock.setCreateDate(rs.getDate("createdate"));
                stocks.add(stock);
            }
        } catch (SQLException ex) {
            Logger.getLogger(StockDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

        return stocks;
    }
    
    public ArrayList<Stock> filterStocksHistory(String dateFrom, String dateTo) throws SQLException {
        ArrayList<Stock> stocks = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT s.movementid, s.productid, p.pname, s.type, s.quantity, s.createdate\n"
                + "FROM StockMovement s LEFT JOIN Product p\n"
                + "ON s.productid = p.pid\n"
                + "WHERE 1=1\n";
        
        if (dateFrom != null) {
            sql += "AND s.createdate >= '" + dateFrom + "'\n";
        }
        if (dateTo != null) {
            sql += "AND s.createdate <= '" + dateTo + "'\n";
        }
        
        sql += "ORDER BY s.createdate DESC";
        
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Stock stock = new Stock();
                stock.setStockId(rs.getInt("movementid"));
                stock.setProductId(rs.getInt("productid"));
                stock.setProductName(rs.getString("pname"));
                stock.setType(rs.getBoolean("type"));
                stock.setTypeName(Validate.checkStockType(rs.getBoolean("type")));
                stock.setQuantity(rs.getInt("quantity"));
                stock.setCreateDate(rs.getDate("createdate"));
                stocks.add(stock);
            }
        } catch (SQLException ex) {
            Logger.getLogger(StockDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

        return stocks;
    }
}
