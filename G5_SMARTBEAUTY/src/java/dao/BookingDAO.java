/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.*;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DAO;
import model.Booking;

/**
 *
 * @author doanv
 */
public class BookingDAO extends DAO {

    public void finalize() {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(CartDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void addNewBooking(Booking b) {
        PreparedStatement ps;
        String sql = "INSERT INTO [dbo].[Booking]\n"
                + "           ([customerid]\n"
                + "           ,[starttime]\n"
                + "           ,[time]\n"
                + "           ,[bookdate])\n"
                + "     VALUES\n"
                + "           (?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, b.getCustomerid());
            ps.setString(2, b.getStart());
            ps.setInt(3, b.getTime());
            ps.setDate(4, b.getDate());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getLatestBooking() {
        String sql = "select top(1) * from Booking\n"
                + "order by bid desc";
        PreparedStatement ps;
        ResultSet rs;
        int id = -1;
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                id = rs.getInt("bid");
            }
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }

    public void addBookingDetail(int bid, int sid) throws SQLException {
        String sql = "INSERT INTO [dbo].[Booking_Detail]\n"
                + "           ([bid]\n"
                + "           ,[sid])\n"
                + "     VALUES\n"
                + "           (?,?)";
        PreparedStatement ps;
        ps = con.prepareStatement(sql);
        ps.setInt(1, bid);
        ps.setInt(2, sid);
        ps.executeUpdate();
    }
}
