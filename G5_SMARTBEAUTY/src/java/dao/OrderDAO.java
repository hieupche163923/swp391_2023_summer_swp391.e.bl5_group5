/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.DAO;
import model.Order;
import model.OrderDetail;
import static util.Validate.checkOrderStatus;

/**
 *
 * @author binhp
 */
public class OrderDAO extends DAO {

    public void finalize() {
        try {
            if (con != null) {
                con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Order> getAllOrders() throws SQLException {
        ArrayList<Order> orders = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT o.orderid, o.customerid, o.date, u.username, o.[receivername], o.[address], o.[phone], o.[status]\n"
                + "FROM [Order] o INNER JOIN [User] u\n"
                + "ON o.customerid = u.uid\n"
                + "ORDER BY o.date DESC";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setOrderId(rs.getInt("orderid"));
                order.setCustomerId(rs.getInt("customerid"));
                order.setOrderDate(rs.getDate("date"));
                order.setCustomerName(rs.getString("username"));
                order.setRevicerName(rs.getString("receivername"));
                order.setAddress(rs.getString("address"));
                order.setPhone(rs.getString("phone"));
                order.setStatus(rs.getInt("status"));
                order.setStatusMess(checkOrderStatus(order.getStatus()));
                orders.add(order);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

        return orders;
    }

    public ArrayList<Order> filterOrders(String dateFrom, String dateTo) throws SQLException {
        ArrayList<Order> orders = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT o.orderid, o.customerid, o.date, u.username, o.[receivername], o.[address], o.[phone], o.[status]\n"
                + "FROM [Order] o INNER JOIN [User] u\n"
                + "ON o.customerid = u.uid\n"
                + "WHERE 1=1\n";
        if (dateFrom != null) {
            sql += "AND o.date >= '" + dateFrom + "'\n";
        }
        if (dateTo != null) {
            sql += "AND o.date <= '" + dateTo + "'\n";
        }
        sql += "ORDER BY o.date DESC";

        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setOrderId(rs.getInt("orderid"));
                order.setCustomerId(rs.getInt("customerid"));
                order.setOrderDate(rs.getDate("date"));
                order.setCustomerName(rs.getString("username"));
                order.setRevicerName(rs.getString("receivername"));
                order.setAddress(rs.getString("address"));
                order.setPhone(rs.getString("phone"));
                order.setStatus(rs.getInt("status"));
                order.setStatusMess(checkOrderStatus(order.getStatus()));
                orders.add(order);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

        return orders;
    }

    public ArrayList<OrderDetail> getOrderDetailById(String id) throws SQLException {
        ArrayList<OrderDetail> orderDetails = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT p.pid, p.pname, p.price, o.amount\n"
                + "FROM Order_Detail o LEFT JOIN Product p\n"
                + "ON o.productid = p.pid\n"
                + "WHERE o.oderid = ?";

        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.setProductId(rs.getInt("pid"));
                orderDetail.setProductName(rs.getString("pname"));
                orderDetail.setPrice(rs.getFloat("price"));
                orderDetail.setAmount(rs.getInt("amount"));
                orderDetails.add(orderDetail);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

        return orderDetails;
    }

    public void submitOrder(int id, String reciverName, String address, String phone) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "INSERT INTO [dbo].[Order]\n"
                + "           ([customerid]\n"
                + "           ,[date]\n"
                + "           ,[receivername]\n"
                + "           ,[address]\n"
                + "           ,[phone]\n"
                + "           ,[status])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,GETDATE()\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,'1')";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.setString(2, reciverName);
            ps.setString(3, address);
            ps.setString(4, phone);
            ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public Order getLastestOrder() throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT TOP 1 o.orderid, o.customerid, o.date\n"
                + "FROM [Order] o\n"
                + "ORDER BY o.orderid DESC";
        try {
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                Order order = new Order();
                order.setOrderId(rs.getInt("orderid"));
                order.setCustomerId(rs.getInt("customerid"));
                order.setOrderDate(rs.getDate("date"));
                return order;
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

        return null;
    }

    public void submitOrderDetails(int orderId, int productId, int amount) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "INSERT INTO [Order_Detail]\n"
                + "           ([oderid]\n"
                + "           ,[productid]\n"
                + "           ,[amount])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, orderId);
            ps.setInt(2, productId);
            ps.setInt(3, amount);
            ps.executeQuery();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public ArrayList<Order> getOrdersById(int id) throws SQLException {
        ArrayList<Order> orders = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT orderid, customerid, [date], [receivername], [address], [phone], [status]\n"
                + "FROM [Order]\n"
                + "WHERE customerid = ?\n"
                + "ORDER BY [date] DESC";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setOrderId(rs.getInt("orderid"));
                order.setCustomerId(rs.getInt("customerid"));
                order.setOrderDate(rs.getDate("date"));
                order.setRevicerName(rs.getString("receivername"));
                order.setAddress(rs.getString("address"));
                order.setPhone(rs.getString("phone"));
                order.setStatus(rs.getInt("status"));
                order.setStatusMess(checkOrderStatus(order.getStatus()));
                orders.add(order);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

        return orders;
    }

    public ArrayList<Order> filterOrdersById(int id, String dateFrom, String dateTo) throws SQLException {
        ArrayList<Order> orders = new ArrayList<>();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "SELECT orderid, customerid, [date], [receivername], [address], [phone], [status]\n"
                + "FROM [Order]\n"
                + "WHERE customerid = ?\n";
        if (dateFrom != null) {
            sql += "AND [date] >= '" + dateFrom + "'\n";
        }
        if (dateTo != null) {
            sql += "AND [date] <= '" + dateTo + "'\n";
        }

        sql += "ORDER BY [date] DESC";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setOrderId(rs.getInt("orderid"));
                order.setCustomerId(rs.getInt("customerid"));
                order.setOrderDate(rs.getDate("date"));
                order.setRevicerName(rs.getString("receivername"));
                order.setAddress(rs.getString("address"));
                order.setPhone(rs.getString("phone"));
                order.setStatus(rs.getInt("status"));
                order.setStatusMess(checkOrderStatus(order.getStatus()));
                orders.add(order);
            }
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }

        return orders;
    }

    public void cancelOrderById(int id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "UPDATE [Order]\n"
                + "   SET [status] = 0\n"
                + " WHERE orderid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }

    public void changeOrderStatus(int id, int status) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "UPDATE [Order]\n"
                + "   SET [status] = ?\n"
                + " WHERE orderid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, status);
            ps.setInt(2, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public void acceptOrderStatus(int id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "UPDATE [Order]\n"
                + "   SET [status] = 4\n"
                + " WHERE orderid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
    
    public void backOrderById(int id) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "UPDATE [Order]\n"
                + "   SET [status] = 5\n"
                + " WHERE orderid = ?";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(OrderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
    }
}
